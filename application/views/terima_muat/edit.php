<style>
    #t_form td{
        padding-left:10px;
        padding-right:10px;
    }
</style>
<div class="row">
    <div class="col-xs-12">
        <form method="post" action="<?php echo base_url(); ?>terima_muat/update" id="tanda_terima" class="form-input">
            <div class="box">
                <div class="box-header">
                    <div class="row">
                        <div class="col-xs-12">
                            <h3>Form Muat Barang</h3>
                        </div>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-4 col-xs-6">
                            <div class="form-group">
                                <label class="control-label">Tanggal</label>
                                <input type="hidden" id="tgl_terima" name="tgl_terima"
                                    value="<?php echo isset($rows[0]['TGL']) ? $rows[0]['TGL'] : null; ?>"/>
                                <input type="text" id="tanggal" name="tanggal" 
                                    class="form-control datepicker" required=""
                                    value="<?php echo isset($rows[0]['tglterima']) ? $rows[0]['tglterima'] : null; ?>" 
                                    autocomplete="off"/>
                                <?php echo form_error('tanggal'); ?>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-6">
                            <div class="form-group">
                                <label class="control-label">No. Polisi</label>
                                <input type="text" id="no_polisi" name="no_polisi" class="form-control typeahead"
                                autocomplete="off" 
                                value="<?php echo isset($rows[0]['nopol']) ? $rows[0]['nopol'] : null; ?>"/>
                                <?php echo form_error('no_polisi'); ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">No. Muat</label>
                                <input type="text" id="no_muat" name="no_muat" class="form-control" 
                                    value="<?php echo isset($rows[0]['noterima']) ? $rows[0]['noterima'] : null; ?>"/>
                                <?php echo form_error('no_muat'); ?>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Pengirim</label>
                                <input type="text" id="pengirim" name="pengirim" class="form-control" readonly=""
                                    value="<?php echo isset($rows[0]['nmpengirim']) ? $rows[0]['nmpengirim'] : null; ?>"/>
                                <?php echo form_error('pengirim'); ?>
                            </div>
                        </div>

                        <div class="col-sm-4 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">No. Terima</label>
                                <input type="number" id="no_terima" name="no_terima" 
                                    autocomplete="off" class="form-control typeahead"
                                    value="<?php echo isset($rows[0]['NOMOR']) ? $rows[0]['NOMOR'] : null; ?>"/>
                                <?php echo form_error('no_terima'); ?>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Penerima</label>
                                <input type="text" id="penerima" name="penerima" class="form-control" readonly=""
                                value="<?php echo isset($rows[0]['NMPENERIMA']) ? $rows[0]['NMPENERIMA'] : null; ?>"/>
                                <?php echo form_error('penerima'); ?>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Status Pembayaran</label>
                                <input type="text" id="status" name="status" class="form-control"
                                value="<?php echo isset($rows[0]['status']) ? $rows[0]['status'] : null; ?>"/>
                                <?php echo form_error('status'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Alamat</label>
                                <textarea name="alamat" id="alamat1" class="form-control" readonly=""><?php 
                                echo isset($rows[0]['alamat']) ? $rows[0]['alamat'] : null; ?>
                                </textarea> 
                                <?php echo form_error('alamat'); ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <table cellpadding="0" cellspacing="0" border="0" 
                                class="table table-striped table-bordered edit-table" id="terima-muat-detail">
                                <thead>
                                    <tr>
                                        <th width="5%">BANYAK</th>
                                        <th width="5%">SATUAN</th>
                                        <th width="40%">JENIS BARANG</th>
                                        <th width="10%">JUMLAH</th>
                                        <th width="5%">KG/M3</th>
                                        <th width="15%">ONGKOS PER KG/M3</th>
                                        <th width="15%">JUMLAH ONGKOS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="addrow">
                                        <td><input type="text" class="span12" name="banyak[]" id="banyak0" /></td>
                                        <td><input type="text" class="span12" name="satuan[]" id="satuan0" /></td>
                                        <td><input type="text" class="span12" name="jenis_barang[]" id="jenis0" /></td>
                                        <td><input type="text" class="span12 text-right" name="jumlah[]" id="jumlah0" /></td>
                                        <td><input type="text" class="span12" readonly="" name="kg_m3[]" id="kg_m30" /></td>
                                        <td><input type="text" class="span12 text-right" readonly name="ongkos[]" id="ongkos0" "/></td>
                                        <td><input type="text" class="span12 text-right" readonly name="jml_ongkos[]" id="jml_ongkos0" /></td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="3" class="text-right"><b>Total Jumlah</b>&nbsp;&nbsp;&nbsp;</td>
                                        <td colspan="1">
                                            <input type="text" name="total" id="total" class="span12 text-right" readonly="true"/>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                            <button type="button" class="btn btn-default" 
                                onclick="self.history.back()">Kembali</button>
                            <button type="button" class="btn btn-warning" id="batal" onclick="">Batal</button>
                            
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>