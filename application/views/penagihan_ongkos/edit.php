<div class="row">
    <div class="col-xs-12">
        <form method="post" action="<?php echo base_url();?>penagihan_ongkos/update" 
                id="tanda_terima" class="form-input">
            <div class="box">
                <div class="box-header">

                </div>

                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-4 col-xs-6">
                            <div class="form-group">
                                <label class="control-label">No</label>
                                <input type="text" id="m_tagih_no" name="m_tagih_no" class="form-control" 
                                    value="<?php echo $rowedit->m_tagih_no; ?>"/>
                                <?php echo form_error('m_tagih_no'); ?>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Tanggal</label>
                                <input type="text" id="m_tagih_tgl" name="m_tagih_tgl" 
                                    class="form-control datepicker" required=""
                                    value="<?php echo $rowedit->m_tagih_tgl; ?>" autocomplete="off"/>
                                <?php echo form_error('m_tagih_tgl'); ?>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-6">
                            <div class="form-group">
                                <label class="control-label">No. Kirim</label>
                                <input type="text" id="m_tagih_fk" name="m_tagih_fk" class="form-control typeahead"
                                    value="<?php echo $rowedit->m_tagih_fk; ?>" readonly="true" />
                                <input type="hidden" id="nokirim" name="nokirim" value="<?php echo $rowedit->nokirim; ?>"
                                    autocomplete="off">
                                <?php echo form_error('m_tagih_fk'); ?>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Tgl Kirim</label>
                                <input type="text" id="tgl_kirim" name="tgl_kirim" class="form-control"
                                    value="<?php echo $rowedit->tglkirim; ?>" readonly="" />
                                <?php echo form_error('tgl_kirim'); ?>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">No. Polisi</label>
                                <input type="text" id="no_polisi" name="no_polisi" class="form-control"
                                    value="<?php echo $rowedit->nopol; ?>" readonly="" />
                                <?php echo form_error('no_polisi'); ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-3 col-md-offset-9 col-xs-12">
                            <div class="input-group margin">
                                <select id="filter-status_tagih" name="filter_bln" class="form-control">
                                    <option value="-1">--All--</option>
                                    <option value="1">Belum ditagih</option>
                                    <option value="3">Sudah ditagih</option>
                                </select>
                                
                                <div class="input-group-btn">
                                    <button id="filter-by" type="button" class="btn btn-default dropdown-toggle" 
                                        data-toggle="dropdown" aria-expanded="false"><i class="icon-print"></i>Export
                                        <span class="fa fa-caret-down"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" data-url="<?php echo base_url() . 'penagihan_ongkos/printed/' . $rowedit->m_tagih_no;?>" 
                                            data-format="printer" class="export-data" target="_blank">
                                            <i class="icon-print"></i> Printer
                                            </a>
                                        </li>
                                        <li><a href="#" data-url="<?php echo base_url() . 'penagihan_ongkos/printed/' . $rowedit->m_tagih_no;?>" 
                                            data-format="pdf" class="export-data" target="_blank">
                                            <i class="icon-file"></i> PDF
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <table cellpadding="0" cellspacing="0" border="0" 
                                class="table table-striped table-bordered edit-table" id="tagih-ongkos-detail">
                                <thead>
                                    <tr>
                                        <th width="1%">NO</th>
                                        <th width="10%">NO.STT</th>
                                        <th width="5%">KODE</th>
                                        <th width="10%">BARANG</th>
                                        <th width="5%">BANYAK</th>
                                        <th width="1%">SATUAN</th>
                                        <th width="10%">PENGIRIM</th>
                                        <th width="10%">PENERIMA</th>
                                        <th width="10%">BB.JKT</th>
                                        <th width="20%">BB.JBI</th>
                                        <th width="20%">SUBTOTAL</th>
                                        <th width="25%">KET</th>
                                        <th width="8%" style="text-align: center;">
                                            All
                                            <input type="checkbox" id="parent-check" />
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="8">Jumlah</th>
                                        <th id="jml_bbjkt" width="20%" style="text-align: right;"></th>
                                        <th id="jml_bbjbi" width="20%" style="text-align: right;"></th>
                                        <th id="jml_subtotal" width="20%" style="text-align: right;"></th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                            <button type="button" class="btn btn-default" 
                                onclick="self.history.back()">Kembali</button>
                            <!-- <button type="button" class="btn btn-warning" id="batal" onclick="">Batal</button> -->
                            
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
