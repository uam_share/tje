<?php
if (!function_exists('showNumber')) {
    function showNumber($number){
        return empty($number) ? '' : number_format($number);
    }
}
?>
<div class="row">
  <div class="col-xs-12">
    <h2 class="page-header">
      <div class="pull-right" align="right" style="float:right !important;text-align:right !important;font-size:8pt">
        Date: <?php echo date('d-m-Y G:i:s');?>
      </div>
      <?php echo $print_title;?>
    </h2>
  </div>
</div>
<div class="row invoice-info">
  <div class="col-sm-12 table-responsive">
    <table  cellpadding="0" cellspacing="0" border="0" id="t_form" class="table borderless">
        <tbody>
          <tr>
              <td style="width:15%;">NO.</td>
              <td style="width:40%;"><strong><?php echo $rowedit->m_tagih_no; ?></strong></td>
              <td style="width:15%px;">NO KIRIM</td>
              <td style="width:30%;"><strong><?php echo $rowedit->nokirim; ?></strong></td>
          </tr>
          <tr>
              <td>TANGGAL</td>
              <td><?php echo $rowedit->m_tagih_tgl; ?></td>
              <td>TGL KIRIM</td>
              <td><?php echo $rowedit->tglkirim; ?></td>
          </tr>
          <tr>
              <td></td>
              <td></td>
              <td><b>NO. POLISI</b></td>
              <td><?php echo $rowedit->nopol; ?></td>
          </tr>
        </tbody>
    </table>
  </div>
</div>
<!-- /.row -->

<!-- Table row -->
<div class="row">
  <div class="col-xs-12 table-responsive">
    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
        <thead>
            <tr style="background:#eee;">
                <!-- <th width="1%" style="text-align:center;padding:10px;background:#eee;">NO</th> -->
                <th width="1%" style="text-align:center;padding:10px;background:#eee;">NO.STT</th>
                <th width="1%" style="text-align:center;padding:10px;background:#eee;">KODE</th>
                <!-- <th width="10%">BARANG</th> -->
                <th width="1%" style="text-align:center;padding:10px;background:#eee;">QTY</th>
                <!-- <th width="1%" style="text-align:center;padding:10px;background:#eee;">SATUAN</th> -->
                <th width="20%" style="text-align:center;padding:10px;background:#eee;">PENGIRIM</th>
                <th width="20%" style="text-align:center;padding:10px;background:#eee;">PENERIMA</th>
                <th width="17%" style="text-align:center;padding:10px;background:#eee;">BB.JKT</th>
                <th width="17%" style="text-align:center;padding:10px;background:#eee;">BB.JBI</th>
                <!-- <th width="20%">SUBTOTAL</th> -->
                <th width="20%" style="text-align:center;padding:10px;background:#eee;">KET</th>
                <!-- <th width="10%" style="text-align: center;"><input type="checkbox" id="parent-check" /></th> -->
            </tr>
        </thead>
        <tbody>
            <?php
                // var_dump($grid);
                $no=0;
                $sum_bbjkt = 0;
                $sum_bbjmb = 0;
                foreach($grid as $row): 
                $no++;
            ?>
            <tr>
                <!-- <td style="padding:5px;"><?php //echo $no;?></td> -->
                <td style="text-align:center;padding:5px;"><?php echo $row['NOMUAT'];?></td>
                <td style="text-align:center;padding:5px;"><?php echo $row['NOTERIMA'];?></td>
                <td style="text-align:right;padding:5px;"><?php echo $row['BANYAK'];?></td>
                <!-- <td style="padding:5px;"><?php //echo $row['SATUAN'];?></td> -->
                <td style="padding:5px;"><?php echo $row['NMPENGIRIM'];?></td>
                <td style="padding:5px;"><?php echo $row['NMPENERIMA'];?></td>
                <td style="text-align:right;padding:5px;"><?php echo showNumber($row['bb_jkt']);?></td>
                <td style="text-align:right;padding:5px;"><?php echo showNumber($row['jml_ongkos']);?></td>
                <td style="text-align:right;padding:5px;"><?php echo $row['ket_tagih'];?></td>
            </tr>
            <?php 
                $sum_bbjkt += $row['bb_jkt'];
                $sum_bbjmb += $row['jml_ongkos'];
                endforeach;
            ?>
        </tbody>
        <tfoot>
            <tr>
                <th colspan="5" style="padding:10px;background:#eee;">Jumlah</th>
                <th id="jml_bbjkt" style="text-align: right;padding:5px;"><?php echo showNumber($sum_bbjkt);?></th>
                <th id="jml_bbjbi" style="text-align: right;padding:5px;"><?php echo showNumber($sum_bbjmb);?></th>
                <!-- <th id="jml_subtotal" style="text-align: right;"></th> -->
                <th></th>
            </tr>
        </tfoot>
    </table>
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
