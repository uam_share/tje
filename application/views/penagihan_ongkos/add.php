<div class="row">
    <div class="col-xs-12">
        <form method="post" action="<?php echo base_url();?>penagihan_ongkos/simpan" 
                id="tanda_terima" class="form-input">
            <div class="box">
                <div class="box-header">
                </div>

                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-4 col-xs-6">
                            <div class="form-group">
                                <label class="control-label">No</label>
                                <input type="text" id="m_tagih_no" name="m_tagih_no" class="form-control" />
                                <?php echo form_error('m_tagih_no'); ?>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Tanggal</label>
                                <input type="text" id="m_tagih_tgl" name="m_tagih_tgl" 
                                        class="form-control datepicker" required=""
                                       value="<?php echo date('Y-m-d');?>" autocomplete="off"/>
                                <?php echo form_error('m_tagih_tgl'); ?>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-6">
                            <div class="form-group">
                                <label class="control-label">No. Kirim</label>
                                <input type="text" id="m_tagih_fk" name="m_tagih_fk" class="form-control typeahead"  autocomplete="off" />
                                <?php echo form_error('m_tagih_fk'); ?>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Tgl Kirim</label>
                                <input type="text" id="tgl_kirim" name="tgl_kirim" class="form-control"  readonly="" />
                                <?php echo form_error('tgl_kirim'); ?>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">No. Polisi</label>
                                <input type="text" id="no_polisi" name="no_polisi" class="form-control"  readonly="" />
                                <?php echo form_error('no_polisi'); ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <table cellpadding="0" cellspacing="0" border="0" 
                                class="table table-striped table-bordered edit-table" id="tagih-ongkos-detail">
                                <thead>
                                    <tr>
                                        <th width="1%">NO</th>
                                        <th width="10%">NO.STT</th>
                                        <th width="5%">KODE</th>
                                        <th width="10%">BARANG</th>
                                        <th width="5%">BANYAK</th>
                                        <th width="1%">SATUAN</th>
                                        <th width="10%">PENGIRIM</th>
                                        <th width="10%">PENERIMA</th>
                                        <th width="10%">BB.JKT</th>
                                        <th width="20%">BB.JBI</th>
                                        <th width="20%">SUBTOTAL</th>
                                        <th width="25%">KET</th>
                                        <th width="8%" style="text-align: center;">
                                            All
                                            <input type="checkbox" id="parent-check" />
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="8">Jumlah</th>
                                        <th id="jml_bbjkt" width="20%" style="text-align: right;"></th>
                                        <th id="jml_bbjbi" width="20%" style="text-align: right;"></th>
                                        <th id="jml_subtotal" width="20%" style="text-align: right;"></th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                            <button type="button" class="btn btn-default" 
                                onclick="self.history.back()">Kembali</button>
                            <button type="button" class="btn btn-warning" id="batal" onclick="">Batal</button>
                            
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
