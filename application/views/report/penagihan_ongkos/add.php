<style>
    #t_form td{
        padding-left:10px;
        padding-right:10px;
    }
</style>
<h3>PENAGIHAN ONGKOS</h3>
<hr>
<form method="post" action="<?php echo base_url(); ?>penagihan_ongkos/simpan" id="tanda_terima">
    <table  cellpadding="0" cellspacing="0" border="0" id="t_form">
        <tr>
            <td>NO.</td>
            <td><input type="text" id="m_tagih_no" name="m_tagih_no"></td>
            <td><b>NO KIRIM</b></td>
            <td><input type="text" id="m_tagih_fk" name="m_tagih_fk" autocomplete="off"></td>
        </tr>
        <tr>
            <td>TANGGAL</td>
            <td><input type="text" id="m_tagih_tgl" name="m_tagih_tgl" class="datepicker"
                       value="<?php echo date('Y-m-d'); ?>" autocomplete="off"></td>
            <td>TGL KIRIM</td>
            <td>
                <input type="text" id="tgl_kirim" name="tgl_kirim" disabled >
                <input type="hidden" id="nokirim" name="nokirim" autocomplete="off">
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td><b>NO. POLISI</b></td>
            <td><input type="text" id="no_polisi" name="no_polisi"  autocomplete="off"></td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
        <thead>
            <tr>
                <th width="1%">NO</th>
                <th width="5%">NO.STT</th>
                <th width="5%">KODE</th>
                <th width="15%">BARANG</th>
                <th width="5%">BANYAK</th>
                <th width="1%">SATUAN</th>
                <th width="20%">PENGIRIM</th>
                <th width="20%">PENERIMA</th>
                <th width="20%">BB.JKT</th>
                <th width="20%">BB.JBI</th>
                <th width="20%">SUBTOTAL</th>
                <th width="25%">KET</th>
                <th width="8%" style="text-align: center;"><input type="checkbox" id="parent-check" /></th>
            </tr>
        </thead>
        <tbody>
        </tbody>
        <tfoot>
            <tr>
                <th colspan="7">Jumlah</th>
                <th id="jml_bbjkt" width="20%" style="text-align: right;"></th>
                <th id="jml_bbjbi" width="20%" style="text-align: right;"></th>
                <th id="jml_subtotal" width="20%" style="text-align: right;"></th>
                <th></th>
            </tr>
        </tfoot>
    </table>
    <p></p>
    <p></p>
    <button type="submit" class="btn btn-primary">Simpan</button>
    <button type="button" class="btn" id="batal" onclick="return history.back(-1)">Batal</button>
</form>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        $('.datepicker').datepicker({
            format : 'yyyy-mm-dd'
        });
        var oTable = $('#example').dataTable({
            "bProcessing": true,
            "bFilter": false,
            "bPaginate": false,
            "bSort": false,
            "sPaginationType": "bootstrap",
            "bScrollCollapse": true
        });
	
        var url_no = '<?php echo base_url(); ?>penagihan_ongkos/get_no_kirim';
	
        function getNoKirim(){
            $.getJSON(url_no,function(data){
                $('#m_tagih_no').val(data.no_kirim);
            });
        }
        getNoKirim();
	
        $("#batal").click(function(){
            $('#tanda_terima input[type="text"]').val('');
            $('.dimrow').remove();
            getNoKirim();
        });
        var addDiv = $('#example');
	
        $('#tanda_terima').submit(function() {
            var no_polisi = $("#no_polisi").val();
            if ($("#m_tagih_no").val().length == 0){
                bootWindow('NO HARUS DIISI');
                return false;
            }
            if ($("#m_tagih_tgl").val().length == 0){
                bootWindow('TGL HARUS DIISI');
                $("#m_tagih_tgl").focus();
                return false;
            }
            if ($("#m_tagih_fk").val().length == 0){
                bootWindow('NO KIRIM HARUS DIISI');
                return false;
            }
            //            if (no_polisi.length == 0){
            //                bootWindow('NO POLISI HARUS DIISI');
            //                return false;
            //            }
            $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                data: $(this).serialize(),
                dataType: 'json',
                success: function(data) {
                    //$('#tanda_terima input[type="text"]').val('');
                    //alert();
                    if(data.success == true){
                        bootWindow(data.resp,BASEURL + 'penagihan_ongkos');
                    }else{
                        bootWindow(data.resp);
                    }   
                }
            })
            return false;
        });
	
        function bootWindow(msg,url){
            $('<div class="modal hide fade" id="myModal">'
                +'<div class="modal-header">'
                +'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'
                +'<h3>KONFIRMASI</h3>'
                +'</div>'
                +'<div class="modal-body">'
                +'<p></p>'
                +'</div>'
                +'<div class="modal-footer">'
                +'<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>'
                +'</div>'
                +'</div>').appendTo('body');
            $('#myModal .modal-body p').text(msg);
            $('#myModal').modal('show');
            //sleep(300);
            if(typeof(url) != 'undefined'){
                setTimeout(function(){window.location = url}, 1000);
                
            }
            
        }
        function addTable(data){
            var jml_bbjkt;
            var jml_bbjbi;
            var jml_subtotal;
            jml_bbjkt = jml_bbjbi = jml_subtotal = 0;
            for(var i=0;i<data.length;i++){
                var number = i +1;
                $('<tr class="dimrow">'
                     + '<td width="1%">' + number + '</td>'
                    +'<td>'+ data[i].NOMUAT + '<input type="hidden" name="id_kirim[]" value="' + data[i].id + '"/></td>'
                    +'<td>' + data[i].NOTERIMA + '</td>'
                    +'<td width="15%">' + data[i].BARANG + '</td>'
                    +'<td style="text-align: center;">' + data[i].BANYAK + '</td>'
                    +'<td width="5%">' + data[i].SATUAN + '</td>'
                    +'<td width="30%">' + data[i].NMPENGIRIM + '</td>'
                    +'<td>' + data[i].NMPENERIMA + '</td>'
                    +'<td><input class="in_bbjkt" onchange="updateJmlBBjkt(this)" type="text" name="bb_jkt[]" value="' + toRP(data[i].bb_jkt) + '" style="text-align: right;width:80%;"/></td>'
                    +'<td><input class="in_bbjbi" onchange="updateJmlBBjbi(this)" type="text" name="jml_ongkos[]" value="' + toRP(data[i].jml_ongkos) + '" style="text-align: right;width:80%;"/></td>'
                    +'<td class="in_subtotal" style="text-align: right;">' + toRP(data[i].subtotal) + '</td>'
                    +'<td><textarea name="ket_tagih[]">'+data[i].ket_tagih+'</textarea></td>'
                    +'<td><input type="checkbox" class="span12 child-check" name="stat[]" value="' + data[i].id + '" onclick="clickcek(this)" /></td>'
                    +'</tr>').appendTo(addDiv);
                jml_bbjkt = jml_bbjkt + parseInt(data[i].bb_jkt);
                jml_bbjbi = jml_bbjbi + parseInt(data[i].jml_ongkos);
                jml_subtotal = jml_subtotal + parseInt(data[i].subtotal);

            }
            $('#jml_bbjkt').html(toRP(jml_bbjkt));
            $('#jml_bbjbi').html(toRP(jml_bbjbi));
            $('#jml_subtotal').html(toRP(jml_subtotal));
        }
        var dtobj = [];
        $('#m_tagih_fk').typeahead({
            source: function (query, process) {
                var objects = [];
                var map = {};
                $.ajax({
                    type: 'POST',
                    url: "<?php echo base_url(); ?>penagihan_ongkos/get_nopol",
                    data: "query="+query, 
                    dataType:"json",
                    success:function(data){
                        if (data == false){
                            $('#m_tagih_fk').val('');
                            return false;
                        }
                        dtobj = data;
                        $.each(data, function(i, object) {
                            map[object.nokirim] = object;
                            objects.push(object.nokirim);
                            
                        });
                        process(objects);
                    }
                    
                });
            },
            updater: function(item) {
                var nokirim = '';
                $.each(dtobj, function(i, object) {
                    if(item == object.nokirim){
                        //                        alert(object.nopol);
                        $('#tgl_kirim').val(object.tglkirim);
                        $('#nokirim').val(object.nokirim);
                        $('#no_polisi').val(object.nopol);
                        
                        nokirim = object.nokirim;
                    }      
                });
                
                $.ajax({
                    type: 'POST',
                    url: "<?php echo base_url(); ?>penagihan_ongkos/get_detail_kirim",
                    data: "no_polisi="+nokirim, 
                    dataType:"json",
                    success:function(data){
                        $('.odd').remove();
                        $('.dimrow').remove();
                        addTable(data.grid);
                    }
                });
                return item;
            }
        });
        
        $('#parent-check').click(function(){
            //alert('tes');
            if($(this).attr('checked') == 'checked'){
                $('#example > tbody > tr > td > .child-check').attr('checked',true);
            }else{
                $('#example > tbody > tr > td > .child-check').attr('checked',false);
            }
                
        });
    });
    function updateJmlBBjkt(obj){
        //alert($(obj).val());
        setJmlbbjkt();
    }
    function setJmlbbjkt(){
        
        var in_bbjbi = $('.in_bbjbi');
        var in_bbjkt = $('.in_bbjkt');
        var in_subtotal = $('.in_subtotal');
        
        var jml_bbjkt,jml_subtotal;
        jml_bbjkt = jml_subtotal = 0;
        var inbbjbi;
        var inbbjkt;
        var insubtotal;
        for(var i=0;i<in_bbjkt.length;i++){
            //alert($(in_bbjkt[i]).val());
            inbbjkt = $(in_bbjkt[i]).val();
            inbbjkt = inbbjkt.replace(".","");
            inbbjkt = inbbjkt.replace(".","");
            jml_bbjkt = jml_bbjkt + parseInt(inbbjkt);
            
            inbbjbi = $(in_bbjbi[i]).val();
            inbbjbi = inbbjbi.replace(".","");
            inbbjbi = inbbjbi.replace(".","");
            
            insubtotal = parseInt(inbbjbi) + parseInt(inbbjkt);
            $(in_subtotal[i]).html(toRP(insubtotal));
            jml_subtotal += insubtotal;
            
            
        }
        $('#jml_bbjkt').html(toRP(jml_bbjkt));
        $('#jml_subtotal').html(toRP(jml_subtotal));
    }
    function updateJmlBBjbi(obj){
        //alert($(obj).val());
        setJmlbbjbi();
    }
    function setJmlbbjbi(){
        var in_bbjbi = $('.in_bbjbi');
        var in_bbjkt = $('.in_bbjkt');
        var in_subtotal = $('.in_subtotal');
        
        var jml_bbjbi,jml_subtotal;
        jml_bbjbi = jml_subtotal = 0;
        var inbbjbi;
        var inbbjkt;
        var insubtotal;
        for(var i=0;i<in_bbjbi.length;i++){
            //alert($(in_bbjkt[i]).val());
            inbbjbi = $(in_bbjbi[i]).val();
            inbbjbi = inbbjbi.replace(".","");
            inbbjbi = inbbjbi.replace(".","");
            jml_bbjbi = jml_bbjbi + parseInt(inbbjbi);
            
            inbbjkt = $(in_bbjkt[i]).val();
            inbbjkt = inbbjkt.replace(".","");
            inbbjkt = inbbjkt.replace(".","");
            
            insubtotal = parseInt(inbbjbi) + parseInt(inbbjkt);
            $(in_subtotal[i]).html(toRP(insubtotal));
            jml_subtotal += insubtotal;
            
            
        }
        $('#jml_bbjbi').html(toRP(jml_bbjbi));
        $('#jml_subtotal').html(toRP(jml_subtotal));
        //alert(in_bbjkt.length);
    }
    
    function clickcek(obj) {
        //alert($(this).attr('checked'));
        if ($(obj).attr('checked') != 'checked') {
            //alert('tes');
            $('#parent-check').attr('checked', false);
            $(obj).parent().parent().removeClass('list-ceck');
        } else {
            $(obj).parent().parent().addClass('list-ceck');
        }
    }
</script>