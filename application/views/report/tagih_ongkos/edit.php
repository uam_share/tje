
<style>
    #t_form td{
        padding-left:10px;
        padding-right:10px;
    }
</style>
<h3>PERINCIAN PENAGIHAN ONGKOS</h3>
<hr>
<form method="post" action="<?php echo base_url(); ?>tagih_ongkos/update" id="tanda_terima">
    <table  cellpadding="0" cellspacing="0" border="0" id="t_form">
        <tr>
            <td>NO.</td>
            <td><input type="text" id="no_kirim" name="no_kirim" value="<?php echo $rowedit->NOKIRIM; ?>" readonly/></td>
            <td>KETERANGAN</td>
            <td rowspan="2"><textarea id="keterangan" name="keterangan"><?php echo $rowedit->KETERANGAN; ?></textarea></td>
        </tr>
        <tr>
            <td>TANGGAL</td>
            <td><input type="text" id="tanggal" name="tanggal" class="datepicker" value="<?php echo $rowedit->TGLKIRIM; ?>" autocomplete="off"></td>
            <td></td>
        </tr>
        <tr>
            <td><b>NO. POLISI</b></td>
            <td><input type="text" id="no_polisi" name="no_polisi" value="<?php echo $rowedit->NOPOL; ?>" readonly autocomplete="off"></td>
            <td></td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
        <thead>
            <tr>
                <th width="10%">NO.STT</th>
                <th width="7%">KODE</th>
                <th width="5%">BANYAK</th>
                <th width="10%">SATUAN</th>
                <th width="20%">PENGIRIM</th>
                <th width="20%">PENERIMA</th>
                <th width="10%">STATUS</th>
                <!--<th width="10%" style="text-align: center;"><input type="checkbox" id="parent-check" /></th>-->
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <p></p>
    <p></p>
    <button type="submit" class="btn btn-primary">Simpan</button>
    <button type="button" class="btn" id="batal">Batal</button>
    <button type="button" class="btn" id="batal" onclick="history.back()">Kembali</button>
</form>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        $('.datepicker').datepicker({
            format : 'yyyy-mm-dd'
        });
        var oTable = $('#example').dataTable({
            "bProcessing": true,
            "bFilter": false,
            "bPaginate": false,
            "bSort": false,
            "sPaginationType": "bootstrap",
            "bScrollCollapse": true
        });
	
        var url_no = '<?php echo base_url(); ?>tagih_ongkos/get_no_kirim';
        //	
        //        function getNoKirim(){
        //            $.getJSON(url_no,function(data){
        //                $('#no_kirim').val(data.no_kirim);
        //            });
        //        }
        //        getNoKirim();
	
        $("#batal").click(function(){
            $('#tanda_terima input[type="text"]').val('');
            $('.dimrow').remove();
            getNoKirim();
        });
        var addDiv = $('#example');
	
        function sum_total(){
            var sum = 0;
            $("input[name *= 'jml_ongkos']").each(function(){
                sum += +$(this).val();
            });
            $("#total").val(sum);
        }
	
        $('#tanda_terima').submit(function() {
            var no_polisi = $("#no_polisi").val();
            if ($("#no_kirim").val().length == 0){
                bootWindow('NO KIRIM HARUS DIISI');
                return false;
            }
            if ($("#tanggal").val().length == 0){
                bootWindow('TANGGAL HARUS DIISI');
                return false;
            }
            if (no_polisi.length == 0){
                bootWindow('NO POLISI HARUS DIISI');
                return false;
            }
            $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                data: $(this).serialize(),
                dataType: 'json',
                success: function(data) {
                    $('#tanda_terima input[type="text"]').val('');
                    $('#keterangan').val('');
                    $('.dimrow').remove();
                    bootWindow(data.resp, BASEURL + 'tagih_ongkos');
                    //getNoKirim();
                }
            })
            return false;
        });
	
        function bootWindow(msg,turl){
            $('<div class="modal hide fade" id="myModal">'
                +'<div class="modal-header">'
                +'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'
                +'<h3>KONFIRMASI</h3>'
                +'</div>'
                +'<div class="modal-body">'
                +'<p></p>'
                +'</div>'
                +'<div class="modal-footer">'
                +'<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>'
                +'</div>'
                +'</div>').appendTo('body');
            $('#myModal .modal-body p').text(msg);
            $('#myModal').modal('show');
            if(typeof(turl) != 'undefined'){
                setTimeout(function(){window.location = turl}, 2000);
            }
        }
        function addTable(data){
            for(var i=0;i<data.length;i++){
		
                var no_stt = 'no_stt'+i;
                var kode = 'kode'+i;
                var banyak = 'banyak'+i;
                var satuan = 'satuan'+i;
                var pengirim = 'pengirim'+i;
                var penerima = 'penerima'+i;
                var status = 'status'+i;
                var jml_ongkos = 'jml_ongkos'+i;
			
                $('<tr class="dimrow">'
                    +'<td>'+data[i].no_stt+'<input type="hidden" class="span12" name="no_stt[]" id="'+no_stt+'" readonly="true"/></td>'
                    +'<td>'+data[i].kode+'<input type="hidden" class="span12" name="kode[]" id="'+kode+'" readonly="true"/></td>'
                    +'<td>'+data[i].banyak+'<input type="hidden" class="span12" name="banyak[]" id="'+banyak+'" readonly="true"/></td>'
                    +'<td>'+data[i].satuan+'<input type="hidden" class="span12" name="satuan[]" id="'+satuan+'" readonly="true"/></td>'
                    +'<td>'+data[i].pengirim+'<input type="hidden" class="span12" name="pengirim[]" id="'+pengirim+'" readonly="true"/></td>'
                    +'<td>'+data[i].penerima+'<input type="hidden" class="span12" name="penerima[]" id="'+penerima+'" readonly="true"/></td>'
                    +'<td>'+data[i].status+'<input type="hidden" class="span12" name="status[]" id="'+status+'" readonly="true"/>'
                    +'<input type="hidden" class="span12" name="jml_ongkos[]" id="'+jml_ongkos+'" readonly="true"/></td>'
                //                        +'<td><input type="checkbox" class="span12 child-check" name="stat[' + data[i].no_stt + ']" value="' + data[i].no_stt + '" onclick="clickcek()" /></td>'
                    +'</tr>').appendTo(addDiv);
                $('#'+no_stt).val(data[i].no_stt);
                $('#'+kode).val(data[i].kode);
                $('#'+banyak).val(data[i].banyak);
                $('#'+satuan).val(data[i].satuan);
                $('#'+pengirim).val(data[i].pengirim);
                $('#'+penerima).val(data[i].penerima);
                $('#'+status).val(data[i].status);
                $('#'+jml_ongkos).val(data[i].jml_ongkos);
                    
                //                }
            }
        }
        //Get Data Detail Kirim
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url(); ?>tagih_ongkos/get_detail_kirim",
            data: "nokirim=<?php echo $rowedit->NOKIRIM;?>", 
            dataType:"json",
            success:function(data){
                $('.odd').remove();
                $('.dimrow').remove();
                addTable(data.grid);
            }
        });
//        $('#no_polisi').typeahead({
//            source: function (query, process) {
//                objects = [];
//                map = {};
//                $.ajax({
//                    type: 'POST',
//                    url: "<?php echo base_url(); ?>tagih_ongkos/get_nopol",
//                    data: "query="+query, 
//                    dataType:"json",
//                    success:function(data){
//                        if (data == false){
//                            $('#no_polisi').val('');
//                            return false;
//                        }
//                        $.each(data, function(i, object) {
//                            map[object.nopol] = object;
//                            objects.push(object.nopol);
//                        });
//                        process(objects);
//                    }
//                });
//            }
//            ,updater: function(item) {
//                $.ajax({
//                    type: 'POST',
//                    url: "<?php echo base_url(); ?>tagih_ongkos/get_detail_muat",
//                    data: "no_polisi="+item, 
//                    dataType:"json",
//                    success:function(data){
//                        $('.odd').remove();
//                        $('.dimrow').remove();
//                        addTable(data.grid);
//                    }
//                });
//                return item;
//            }
//        });
        $('#parent-check').click(function(){
            //alert('tes');
            if($(this).attr('checked') == 'checked'){
                $('#example > tbody > tr > td > .child-check').attr('checked',true);
            }else{
                $('#example > tbody > tr > td > .child-check').attr('checked',false);
            }
                
        });
    });
    function clickcek(){
        //alert($(this).attr('checked'));
        if($(this).attr('checked') != 'checked')
            $('#parent-check').attr('checked',false);
    };
</script>

