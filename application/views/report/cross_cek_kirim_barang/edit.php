<style>
    #t_form td{
        padding-left:10px;
        padding-right:10px;
    }
</style>
<h3>CROSS CEK PENGIRIMAN BARANG</h3>
<hr>
<form method="post" action="<?php echo base_url(); ?>crosscek_kirim_barang/update" id="tanda_terima">
    <table  cellpadding="0" cellspacing="0" border="0" id="t_form">
        <tr>
            <td>NO.</td>
            <td><input type="text" id="m_cek_no" name="m_cek_no" readonly 
                       value="<?php echo $rowedit->m_cek_no; ?>"></td>
            <td><b>NO KIRIM</b></td>
            <td><input type="text" id="m_cek_fk" name="m_cek_fk" readonly 
                       value="<?php echo $rowedit->m_cek_fk; ?>" autocomplete="off"></td>
        </tr>
        <tr>
            <td>TANGGAL</td>
            <td><input type="text" id="m_cek_tgl" name="m_cek_tgl" class="datepicker" 
                       value="<?php echo $rowedit->m_cek_tgl; ?>" autocomplete="off"></td>
            <td>TGL KIRIM</td>
            <td><input type="text" id="tgl_kirim" name="tgl_kirim" disabled 
                       value="<?php echo $rowedit->TGLKIRIM; ?>"/></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td><b>NO. POLISI</b></td>
            <td><input type="text" id="no_polisi" name="no_polisi" readonly 
                       value="<?php echo $rowedit->NOPOL; ?>" autocomplete="off"></td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
        <thead>
            <tr>
                <th width="1%">NO</th>
                <th width="10%">NO.STT</th>
                <th width="10%">KODE</th>
                <th width="10%">BARANG</th>
                <th width="10%">BANYAK</th>
                <th width="10%">SATUAN</th>
                <th width="25%">PENGIRIM</th>
                <th width="25%">PENERIMA</th>
                <th width="25%">KET</th>
                <th width="10%" style="text-align: center;"><input type="checkbox" id="parent-check" name="parentstat" /></th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <p></p>
    <p></p>
    <button type="submit" class="btn btn-primary">Simpan</button>
    <button type="button" class="btn" id="batal" onclick="return history.back(-1)">Batal</button>
</form>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd'
        });
        var oTable = $('#example').dataTable({
            "bProcessing": true,
            "bFilter": false,
            "bPaginate": false,
            "bSort": false,
            "sPaginationType": "bootstrap",
            "bScrollCollapse": true
        });

        //        var url_no = '<?php echo base_url(); ?>crosscek_kirim_barang/get_no_kirim';
        //	
        //        function getNoKirim(){
        //            $.getJSON(url_no,function(data){
        //                $('#m_cek_no').val(data.no_kirim);
        //            });
        //        }
        //        getNoKirim();

        getdetailkirim('<?php echo $rowedit->m_cek_fk; ?>', BASEURL + "crosscek_kirim_barang/get_detail_kirim/true");

        $("#batal").click(function() {
            $('#tanda_terima input[type="text"]').val('');
            $('.dimrow').remove();
            getNoKirim();
        });
        var addDiv = $('#example');

        $('#tanda_terima').submit(function() {
            var no_polisi = $("#no_polisi").val();
            if ($("#m_cek_no").val().length == 0) {
                bootWindow('NO HARUS DIISI');
                return false;
            }
            if ($("#m_cek_tgl").val().length == 0) {
                bootWindow('TGL HARUS DIISI');
                $("#m_cek_tgl").focus();
                return false;
            }
            if ($("#m_cek_fk").val().length == 0) {
                bootWindow('NO KIRIM HARUS DIISI');
                return false;
            }
            if (no_polisi.length == 0) {
                bootWindow('NO POLISI HARUS DIISI');
                return false;
            }
            $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                data: $(this).serialize(),
                dataType: 'json',
                success: function(data) {
                    //$('#tanda_terima input[type="text"]').val('');
                    //alert();
                    if (data.success == true) {
                        bootWindow(data.resp, BASEURL + 'crosscek_kirim_barang');
                    } else {
                        bootWindow(data.resp);
                    }


                }
            })
            return false;
        });

        function bootWindow(msg, url) {
            $('<div class="modal hide fade" id="myModal">'
                    + '<div class="modal-header">'
                    + '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'
                    + '<h3>KONFIRMASI</h3>'
                    + '</div>'
                    + '<div class="modal-body">'
                    + '<p></p>'
                    + '</div>'
                    + '<div class="modal-footer">'
                    + '<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>'
                    + '</div>'
                    + '</div>').appendTo('body');
            $('#myModal .modal-body p').text(msg);
            $('#myModal').modal('show');
            //sleep(300);
            if (typeof (url) != 'undefined') {
                setTimeout(function() {
                    window.location = url
                }, 1000);

            }

        }
        function addTable(data) {
            for (var i = 0; i < data.length; i++) {
                var childcheck = (data[i].stat2 == '2') ? 'checked' : '';
                var childread;
                var clstr = '';
                if (data[i].stat2 == '2') {
                    childread = 'checked';
                    //$('#parent-check').hide();
                    $('#parent-check').attr('checked', true);
                    $('#parent-check').val(data[i].stat);
                    clstr = 'list-ceck';
                } else {
                    childread = '';
                }
                var number = i +1;
                $('<tr class="dimrow ' + clstr + '">'
                        + '<td width="1%">' + number + '</td>'
                        + '<td>' + data[i].NOMUAT + '<input type="hidden" name="id_kirim[]" value="' + data[i].id + '"/></td>'
                        + '<td>' + data[i].NOTERIMA + '<input type="hidden" name="stat_old[]" value="' + data[i].stat + '"/></td>'
                        + '<td width="15%">' + data[i].BARANG + '</td>'
                        + '<td>' + data[i].BANYAK + '</td>'
                        + '<td width="1%">' + data[i].SATUAN + '</td>'
                        + '<td>' + data[i].NMPENGIRIM + '</td>'
                        + '<td>' + data[i].NMPENERIMA + '</td>'
                        + '<td><textarea name="ket_cek[]"> ' + data[i].ket_cek + '</textarea></td>'
                        + '<td><input type="checkbox" ' + childread + ' class="span12 child-check" ' + childcheck + ' name="stat2[]" value="' + data[i].id + '" onclick="clickcek(this)" /></td>'
                        + '</tr>').appendTo(addDiv);

            }
        }

        function getdetailkirim(item, turl) {
            $.ajax({
                type: 'POST',
                url: turl,
                data: "no_polisi=" + item,
                dataType: "json",
                success: function(data) {
                    $('.odd').remove();
                    $('.dimrow').remove();
                    addTable(data.grid);
                }
            });
        }
        $('#parent-check').click(function() {
            //alert('tes');
            if ($(this).attr('checked') == 'checked') {
                $('#example > tbody > tr > td > .child-check').attr('checked', true);
            } else {
                $('#example > tbody > tr > td > .child-check').attr('checked', false);
            }

        });
    });
    function clickcek(obj) {
        //alert($(this).attr('checked'));
        if ($(obj).attr('checked') != 'checked') {
            //alert('tes');
            $('#parent-check').attr('checked', false);
            $(obj).parent().parent().removeClass('list-ceck');
        } else {
            $(obj).parent().parent().addClass('list-ceck');
        }
    }
</script>