<!DOCTYPE html>
<html>
    <head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <base href="<?php echo base_url(); ?>" />
		<title>Muat Barang</title>
		<link rel="stylesheet" type="text/css" href="assets/css/print.css" media="screen,print" />
	</head>
	<body>
		<div class="wrapper-print">
			<div id="box-print">
			<?php
				//var_dump($ttmuat);
			?>
				<label class="label-fields" style="top : 0.2cm; left: 15cm;width : 2.1cm;font-size: 10pt; ">
					<b>NO TERIMA :<b>
				</label >
				<label style="top : 0.2cm; left: 18cm;width : 2.1cm;font-size: 10pt;">
					<b><?php echo $ttmuat[0]->NOMOR;?></b>
				</label>
				<label style="top : 0.8cm; left: 10cm;width : 2.1cm;font-size: 10pt;">
					<b><?php echo $ttmuat[0]->NOPOL;?></b>
				</label>
				<label style="top : 1.5cm; left: 10cm;width : 2.1cm;font-size: 10pt;">
					<b><?php echo $ttmuat[0]->NOTERIMA;?></b>
				</label>
				<!-- Pengirim -->
				<label style="top : 1.3cm; left: 2.8cm;font-size: 8pt;">
					<?php echo $ttmuat[0]->NMPENGIRIM;?>
				</label>
				<label style="top : 1.7cm; left: 2.8cm;font-size: 8pt;">
					<?php echo $ttmuat[0]->ALAMAT1;?>
				</label>
				
				<!-- Penerima -->
				<label style="top : 1.3cm; left: 14.8cm;font-size: 8pt;">
					<?php echo $ttmuat[0]->NMPENERIMA;?>
				</label>
				<label style="top : 1.7cm; left: 14.8cm;font-size: 8pt;">
					<?php echo $ttmuat[0]->ALAMAT2;?>
				</label>
				
				<!-- Detail -->
				<?php
					$top = 2.6;
					foreach($ttmuat as $rmuat): 
					?>
					<label style="top : <?php echo $top;?>cm; left: 1.5cm;font-size: 8pt;">
					<?php echo $rmuat->BANYAK . ' '.$rmuat->SATUAN;?>
					</label>
					<label style="top : <?php echo $top;?>cm; left: 3.6cm;font-size: 8pt;">
						<?php echo $rmuat->BARANG;?>
					</label>
					<label style="top : <?php echo $top;?>cm; left: 12.7cm;font-size: 8pt;">
						<?php echo $rmuat->JUMLAH . ' '.$rmuat->SAT;?>
					</label>
					<label style="top : <?php echo $top;?>cm; left: 15.1cm;font-size: 8pt;">
						<?php echo $rmuat->ONGKOS;?>
					</label>
					<label style="top : <?php echo $top;?>cm; left: 17.5cm;font-size: 8pt;">
						<?php echo $rmuat->JML_ONGKOS;?>
					</label>
					<?php
					$top = $top + 0.5;
					endforeach;
				?>
				<!-- END Detail -->
				
				<!-- Tanggal -->
				<label style="top : 5cm; left: 14.8cm;font-size: 8pt;">
					JAKARTA 22 Nopember 2014
				</label>
			</div>
		</div>
	</body>
</html>

