<h3>STATUS BARANG</h3>
<hr>
<form action="status_barang/caristatus" method="post" id="form-cari-no">
    <table class="table table-striped table-bordered">
        <tr>
            <td>
                <select id="filter_no" name="filter_no" style="margin-top: 10px; width: 150px;">
                    <option value="noterima" <?php echo (isset($filter_no) && $filter_no == 'noterima') ? 'selected' : ''; ?>>No Terima Barang</option>
                    <option value="nmpengirim" <?php echo (isset($filter_no) && $filter_no == 'nmpengirim') ? 'selected' : ''; ?>>Pengirim</option>
                    <option value="nmpenerima" <?php echo (isset($filter_no) && $filter_no == 'nmpenerima') ? 'selected' : ''; ?>>Penerima</option>
                </select>          
                <input type="text" name="nosearch" id="nosearch" placeholder="No Barang"
                       style="margin-top: 10px; width: 100px;<?php echo isset($nosearch) ? '' : 'display: none;'; ?>"
                       value="<?php echo isset($nosearch) ? $nosearch : ''; ?>"/>
                <input type="text" id="pengirim" name="pengirim" placeholder="Masukan Nama Pengirim"
                       style="margin-top: 10px; width: 300px;<?php echo isset($nmpengirim) ? '' : 'display: none;'; ?>" 
                       value="<?php echo isset($nmpengirim) ? $nmpengirim : ''; ?>" autocomplete="off">
                <input type="text" id="penerima" name="penerima" placeholder="Masukan Nama Penerima"
                       style="margin-top: 10px; width: 300px;<?php echo isset($nmpenerima) ? '' : 'display: none;'; ?>"
                       value="<?php echo isset($nmpenerima) ? $nmpenerima : ''; ?>" autocomplete="off">
                <button class="btn btn-primary">Cari</button>
                <div style="float: right;padding-top: 10px;">
                    <select name="filter_bln" style="width: 120px;">
                        <?php for ($i = 1; $i <= 12; $i++): ?>
                            <option value="<?php echo $i; ?>" <?php echo (isset($filter_bln) && $filter_bln == $i) ? 'selected' : ''; ?>>
                                <?php echo $this->fungsi->bulan2($i); ?>
                            </option>
                            <?php
                        endfor;
                        ?>
                    </select>
                    <select name="filter_thn" style="width: 75px;">
                        <?php for ($i = 2010; $i <= 2050; $i++): ?>
                            <option value="<?php echo $i; ?>" <?php echo (isset($filter_thn) && $filter_thn == $i) ? 'selected' : ''; ?>>
                                <?php echo $i; ?>
                            </option>
                            <?php
                        endfor;
                        ?>
                    </select>
                </div>
            </td>
        </tr>
    </table>    
</form>

<?php if (!empty($msg)) { ?>
    <div class="mesg-status-barang">
        <span><?php echo $msg; ?></span>
    </div>
    <?php
}
?>

<table class="table table-striped table-bordered" id="table-search"
       style="font-size: 12px;">
    <thead>
        <tr style="background: #DEE;">
            <th width="5%">KODE</th>
            <th width="7%">No. Muat</th>
            <th width="10%">No. Kirim</th>
            <th width="10%">Nopol</th>
            <th width="10%">Barang</th>
            <th width="1%">BYK</th>
            <th width="1%">Satuan</th>
            <th width="10%">Jumlah</th>
            <th width="25%">Nama Pengirim</th>
            <th width="25%">Nama Penerima</th>
            <th width="10%">Status</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (!empty($hasil) && $hasil):
            //var_dump($querydata);
            foreach ($querydata as $rdata):
                ?>
                <tr>
                    <td><?php echo $rdata->noterima . ' - ' . $rdata->tglterima; ?></td>
                    <td><?php echo $rdata->nomuat . ' - ' . $rdata->tglmuat; ?></td>
                    <td><?php echo $rdata->nokirim . ' - ' . $rdata->tglkirim; ?></td>
                    <td><?php echo $rdata->nopol; ?></td>
                    <td><?php echo $rdata->barang; ?></td>
                    <td><?php echo $rdata->banyak; ?></td>
                    <td><?php echo $rdata->satuan; ?></td>
                    <td><?php echo $rdata->jumlah; ?></td>
                    <td><?php echo $rdata->nmpengirim; ?></td>
                    <td><?php echo $rdata->nmpenerima; ?></td>
                    <td><?php echo $rdata->status_barang; ?></td>
                </tr>
                <?php
            endforeach;
        endif;
        ?>
    </tbody>
</table>
<script type="text/javascript" charset="utf-8">
    $('#table-search').dataTable({
        "bInfo":     false,
        "bOrder":     false
    });
    $(document).ready(function() {
        $('#filter_no').change(function(){
            $('#form-cari-no input[type=text]').hide();
            switch($(this).val()){
                default :
                    break;
                case 'noterima':  
                    $('#nosearch').show();
                    break;
                case 'nmpenerima':  
                    $('#penerima').show();
                    break;
                case 'nmpengirim':  
                    $('#pengirim').show();
                    break;
            }
        })
        $('#pengirim').typeahead({
            source: function (query, process) {
                objects = [];
                map = {};
                $.ajax({
                    type: 'POST',
                    url: "<?php echo base_url(); ?>terima_barang/get_pengirim",
                    data: "query="+query, 
                    dataType:"json",
                    success:function(data){
                        if (data == false){
                            $('#pengirim').val('');
                            return false;
                        }
                        $.each(data, function(i, object) {
                            map[object.nmpengirim] = object;
                            objects.push(object.nmpengirim);
                        });
                        process(objects);
                    }
                });
            }
        });
        $('#penerima').typeahead({
            source: function (query, process) {
                objects = [];
                map = {};
                $.ajax({
                    type: 'POST',
                    url: "<?php echo base_url(); ?>terima_barang/get_penerima",
                    data: "query="+query, 
                    dataType:"json",
                    success:function(data){
                        if (data == false){
                            $('#penerima').val('');
                            return false;
                        }
                        $.each(data, function(i, object) {
                            map[object.nmpenerima] = object;
                            objects.push(object.nmpenerima);
                        });
                        process(objects);
                    }
                });
                return false
            }
        });
    });
</script>