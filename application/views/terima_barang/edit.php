<div class="row">
    <div class="col-xs-12">
    <form method="post" action="<?php echo base_url('terima_barang/update');?>" id="tanda_terima" class="form-input">
        <div class="box">
            <div class="box-header">
                <h3>Form Tanda terima Barang</h3>
                <?php echo $this->session->flashdata('msg');?>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label class="control-label">No. Terima</label>
                            <input type="text" id="no_terima" tabindex="1" name="no_terima" maxlength="13"
                                value="<?php echo isset($rows[0]['NoTerima']) ? $rows[0]['NoTerima'] : null; ?>"
                                class="form-control" 
                            />
                            <?php echo form_error('no_terima'); ?>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Pengirim</label>
                            <select id="pengirim" tabindex="2" name="pengirim" maxlength="20"
                                autocomplete="off" class="form-control">
                                <option selected="true" value="<?php echo isset($rows[0]['nmpengirim']) ? $rows[0]['nmpengirim'] : null; ?>">
                                    <?php echo isset($rows[0]['nmpengirim']) ? $rows[0]['nmpengirim'] : null; ?>
                                </option>
                            </select>
                            <?php echo form_error('pengirim'); ?>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Penerima</label>
                            <select id="penerima" tabindex="3" name="penerima" maxlength="20"
                                autocomplete="off" class="form-control">
                                <option selected="true" value="<?php echo isset($rows[0]['nmpenerima']) ? $rows[0]['nmpenerima'] : null; ?>">
                                    <?php echo isset($rows[0]['nmpenerima']) ? $rows[0]['nmpenerima'] : null; ?>
                                </option>
                            </select>
                            <?php echo form_error('penerima'); ?>
                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="form-group">
                            <label class="control-label">Tanggal</label>
                            <input type="text" id="tanggal" name="tanggal" class="datepicker form-control"
                                   value="<?php echo isset($rows[0]['tglterima']) ? $rows[0]['tglterima'] : null; ?>"
                                   autocomplete="off">
                            <?php echo form_error('tanggal'); ?>
                        </div>
                        <div class="form-group">
                            <label>
                                <input type="checkbox" name="status" class="minimal"
                                <?php echo ($rows[0]['STATUS'] == 'LUNAS') ? 'checked' : '';?>
                                />
                                Lunas Bayar
                            </label>
                            <?php echo form_error('status'); ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered edit-table" 
                                id="terima-barang-detail">
                            <thead>
                                <tr>
                                    <th width="5%">BANYAK</th>
                                    <th width="5%">SATUAN</th>
                                    <th width="40%">JENIS BARANG</th>
                                    <th width="5%">JUMLAH</th>
                                    <th width="10%">KG/M3</th>
                                    <th width="15%">ONGKOS PER KG/M3</th>
                                    <th width="15%">JUMLAH ONGKOS</th>
                                    <th width="5%">ACTION</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="addrow">
                                    <td><input type="number" class="span12" name="banyak[]" id="banyak0"/></td>
                                    <td><input type="text" class="span12" name="satuan[]" id="satuan0"/></td>
                                    <td><input type="text" class="span12" name="jenis_barang[]" id="jenis0"/></td>
                                    <td><input type="number" class="span12" name="jumlah[]" id="jumlah0" autocomplete="off"/></td>
                                    <td>
                                        <select name="kg_m3[]" id="kg_m30" class="span12">
                                            <option value="KG">KG</option><option value="M3">M3</option>
                                        </select>
                                    </td>
                                    <td><input type="number" class="span12" name="ongkos[]" id="ongkos0" autocomplete="off"/></td>
                                    <td><input type="number" class="span12" name="jml_ongkos[]" id="jml_ongkos0" /></td>
                                    <td style="text-align: center;">
                                        <a id="addNew" href="javascript:void(0);"><i class="icon-plus-sign fa fa-plus-circle"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                        <button type="button" class="btn btn-default" 
                            onclick="self.history.back()">Kembali</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    </div>
</div>


