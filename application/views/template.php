<?php $this->load->view('layout/header.php'); ?>
<?php $this->load->view('layout/content-header.php'); ?>
<?php $this->load->view('layout/content-sidebar.php'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php
    if(!empty($page_title)){
       $controller1 = $page_title; 
    }else{
        $controller1 = $this->uri->segment(1);
        $controller1 = ucfirst(str_replace('_', ' ', $controller1));
    }
    $controller2 = ($this->uri->segment(2)) ? $this->uri->segment(2) : 'Index';
    
    ?>
    <section class="content-header">
        <h1>
            <?php echo $controller1;?>
            <small>
                <?php echo $controller2;?>
            </small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url('dashboard');?>"><i class="fa fa-dashboard"></i>Dashboard</a>
            </li>
            <li class="active">
                <a href="<?php echo base_url($this->uri->segment(1));?>"><?php echo $controller1;?></a>
            </li>
            <li class="active">
                <a href="<?php echo base_url($this->uri->segment(1) . $this->uri->segment(2));?>"><?php echo $controller2;?></a>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="modal modal-info fade" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Info</h4>
                    </div>
                    <div class="modal-body">
                        <p></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal modal-warning fade" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Warning</h4>
                    </div>
                    <div class="modal-body">
                        <p></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal modal-success fade" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Success</h4>
                    </div>
                    <div class="modal-body">
                        <p></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal modal-danger fade" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Error</h4>
                    </div>
                    <div class="modal-body">
                        <p></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view($content); ?>
    </section>
    <!-- /.content -->
</div>
<?php $this->load->view('layout/content-footer.php'); ?>
<?php $this->load->view('layout/footer.php'); ?>
