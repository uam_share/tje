<!DOCTYPE html>
<html>
    <head>
        <base href="<?php echo base_url(); ?>"/>
        <meta charset="utf-8"/>
        <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
        <title>
            MSA | Log in
        </title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"/>
        <!-- Bootstrap 3.3.7 -->
        <link href="assets/AdminLTE/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"/>
        <!-- Font Awesome -->
        <link href="assets/AdminLTE/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
        <!-- Ionicons -->
        <link href="assets/AdminLTE/bower_components/Ionicons/css/ionicons.min.css" rel="stylesheet"/>
        <!-- Theme style -->
        <link href="assets/AdminLTE/dist/css/AdminLTE.min.css" rel="stylesheet"/>
        <!-- iCheck -->
        <link href="assets/AdminLTE/plugins/iCheck/square/blue.css" rel="stylesheet"/>
        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic" rel="stylesheet"/>

        <link rel="stylesheet" type="text/css" href="assets/css/styles-new.css" />
    </head>
</html>
<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <a href="../../index2.html">
                <b>
                    MSA
                </b>
                EXPRESS
            </a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">
                Silahkan masuk
            </p>
            <form action="dashboard/dologin" method="post">
                <div class="form-group has-feedback">
                    <input class="form-control" name="tje_username" placeholder="Username" required="" type="text">
                        <span class="glyphicon glyphicon-user form-control-feedback">
                        </span>
                    </input>
                </div>
                <div class="form-group has-feedback">
                    <input class="form-control" name="tje_password" placeholder="Password" required="" type="password">
                        <span class="glyphicon glyphicon-lock form-control-feedback">
                        </span>
                    </input>
                </div>
                <div class="row">
                    <div class="col-xs-8">
                        <p class="text-red">
                            <?php echo $this->session->flashdata('msg');?>
                        </p>
                        
                        <!-- <div class="checkbox icheck">
                            <label>
                                <input type="checkbox">
                                    Remember Me
                                </input>
                            </label>
                        </div> -->
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <button class="btn btn-primary btn-block btn-flat" type="submit">
                            Masuk
                        </button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->
    <!-- jQuery 3 -->
    <script src="assets/AdminLTE/bower_components/jquery/dist/jquery.min.js">
    </script>
    <!-- Bootstrap 3.3.7 -->
    <script src="assets/AdminLTE/bower_components/bootstrap/dist/js/bootstrap.min.js">
    </script>
    <!-- iCheck -->
    <script src="assets/AdminLTE/plugins/iCheck/icheck.min.js">
    </script>
    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' /* optional */
            });
        });
    </script>
</body>
