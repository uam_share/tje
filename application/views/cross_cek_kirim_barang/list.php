<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">

                <div class="row">
                    <div class="col-sm-9 col-xs-12">
                        <h3 class="box-title">
                            <a href="<?php echo base_url('crosscek_kirim_barang/add');?>" class="btn btn-primary">Tambah</a>
                        </h3>
                    </div>
                   
                    <div class="col-sm-3 col-xs-12">
                        <div class="input-group pull-right">
                            <span class="input-group-addon filter">Filter</span>
                            <div class="input-group-btn select">
                                <select class="filter-el form-control" name="filter_bln">
                                    <option></option>
                                    <?php for ($i = 1; $i <= 12; $i++): ?>
                                        <option value="<?php echo $i; ?>" <?php echo (isset($filter_bln) && $filter_bln == $i) ? 'selected' : ''; ?>>
                                            <?php echo $this->fungsi->bulan2($i); ?>
                                        </option>
                                        <?php
                                    endfor;
                                    ?>
                                </select>
                            </div>
                            <!-- /btn-group -->
                            <select class="filter-el form-control" name="filter_thn">
                                <?php for ($i = 2010; $i <= 2050; $i++): ?>
                                    <option value="<?php echo $i; ?>" <?php echo (isset($filter_thn) && $filter_thn == $i) ? 'selected' : ''; ?>>
                                        <?php echo $i; ?>
                                    </option>
                                    <?php
                                endfor;
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <?php echo $this->session->flashdata('msg');?>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <table cellpadding="0" cellspacing="0" border="0" 
                    class="table table-striped table-bordered dt-responsive nowrap" id="dtTable">
                    <thead>
                        <tr>
                            <th width="1%">NO</th>
                            <th width="10%">TGL</th>
                            <!--<th width="10%">NO CEK</th>-->
                            <th width="10%">NO KIRIM</th>
                            <th width="10%">TGL KIRIM</th>
                            <th width="10%">NOPOL</th>
                            <th >KET</th>
                            <th width="15%">STATUS</th>
                            <th width="10%" class="no-sort">AKSI</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>