<div class="row">
    <div class="col-xs-12">
        <form method="post" action="<?php echo base_url();?>crosscek_kirim_barang/update" 
                id="tanda_terima" class="form-input">
            <div class="box">
                <div class="box-header">
                    
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-4 col-xs-6">
                            <div class="form-group">
                                <label class="control-label">No</label>
                                <input type="text" id="m_cek_no" name="m_cek_no" class="form-control" 
                                    value="<?php echo $rowedit->m_cek_no; ?>"/>
                                <?php echo form_error('m_cek_no'); ?>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Tanggal</label>
                                <input type="text" id="m_cek_tgl" name="m_cek_tgl" 
                                    class="form-control datepicker" required=""
                                    value="<?php echo $rowedit->m_cek_tgl; ?>"autocomplete="off"/>
                                <?php echo form_error('m_cek_tgl'); ?>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-6">
                            <div class="form-group">
                                <label class="control-label">No. Kirim</label>
                                <input type="text" id="m_cek_fk" name="m_cek_fk" class="form-control typeahead" 
                                    value="<?php echo $rowedit->m_cek_fk; ?>" autocomplete="off" />
                                <?php echo form_error('m_cek_fk'); ?>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Tgl Kirim</label>
                                <input type="text" id="tgl_kirim" name="tgl_kirim" class="form-control" 
                                    value="<?php echo $rowedit->TGLKIRIM; ?>" readonly="" />
                                <?php echo form_error('tgl_kirim'); ?>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">No. Polisi</label>
                                <input type="text" id="no_polisi" name="no_polisi" class="form-control" 
                                    value="<?php echo $rowedit->NOPOL; ?>" readonly="" />
                                <?php echo form_error('no_polisi'); ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <table cellpadding="0" cellspacing="0" border="0" 
                                class="table table-striped table-bordered" id="cek-kirim-detail">
                                <thead>
                                    <tr>
                                        <th width="1%">NO</th>
                                        <th width="10%">NO.STT</th>
                                        <th width="10%">KODE</th>
                                        <th width="10%">BARANG</th>
                                        <th width="10%">BANYAK</th>
                                        <th width="10%">SATUAN</th>
                                        <th width="25%">PENGIRIM</th>
                                        <th width="25%">PENERIMA</th>
                                        <th width="35%">KET</th>
                                        <th width="10%" style="text-align: center;">
                                            All
                                            <input type="checkbox" id="parent-check" />
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                            <button type="button" class="btn btn-default" 
                                onclick="self.history.back()">Kembali</button>
                            <!-- <button type="button" class="btn btn-warning" id="batal" onclick="">Batal</button> -->
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
