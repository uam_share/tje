<!-- /.content-wrapper -->
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>
            Version
        </b>
        2.0.0
    </div>
    <strong>
        Copyright © 2018
        <a href="<?php echo base_url(); ?>">
            MSA EXPRESS
        </a>
    </strong>
</footer>