<header class="main-header">
    <!-- Logo -->
    <a class="logo" href="<?php echo base_url('/');?>">
        <span class="logo-mini"><b>MSA</b></span>
        <span class="logo-lg">
            <b>
                MSA
            </b>
            EXPRESS
        </span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a class="sidebar-toggle" data-toggle="push-menu" href="#" role="button">
            <span class="sr-only">
                Toggle navigation
            </span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="<?php echo base_url('status_barang');?>">
                        <i class="fa fa-search"></i>Status Barang
                    </a>
                </li>
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user-circle">
                            <span class="hidden-xs">
                                <?php echo $this->session->userdata(SESS_PREFIK . 'nama'); ?>
                            </span>
                        </i>
                        <!-- <img alt="User Image" class="user-image" src="assets/img/avatar.png">
                            
                        </img> -->
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <i class="fa fa-user-circle fa-2x">
                                <p><?php echo $this->session->userdata(SESS_PREFIK . 'nama'); ?></p>
                            </i>
                            <!-- <img alt="User Image" class="img-circle" src="assets/img/avatar.png">
                                <p><?php // echo $this->session->userdata(SESS_PREFIK . 'nama'); ?></p>
                            </img> -->
                        </li>
                        <!-- Menu Body -->
                        <li class="user-body"></li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a class="btn btn-default btn-flat" href="<?php echo base_url('users/password');?>">
                                    Ubah Password
                                </a>
                            </div>
                            <div class="pull-right">
                                <a class="btn btn-default btn-flat" href="<?php echo base_url('dashboard/dologout');?>">
                                    Log Out
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>

            </ul>
        </div>
    </nav>
</header>