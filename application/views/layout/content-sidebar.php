<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <?php
    $CI = & get_instance();
    $CI->load->model('menu_m', 'menu');
    $menus = $CI->menu->getMenuTopAsArray();
    unset($CI);

    $controller = $this->uri->segment(1);
    if($this->uri->segment(2) && !in_array($this->uri->segment(2), array('add','edit','form'))){
        $controller = $controller . '/' . $this->uri->segment(2);
    }
    
    ?>
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">
                MENU APLIKASI
            </li>
            <?php
            if (count($menus) > 0) {
                foreach ($menus as $k => $menu) { 
                    $active = ($controller == $menu['controller'] && $menu['controller'] !='') ? 'active' : '';
                    if(count($menu['childs']) > 0){
                        foreach ($menu['childs'] as $k => $child) {
                            if(($controller == $child['controller'])){
                                $active = 'active';
                                break;
                            }
                        }
                    }
                ?>
                    <li class="<?php echo $active;?> treeview">
                        <a href="#">
                            <i class="<?php echo $menu['icon_cls'];?>">
                            </i>
                            <span>
                                <?php echo $menu['menu'];?>
                            </span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <?php
                        if(count($menu['childs']) > 0){?>
                        <ul class="treeview-menu">
                            <?php
                            foreach ($menu['childs'] as $k => $child) { ?>
                                <li class="<?php echo ($controller == $child['controller']) ? 'active' : '';?>">
                                    <a href="<?php echo $child['controller'];?>">
                                        <i class="fa fa-circle-o"></i>
                                        <?php echo $child['menu'];?>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                        <?php
                        }
                        ?>
                    </li>
                <?php }
            }
            ?>
            
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>