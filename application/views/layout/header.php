<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<base href="<?php echo base_url(); ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>MSA EXPRESS</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" href="assets/AdminLTE/bower_components/bootstrap/dist/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="assets/AdminLTE/bower_components/font-awesome/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="assets/AdminLTE/bower_components/Ionicons/css/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="assets/AdminLTE/dist//css/AdminLTE.min.css">
	<!-- AdminLTE Skins. Choose a skin from the css/skins
	   folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="assets/AdminLTE/dist//css/skins/_all-skins.min.css">
	<!-- Morris chart -->
	<!-- <link rel="stylesheet" href="assets/AdminLTE/bower_components/morris.js/morris.css"> -->
	<!-- jvectormap -->
	<!-- <link rel="stylesheet" href="assets/AdminLTE/bower_components/jvectormap/jquery-jvectormap.css"> -->
	<!-- Date Picker -->
	<link rel="stylesheet" href="assets/AdminLTE/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
	<!-- Daterange picker -->
	<!-- <link rel="stylesheet" href="assets/AdminLTE/bower_components/bootstrap-daterangepicker/daterangepicker.css"> -->
	<!-- bootstrap wysihtml5 - text editor -->
	<!-- <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"> -->

	<!-- Select2 -->
  	<link rel="stylesheet" href="assets/AdminLTE/bower_components/select2/dist/css/select2.min.css">
  	<!-- iCheck for checkboxes and radio inputs -->
  	<link rel="stylesheet" href="assets/AdminLTE/plugins/iCheck/all.css">
  	<link rel="stylesheet" href="assets/AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  	<link rel="stylesheet" type="text/css" href="assets/AdminLTE/bower_components/datatables.net-bs/css/responsive.bootstrap.min.css">

	<!-- Google Font -->
	<link rel="stylesheet" 
		href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
	

	<!-- jQuery 3 -->
	<script src="assets/AdminLTE/bower_components/jquery/dist/jquery.min.js"></script>
    
    <!-- plugins -->
	<script type="text/javascript" language="javascript" src="media/js/jquery.jeditable.js"></script>
	<script src="assets/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="assets/AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
	<script type="text/javascript" language="javascript" src="assets/AdminLTE/bower_components/datatables.net-bs/js/dataTables.responsive.min.js"></script>
	<script type="text/javascript" language="javascript" src="assets/AdminLTE/bower_components/datatables.net-bs/js/responsive.bootstrap.min.js"></script>
	

	<!-- <script type="text/javascript" language="javascript" src="media/js/jquery.dataTables.js"></script> -->
    <!-- <script type="text/javascript" charset="utf-8" language="javascript" src="media/DT_bootstrap.js"></script> -->

    <link rel="stylesheet" type="text/css" href="assets/css/styles-new.css" />
    
    <script>
        var BASEURL = "<?php echo base_url(); ?>";
        function deleteData(obj){
            return window.confirm("Data akan dihapus ?");
        }
        function toRP(angka){
            var rev = parseInt(angka,10).toString().split('').reverse().join('');
            var rev2 = '';
            for(var i=0;i<rev.length;i++){
                rev2 += rev[i];
                if((i + 1) % 3 === 0 && i !==(rev.length - 1)){
                    rev2 += '.';
                }
            }
            return rev2.split('').reverse().join('');
        }
        // $(document).ready(function(){
        //     $(".img-loading").ajaxStart(function(){
        //         $(this).show();
        //     }); 
        //     $(".img-loading").ajaxStop(function(){
        //         $(this).hide();
        //     }); 
        // });
    </script>
</head>
<body class="hold-transition skin-green sidebar-mini sidebar-collapse">
<div class="wrapper">
	