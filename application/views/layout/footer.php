</div>
<!-- ./wrapper -->


<script>
  // $.widget.bridge('uibutton', $.ui.button);
</script>

<!-- jQuery 3 -->
<!-- <script src="assets/AdminLTE/bower_components/jquery/dist/jquery.min.js"></script> -->
<!-- Bootstrap 3.3.7 -->
<script src="assets/AdminLTE/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<!-- <script src="assets/AdminLTE/bower_components/raphael/raphael.min.js"></script> -->
<!-- <script src="assets/AdminLTE/bower_components/morris.js/morris.min.js"></script> -->
<!-- Sparkline -->
<!-- <script src="assets/AdminLTE/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script> -->
<!-- jvectormap -->
<!-- <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script> -->
<!-- <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script> -->
<!-- jQuery Knob Chart -->
<!-- <script src="assets/AdminLTE/bower_components/jquery-knob/dist/jquery.knob.min.js"></script> -->
<!-- daterangepicker -->
<!-- <script src="assets/AdminLTE/bower_components/moment/min/moment.min.js"></script> -->
<!-- <script src="assets/AdminLTE/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script> -->
<!-- datepicker -->
<script src="assets/AdminLTE/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<!-- <script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script> -->
<!-- Slimscroll -->
<!-- <script src="assets/AdminLTE/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script> -->
<!-- FastClick -->
<!-- <script src="assets/AdminLTE/bower_components/fastclick/lib/fastclick.js"></script> -->
<!-- AdminLTE App -->
<!-- Select2 -->
<script src="assets/AdminLTE/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="assets/AdminLTE/plugins/iCheck/icheck.min.js"></script>
<script src="assets/AdminLTE/bower_components/bootstrap3-typeahead/bootstrap3-typeahead.min.js"></script>

<!-- Jquery Tree View -->
<link rel="stylesheet" type="text/css" href="assets/css/jquery.treeview.css" />
<script type="text/javascript" src="assets/js/plugins/jquery.treeview.js"></script>

<script src="assets/AdminLTE/dist/js/adminlte.min.js"></script>

<script src="assets/js/app/app.js"></script>


<?php
$filename = 'assets/js/app/' . $this->uri->segment(1);
if($this->uri->segment(2)){
    $filename = $filename . '/' . $this->uri->segment(2) . '.js';
}else{
	$filename = $filename . '/index.js';
}
if (file_exists($filename)) {
    echo '<script src="' . $filename . '?' . filemtime($filename) . '"></script>';
}
?>
</body>
</html>