<div class="navbar">
    <div class="navbar-inner">
        <div class="container">

            <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            <!-- Be sure to leave the brand out there if you want it shown -->
            <!--<a class="brand" href="#">Project name</a>-->

            <!-- Everything you want hidden at 940px or less, place within here -->
            <div class="nav-collapse collapse">
                <?php
                $CI = & get_instance();
                $CI->load->model('menu_m', 'menu');
                echo $CI->menu->getMenuTop();
                unset($CI);
                ?>  

                <ul class="nav pull-right">
                    <li class="divider-vertical"></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <?php echo " Selamat Datang, " . $this->session->userdata(SESS_PREFIK . 'nama'); ?>
                            <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href='users/password'><i class="icon-lock"></i> Ubah Password</a></li>
                            <li><a href='dashboard/dologout'><i class="icon-off"></i> Logout</a></li>
                        </ul>
                    </li>
                </ul>

            </div>
        </div>
    </div>
</div>