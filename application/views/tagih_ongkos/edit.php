<div class="row">
    <div class="col-xs-12">
    <form method="post" action="<?php echo base_url('tagih_ongkos/update'); ?>" 
            id="tanda_terima" class="form-input">
        <div class="box">
            <div class="box-header">
                <?php echo $this->session->flashdata('msg');?>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">No Kirim</label>
                            <input type="text" id="no_kirim" tabindex="1" name="no_kirim" maxlength="13" 
                                class="form-control" value="<?php echo $rowedit->NOKIRIM; ?>" readonly
                            />
                            <?php echo form_error('no_kirim'); ?>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Tanggal</label>
                            <input type="text" id="tanggal" name="tanggal" class="datepicker form-control"
                                   value="<?php echo $rowedit->TGLKIRIM; ?>" autocomplete="off">
                                <?php echo form_error('tanggal'); ?>
                        </div>
                    </div>

                    <div class="col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">No. Polisi</label>
                            <input type="text" id="no_polisi" name="no_polisi" class="form-control"
                                value="<?php echo $rowedit->NOPOL; ?>" readonly autocomplete="off"/>
                            <?php echo form_error('no_polisi'); ?>
                        </div>
                        <div class="form-group">
                            <div class="form-group">
                                <label class="control-label">Keterangan</label>
                                <textarea name="keterangan" id="keterangan" 
                                    class="form-control"><?php echo $rowedit->KETERANGAN; ?></textarea> 
                                <?php echo form_error('keterangan'); ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <table cellpadding="0" cellspacing="0" border="0" 
                            class="table table-striped table-bordered" id="tagih-ongkos-detail">
                            <thead>
                                <tr>
                                    <th width="10%">NO.STT</th>
                                    <th width="7%">KODE</th>
                                    <th width="5%">BANYAK</th>
                                    <th width="10%">SATUAN</th>
                                    <th width="20%">PENGIRIM</th>
                                    <th width="20%">PENERIMA</th>
                                    <th width="10%">STATUS</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                        <button type="button" class="btn btn-default" 
                            onclick="self.history.back()">Kembali</button>
                        <button type="button" class="btn btn-warning" id="batal" onclick="">Batal</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    </div>
</div>
