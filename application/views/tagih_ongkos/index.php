<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="box-title">
                            <a href="<?php echo base_url('tagih_ongkos/add');?>" class="btn btn-primary">Tambah</a>
                        </h3>
                        <br />
                        <?php echo $this->session->flashdata('msg');?>
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-sm-3 col-xs-12">
                         <div class="input-group">
                            <input class="form-control" 
                                id="datefrom0" size="80" type="text" value="<?php echo date('Y-m-d'); ?>" />
                            <span class="input-group-addon addon1"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12">
                        <div class="input-group">
                            <input class="form-control" id="dateto0" size="16" type="text" value="<?php echo date('Y-m-d'); ?>">
                            <span class="input-group-addon addon2"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <table cellpadding="0" cellspacing="0" border="0" 
                    class="table table-striped table-bordered dt-responsive nowrap" id="dtTable">
                    <thead>
                        <tr>
                            <th width="3%">No</th>
                            <th width="15%">No. Kirim</th>
                            <th width="15%">Tgl Kirim</th>
                            <th width="15%">No. Polisi</th>
                            <th width="45%">Keterangan</th>
                            <th width="5%" class="no-sort">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
