<div class="row">
	<div class="col-xs-12">
	<form class="form-input" method="post" id="form-penerima">
		<div class="box">
			<div class="box-header">
				<h3>Form Kendaraan</h3>
				<?php echo $this->session->flashdata('msg');?>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-xs-6">
						<div class="form-group">
							<label class="control-label">Nopol</label>
							<?php
							$value = "";
							$class = 'class=" form-control"';
							if (!empty($result[0]['nopol'])){
								$value = $result[0]['nopol'];
								$class = 'class="uneditable-input" readonly="true"';
							}
							?>
							<input type="text" id="nopol" name="nopol" class="form-control" maxlength=10
								<?php echo $class;?> value="<?php echo $value;?>">
							<?php echo form_error('nopol'); ?>
					    </div>
						<div class="form-group">
							<label class="control-label">Merk</label>
							<input type="text" id="merk" name="merk" class="form-control" maxlength=10
								value="<?php echo isset($result[0]['merk'])?$result[0]['merk']:null;?>">
							<?php echo form_error('merk'); ?>
					    </div>
						<div class="form-group">
							<label class="control-label">Jenis</label>
							<input type="text" id="jenis" name="jenis" class="form-control" maxlength=10
								value="<?php echo isset($result[0]['jenis'])?$result[0]['jenis']:null;?>">
							<?php echo form_error('jenis'); ?>
					    </div>
						<div class="form-group">
							<label class="control-label">Tahun</label>
							<input type="text" id="tahun" name="tahun" class="form-control" maxlength=10
								value="<?php echo isset($result[0]['tahun'])?$result[0]['tahun']:null;?>">
					    </div>
						<div class="form-group">
							<label class="control-label">Warna</label>
							<input type="text" id="warna" name="warna" class="form-control" maxlength=10
								value="<?php echo isset($result[0]['warna'])?$result[0]['warna']:null;?>">
					    </div>
					</div>

					<div class="col-xs-6">
						<div class="form-group">
							<label class="control-label">Rangka</label>
							<input type="text" id="rangka" name="rangka" class="form-control" maxlength=10
								value="<?php echo isset($result[0]['rangka'])?$result[0]['rangka']:null;?>">
					    </div>
						<div class="form-group">
							<label class="control-label">Mesin</label>
							<input type="text" id="mesin" name="mesin" class="form-control" maxlength=10
								value="<?php echo isset($result[0]['mesin'])?$result[0]['mesin']:null;?>">
					    </div>
						<div class="form-group">
							<label class="control-label">BPKB</label>
							<input type="text" id="bpkb" name="bpkb" class="form-control" maxlength=10
								value="<?php echo isset($result[0]['bpkb'])?$result[0]['bpkb']:null;?>">
					    </div>
						<div class="form-group">
							<label class="control-label">Supir</label>
							<input type="text" id="supir" name="supir" class="form-control" maxlength=10
								value="<?php echo isset($result[0]['supir'])?$result[0]['supir']:null;?>">
					    </div>
						<div class="form-group">
							<label class="control-label">Keterangan</label>
							<textarea rows="2" name="ket" id="ket" class="form-control" maxlength=10>
								<?php echo isset($result[0]['ket'])?$result[0]['ket']:null;?></textarea>
					    </div>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<div class="row">
					<div class="col-xs-12">
						<button type="submit" class="btn btn-primary pull-right">Simpan</button>
						<button type="button" class="btn btn-default" 
							onclick="self.history.back()">Kembali</button>
					</div>
			    </div>
			</div>
		</div>
	</form>
	</div>
</div>