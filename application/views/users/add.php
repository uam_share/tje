<div class="row">
    <div class="col-xs-12">
    <form class="form-input" action="users/create" method="post" id="form-users">
        <div class="box">
            <div class="box-header">
                <h3>Form User</h3>
                <?php echo $this->session->flashdata('msg');?>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label class="control-label">Nama Lengkap</label>
                            <input name="user_name" required type="text" class="form-control" 
                                id="user_name" placeholder="Nama Lengkap" value="">
                            <?php echo form_error('user_name'); ?>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Username</label>
                            <input name="user_username" required type="text" class="form-control" 
                                id="user_username" placeholder="Username" value="">
                            <?php echo form_error('user_username'); ?>
                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="form-group">
                            <label class="control-label">E-mail</label>
                            <input name="e_mail" value="" required type="email" class="form-control" 
                                id="e_mail" placeholder="E-mail">
                            <?php echo form_error('e_mail'); ?>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Password</label>
                            <input name="user_password" value="" required type="password" class="form-control" 
                                id="user_password" placeholder="Password">
                            <?php echo form_error('user_password'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                        <button type="button" class="btn btn-default" 
                            onclick="self.history.back()">Kembali</button>
                        <button class="btn btn-default" type="reset"><i class="icon-refresh"></i>Set Ulang</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    </div>
</div>