
<div class="row">
    <div class="col-xs-12">
    <form class="form-input" action="users/updatepass" method="post" id="form-users">
        <div class="box">
            <div class="box-header">
                <h3>Ganti Password</h3>
                <?php echo $this->session->flashdata('msg');?>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label class="control-label">Password Lama</label>
                            <input type="password" required name="old_password" 
                                placeholder="Isi password baru" value="" class="form-control" id="inputNewPassword" />
                            <?php echo form_error('password'); ?>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Password Baru</label>
                            <input type="password" required name="newpassword" placeholder="Isi password baru" 
                                value="" class="form-control" id="newpassword" />
                            <?php echo form_error('newpassword'); ?>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Konfirmasi Password Baru</label>
                            <input type="password" required name="confirmpassword" placeholder="Konfirmasi Password Baru" 
                                value="" class="form-control" id="confirmpassword" />
                            <?php echo form_error('confirmpassword'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                        <button type="button" class="btn btn-default" 
                            onclick="self.history.back()">Kembali</button>
                        <button class="btn btn-default" type="reset"><i class="icon-refresh"></i>Set Ulang</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    </div>
</div>