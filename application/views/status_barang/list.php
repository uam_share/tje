<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                
            </div>

            <div class="box-body">
                <div class="row">
                    <form action="status_barang/caristatus" method="post" id="form-cari-no">
                        <!-- <div class="col-xs-12"> -->
                            <div class="col-sm-4 col-xs-12">
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <?php
                                            if(isset($nosearch)){
                                                var_dump($nosearch);
                                                $filterTitle = 'No Barang';
                                            }
                                            if(isset($nmpengirim)){
                                                var_dump($nmpengirim);
                                                $filterTitle = 'Pengirim';
                                            }
                                            if(isset($nmpenerima)){
                                                var_dump($nmpenerima);
                                                $filterTitle = 'Penerima';
                                            }
                                        ?>
                                        <button id="filter-by" type="button" class="btn btn-info dropdown-toggle" 
                                            data-toggle="dropdown" aria-expanded="false">
                                            <span id="filter-title"><?php echo $filterTitle;?></span>
                                            <span class="fa fa-caret-down"></span>
                                        </button>
                                        <input id="filter_no" type="hidden" name="filter_no" class="form-control" />
                                        <ul id="filter-item" class="dropdown-menu">
                                            <li><a href="#" value="noterima">No Barang</a></li>
                                            <li><a href="#" value="nmpengirim">Pengirim</a></li>
                                            <li><a href="#" value="nmpenerima">Penerima</a></li>
                                        </ul>
                                    </div>
                                    <!-- /btn-group -->
                                    <input type="text" name="nosearch" id="nosearch" placeholder="No Terima Barang" 
                                            class="form-control"
                                           style="<?php echo isset($nosearch) ? '' : 'display: none;'; ?>"
                                           value="<?php echo isset($nosearch) ? $nosearch : ''; ?>"
                                    />
                                    <input type="text" id="pengirim" name="pengirim" placeholder="Masukan Nama Pengirim" 
                                            class="form-control"
                                           style="<?php echo isset($nmpengirim) ? '' : 'display: none;'; ?>" 
                                           value="<?php echo isset($nmpengirim) ? $nmpengirim : ''; ?>" autocomplete="off"
                                    />
                                    <input type="text" id="penerima" name="penerima" placeholder="Masukan Nama Penerima"
                                            class="form-control"
                                           style="<?php echo isset($nmpenerima) ? '' : 'display: none;'; ?>"
                                           value="<?php echo isset($nmpenerima) ? $nmpenerima : ''; ?>" autocomplete="off"
                                    />
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-4 col-xs-12 col-sm-offset-4">
                                <div class="input-group pull-right">
                                    <span class="input-group-addon filter">Filter</span>
                                    <div class="input-group-btn select">
                                        <select name="filter_bln" class="form-control">
                                            <?php for ($i = 1; $i <= 12; $i++): ?>
                                                <option value="<?php echo $i; ?>" <?php echo (isset($filter_bln) && $filter_bln == $i) ? 'selected' : ''; ?>>
                                                    <?php echo $this->fungsi->bulan2($i); ?>
                                                </option>
                                                <?php
                                            endfor;
                                            ?>
                                        </select>
                                    </div>
                                    
                                    <select name="filter_thn" class="form-control">
                                        <?php for ($i = 2010; $i <= 2050; $i++): ?>
                                            <option value="<?php echo $i; ?>" <?php echo (isset($filter_thn) && $filter_thn == $i) ? 'selected' : ''; ?>>
                                                <?php echo $i; ?>
                                            </option>
                                            <?php
                                        endfor;
                                        ?>
                                    </select>
                                </div>
                            </div>
                        <!-- </div> -->
                    </form>
                </div>
                
                <div class="row">
                    <div class="col-sm-12">
                        <?php if (!empty($msg)) { ?>
                            <div class="alert alert-warning alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <?php echo $msg; ?>
                            </div>
                        <?php
                        }
                        ?>

                        <table class="table table-striped table-bordered dt-responsive nowrap" id="table-search"
                               style="font-size: 16px;width:100%">
                            <thead>
                                <tr>
                                    <th width="5%" data-priority="1">KODE</th>
                                    <th width="7%" data-priority="1">No. Muat</th>
                                    <th width="10%" data-priority="1">No. Kirim</th>
                                    <th width="10%" data-priority="1">Nopol</th>
                                    <th width="10%" data-priority="2">Barang</th>
                                    <th width="1%" data-priority="2">BYK</th>
                                    <th width="1%" data-priority="2">Satuan</th>
                                    <th width="10%" data-priority="2">Jumlah</th>
                                    <th width="10%" data-priority="1">Status</th>
                                    <th width="25%" data-priority="2">Nama Pengirim</th>
                                    <th width="25%" data-priority="2">Nama Penerima</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($hasil) && $hasil):
                                    foreach ($querydata as $rdata):
                                        ?>
                                        <tr>
                                            <td><?php echo '<b>' . $rdata->noterima . '</b> - ' . $rdata->tglterima; ?></td>
                                            <td><?php echo '<b>' . $rdata->nomuat . '</b> - ' . $rdata->tglmuat; ?></td>
                                            <td><?php echo '<b>' . $rdata->nokirim . '</b> - ' . $rdata->tglkirim; ?></td>
                                            <td><?php echo $rdata->nopol; ?></td>
                                            <td><?php echo $rdata->barang; ?></td>
                                            <td><?php echo $rdata->banyak; ?></td>
                                            <td><?php echo $rdata->satuan; ?></td>
                                            <td><?php echo $rdata->jumlah; ?></td>
                                            <td><?php echo $rdata->status_barang; ?></td>
                                            <td><?php echo $rdata->nmpengirim; ?></td>
                                            <td><?php echo $rdata->nmpenerima; ?></td>
                                            
                                        </tr>
                                        <?php
                                    endforeach;
                                endif;
                                ?>
                            </tbody>
                        </table> 
                    </div>
                </div>
                     
            </div>
        </div>
    </div>
</div>