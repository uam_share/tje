<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <base href="<?php echo base_url(); ?>" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>MSA EXPRESS</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="assets/css/print/bootstrap.min.css">

  <!-- <link rel="stylesheet" href="assets/css/print/AdminLTE.min.css">  -->
  <link rel="stylesheet" href="assets/css/print/print-styles.css">
  <style type="text/css" media="print">
    @media print{
      /*@page {size: landscape}*/
      @page {
        /*size: 'A4';*/
        margin: 3cm 0.5cm 1.5cm 1.5cm;
      }
    }
  </style>
</head>
<body style="background : #CCC" onload="window.print();">
<!-- <body style="background : #CCC" > -->
<div class="wrapper">
  <section class="invoice">
    <?php $this->load->view($content); ?>
  </section>
</div>
</body>
</html>
