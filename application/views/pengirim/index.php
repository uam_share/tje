<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
              	<h3 class="box-title">
              		<a href="<?php echo base_url();?>pengirim/form/" class="btn btn-primary">Tambah</a>
              	</h3>
            </div>
            <div class="box-body">
				<?php echo $this->session->flashdata('msg');?>
				<table cellpadding="0" cellspacing="0" border="0" 
					class="table table-striped table-bordered dt-responsive nowrap" id="dtTable">
					<thead>
						<tr>
							<th width="3%">No</th>
							<th width="25%">Nama pengirim</th>
							<th width="20%">Alamat</th>
							<th width="15%">Telp</th>
							<th width="20%">Email</th>
							<th width="5%" class="no-sort">Action</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
            </div>
		</div>
	</div>
</div>
