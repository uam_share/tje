<div class="row">
	<div class="col-xs-12">
	<form class="form-input" method="post" id="form-pengirim">
		<div class="box">
			<div class="box-header">
				<h3>Form Pengirim</h3>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-xs-6">
		                <div class="form-group">
							<label class="control-label">Nama pengirim</label>
							<?php
								$value = "";
								$class = 'class="uneditable-input form-control"';
								if (isset($result[0]['nmpengirim'])){
									$value = $result[0]['nmpengirim'];
									$class = 'class="uneditable-input form-control" readonly="true"';
								}
							?>
							<input type="text" id="nmpengirim" name="nmpengirim" 
								<?php echo $class;?> maxlength=20
								required=""
								value="<?php echo $value;?>"/>

							<?php echo form_error('nmpengirim'); ?>
					    </div>

					    <div class="form-group">
							<label class="control-label">Alamat</label>
							<textarea rows="2" id="alamat" class="form-control" maxlength=50
								name="alamat"><?php echo isset($result[0]['alamat'])?$result[0]['alamat']:null;?></textarea>
					    </div>
					    <div class="form-group">
							<label class="control-label">Domisili</label>
							<input type="text" id="domisili" class="form-control" maxlength=50
								name="domisili" 
								value="<?php echo isset($result[0]['domisili'])?$result[0]['domisili']:null;?>"/>
							<?php echo form_error('domisili'); ?>
					    </div>

					</div>

					<div class="col-xs-6">
						<div class="form-group">
							<label class="control-label">Telepon</label>
							<input type="number" id="telp" class="form-control" maxlength=15
								name="telp" 
								value="<?php echo isset($result[0]['telp'])?$result[0]['telp']:null;?>"/>
								<?php echo form_error('telp'); ?>
					    </div>
						<div class="form-group">
							<label class="control-label">Faximili</label>
							<input type="number" id="fax" name="fax" maxlength=15 class="form-control"
								value="<?php echo isset($result[0]['fax'])?$result[0]['fax']:null;?>"/>
								<?php echo form_error('fax'); ?>
					    </div>
						<div class="form-group">
							<label class="control-label">Email</label>
							<input type="text" id="email" 
								name="email" maxlength=25 class="form-control"
								value="<?php echo isset($result[0]['email'])?$result[0]['email']:null;?>"/>
								<?php echo form_error('email'); ?>
					    </div>
						<div class="form-group">
							<label class="control-label">Attn</label>
							<input type="text" id="attn" 
								name="attn" maxlength=25 class="form-control"
								value="<?php echo isset($result[0]['attn'])?$result[0]['attn']:null;?>"/>
								<?php echo form_error('attn'); ?>
					    </div>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<div class="row">
					<div class="col-xs-12">
						<button type="submit" class="btn btn-primary pull-right">Simpan</button>
						<button type="button" class="btn btn-default" 
							onclick="self.history.back()">Kembali</button>
					</div>
			    </div>
			</div>
		</div>
	</form>
	</div>
</div>