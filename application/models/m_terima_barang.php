<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class M_terima_barang extends CI_Model
{
    protected $dbActive;

    public function __construct()
    {
        parent::__construct();
        $this->dbActive = $this->load->database('tjedb', true); // $this->db
    }

    public function getDb()
    {
        return $this->dbActive;
    }

    public function getTypehead($table, $field, $query)
    {
        $query = $this->dbActive->query("SELECT $field FROM $table WHERE $field LIKE '%$query%'");
        return ($query->num_rows() > 0) ? $query->result_array() : false;
    }

    public function getAsList($query, $columns = "NoTerima as no_terima")
    {
        $query = $this->dbActive->query("SELECT $columns FROM mtt_antri
                                WHERE NoTerima LIKE '%$query%'");
        return ($query->num_rows() > 0) ? $query->result_array() : false;
    }

    public function checkNoTerima($noterima)
    {
        $query = $this->dbActive->query("SELECT * FROM mtt_antri WHERE NoTerima = '$noterima'");
        return $query->num_rows();
    }

    public function getData($sWhere, $sOrder, $sLimit)
    {
        $result = array();
        $query = "SELECT * FROM mtt_antri $sWhere ";

        $sqlX = $this->dbActive->query($query);
        $result['total'] = $sqlX->num_rows();

        $sqlY = $this->dbActive->query($query . "$sOrder $sLimit");
        $result['data'] = $sqlY->result_array();

        return $result;
    }

    public function getRows($noterima)
    {
        $query = $this->dbActive->query("SELECT * FROM mtt_antri WHERE NoTerima = '$noterima'");
        return ($query->num_rows() > 0) ? $query->result_array() : false;
    }

    public function getDetailAntri($noterima)
    {
        $query = $this->dbActive->query("select a.NoTerima, a.tglterima, a.nmpengirim, a.nmpenerima, a.`STATUS`, b.alamat
                from mtt_antri a left join m_pengirim b on a.nmpengirim=b.nmpengirim
                where a.NoTerima = '$noterima'");
        return ($query->num_rows() > 0) ? $query->result_array() : false;
    }

    public function getGridAntri($noterima)
    {
        $query = $this->dbActive->query("select a.BANYAK,a.Satuan,a.Barang, round(JUMLAH,2) as JUMLAH, a.SAT,
                 a.Ongkos,a.Jml_ongkos
                 from tt_antri a where a.NoTerima = '$noterima'");
        return ($query->num_rows() > 0) ? $query->result_array() : false;
    }

    public function getReport($noterima)
    {
        $sql = "SELECT CONCAT(mtt_antri.NOTERIMA,' ')AS NOTERIMA,mtt_antri.TGLTERIMA,m_pengirim.NMPENGIRIM,(m_pengirim.ALAMAT)AS ALAMAT1,m_penerima.NMPENERIMA,(m_penerima.ALAMAT)AS ALAMAT2,
            tt_antri.BANYAK,tt_antri.BANYAK,tt_antri.SATUAN,tt_antri.BARANG,tt_antri.JUMLAH,tt_antri.SAT,tt_antri.STATUS
            FROM (mtt_antri INNER JOIN tt_antri ON mtt_antri.NOTERIMA=tt_antri.NOTERIMA),m_penerima,m_pengirim
            WHERE mtt_antri.NMPENGIRIM=m_pengirim.NMPENGIRIM AND mtt_antri.NMPENERIMA=m_penerima.NMPENERIMA AND
            mtt_antri.NOTERIMA='$noterima' AND tt_antri.STATUS='ANTRI'";
        return $sql;
    }
    public function saveTtAntri($no, $request)
    {
        if (isset($request['banyak']) && count($request['banyak']) > 0) {
            $jml = count($request['banyak']);
            for ($i = 0; $i < $jml; $i++) {
                $data[$i] = array(
                    'Noterima' => $no,
                    'BANYAK' => !empty($request['banyak'][$i]) ? $request['banyak'][$i] : 0,
                    'Satuan' => $request['satuan'][$i],
                    'Barang' => $request['jenis_barang'][$i],
                    'JUMLAH' => !empty($request['jumlah'][$i]) ? $request['jumlah'][$i] : 0,
                    'SAT' => $request['kg_m3'][$i],
                    'STATUS' => 'ANTRI',
                    'Ongkos' => !empty($request['ongkos'][$i]) ? $request['ongkos'][$i] : '',
                    'jml_ongkos' => !empty($request['jml_ongkos'][$i]) ? $request['jml_ongkos'][$i] : 0,
                );
                $data[$i]['jml_ongkos'] = str_replace(".", "", $data[$i]['jml_ongkos']);
            }
        }

        return $this->dbActive->insert_batch('tt_antri', $data);
    }

    public function add($request)
    {
        $this->dbActive->trans_start();
        $msg = array();
        $status = isset($request['status']) ? "LUNAS" : "";
        $data = array(
            'NoTerima' => $request['no_terima'],
            'tglterima' => $request['tanggal'],
            'nmpenerima' => $request['penerima'],
            'nmpengirim' => $request['pengirim'],
            'STATUS' => $status,
        );
        $query = $this->dbActive->insert('mtt_antri', $data);
        if ($query) {
            $this->saveTtAntri($request['no_terima'], $request);
        }
        $this->dbActive->trans_complete();
        if ($this->dbActive->trans_status() === false) {
            $error = $this->dbActive->error();
            $msg['save'] = false;
            $msg['resp'] = !empty($error['message']) ? $error['message'] : 'Data Gagal disimpan';
        } else {
            $msg['save'] = true;
            $msg['resp'] = 'Data Berhasil Disimpan';
        }
        return $msg;
    }

    public function edit($request)
    {
        $this->dbActive->trans_start();
        $status = isset($request['status']) ? "LUNAS" : "";
        $data = array(
            'tglterima' => $request['tanggal'],
            'nmpenerima' => $request['penerima'],
            'nmpengirim' => $request['pengirim'],
            'STATUS' => $status,
        );

        $this->dbActive->where('NoTerima', $request['no_terima']);
        $query = $this->dbActive->update('mtt_antri', $data);
        if ($query) {
            $this->dbActive
                ->where('Noterima', $request['no_terima'])
                ->delete('tt_antri');
            // $this->dbActive->delete('tt_antri');

            $this->saveTtAntri($request['no_terima'], $request);
        }
        $this->dbActive->trans_complete();
        if ($this->dbActive->trans_status() === false) {
            $error = $this->dbActive->error();
            $msg['save'] = false;
            $msg['resp'] = !empty($error['message']) ? $error['message'] : 'Data Gagal Diubah';
        } else {
            $msg['save'] = true;
            $msg['resp'] = 'Data Berhasil Diubah';
        }
        return $msg;
    }

    public function delete($noterima, $isSaveLog = true)
    {
        $this->dbActive->trans_start();
        $this->crud_m->delete('mtt_antri', array('NoTerima' => $noterima), $isSaveLog);
        $this->dbActive->trans_complete();

        return $this->dbActive->trans_status();
    }

}
