<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Crud_m extends CI_Model
{

    public $CI = null;

    public function __construct()
    {
        parent::__construct();
        //$this->db->update('online', array('last_activity' => date('Y-m-d H:i:s')));
    }

    public function insert($namaTabel, $data, $log = false)
    {
        $result = $this->db->insert($namaTabel, $data);
        if ($result == true) {
            if ($log == true) {
                $this->privileges->catat($data, '', 'insert', true);
            }

            return $result;
        } else {
            return $this->db->_error_message();
        }
    }

    public function update($namaTabel, $data, $where, $log = false)
    {
        $result = $this->db->update($namaTabel, $data, $where);
        if ($log == true) {
            $this->privileges->catat($data, '', 'update', true);
        }
        if (!empty($result) && $result == true) {
            return $result;
        } else {
            //echo 'Terst'.$this->db->_errornumber();
            return $this->db->_error_message();
        }
    }

    public function delete($namaTabel, $where, $log = false, $child = false)
    {
        if ($log == true) {
            $sql = $this->db->get_where($namaTabel, $where);
            $old_data = $sql->row_array();
            $this->privileges->catat($old_data, '', 'delete', true);
        }
        $hasil = $this->db->delete($namaTabel, $where);
        if (!empty($hasil) && $hasil == true) {
            return true;
        } else {
            return $this->db->_error_message();
        }
    }

    public function result($result = false)
    {
        $user_message = '';
        if ($result === true) {
            $ret = array(
                'success' => true,
            );
        } else {
            //echo $this->db->_error_number();
            switch ($this->db->_error_number()) {
                default:
                    $ret = array(
                        'success' => false,
                        'error' => !empty($result) ? $result : $this->db->_error_message(),
                    );
                    break;
                case 1451:
                    $user_message = 'Nomor transaksi yg anda pilih telah digunakan sebagai referensi di transaksi yang lain.<br>
                                            Hapus terlebih dahulu transaksi yang menggunakan no tersebut';
                    $ret = array(
                        'success' => false,
                        'error' => $user_message, //.$this->db->_error_message()
                    );
                    break;
                case 1062:
                    $user_message = 'Nomor transaksi yg anda masukan sudah terpakai!';
                    $ret = array(
                        'success' => false,
                        'error' => $user_message,
                    );
                    break;
            }
            //Catat Error Database yang terjasi
            $kegiatan = 'Error Db No : ' . $this->db->_error_number() . '<br>' .
            'Error Message : ' . $this->db->_error_message() . '<br>' .
            'User Message : ' . $user_message . '<br>' .
            'User Active : ' . $this->session->userdata(SESS_PREFIK . 'nama') . '<br>' .
            'Controller Name : ' . $this->uri->segment(1) . '<br>' .
            'Fumnction Name : ' . $this->uri->segment(2);

            $this->privileges->catat($kegiatan, '', 'error');
        }
        return $ret;
    }

    public function get_data($configs)
    {
        $_POST = $this->securepost->postMethod();
        $arr_no_col_search = array('status_cek');
        $default = array(
            'id' => 'id',
            'aColumns' => array(),
            'sLimit' => '',
            'sOrder' => '', // Pdf,Docx dll
            'sWhere' => array(),
            'datamodel' => '',
            'actiontable' => '',
        );

        if (is_array($configs)) {
            foreach ($configs as $key => $val) {
                if (isset($default[$key])) {
                    $default[$key] = $val;
                }
            }
        }

        //$default['aColumns'] = array('nopol', 'merk', 'jenis', 'warna', 'supir');
        /*
         * Paging
         */
        //$sLimit = "";
        if (isset($_POST['iDisplayStart']) && $_POST['iDisplayLength'] != '-1') {
            $default['sLimit'] = "LIMIT " . intval($_POST['iDisplayStart']) . ", " .
            intval($_POST['iDisplayLength']);
        }
        if (empty($default['number_row']) || !$default['number_row']) {
            $_POST['iSortCol_0'] = ($_POST['iSortCol_0'] == 0) ? 1 : $_POST['iSortCol_0'] - 1;
        }
        /*
         * Ordering
         */
        //$sOrder = "";
        if (isset($_POST['iSortCol_0'])) {
            $default['sOrder'] = "ORDER BY  ";
            for ($i = 0; $i < intval($_POST['iSortingCols']); $i++) {
                if ($_POST['bSortable_' . intval($_POST['iSortCol_' . $i])] == "true" && $default['aColumns'][intval($_POST['iSortCol_' . $i])] != '') {
                    $default['sOrder'] .= "`" . $default['aColumns'][intval($_POST['iSortCol_' . $i])] . "` " .
                        ($_POST['sSortDir_' . $i] === 'asc' ? 'asc' : 'desc') . ", ";
                }
            }

            $default['sOrder'] = substr_replace($default['sOrder'], "", -2);
            if ($default['sOrder'] == "ORDER BY") {
                $default['sOrder'] = "";
            }
        }
        /*
         * Filtering
         * NOTE this does not match the built-in DataTables filtering which does it
         * word by word on any field. It's possible to do here, but concerned about efficiency
         * on very large tables, and MySQL's regex functionality is very limited
         */
        //$sWhere = "";

        if (isset($_POST['sSearch']) && $_POST['sSearch'] != "") {
            $default['sWhere'] = "WHERE (";
            for ($i = 0; $i < count($default['aColumns']); $i++) {
                if (isset($_POST['bSearchable_' . $i]) && $_POST['bSearchable_' . $i] == "true") {
                    if (!in_array($default['aColumns'][$i], $arr_no_col_search)) {
                        $default['sWhere'] .= "`" . $default['aColumns'][$i] . "` LIKE '%" . $_POST['sSearch'] . "%' OR ";
                    }

                }
            }
            $default['sWhere'] = substr_replace($default['sWhere'], "", -3);
            $default['sWhere'] .= ')';
        }

        /* Individual column filtering */
        for ($i = 0; $i < count($default['aColumns']); $i++) {
            if (isset($_POST['bSearchable_' . $i]) && $_POST['bSearchable_' . $i] == "true" && $_POST['sSearch_' . $i] != '') {
                if ($default['sWhere'] == "") {
                    $default['sWhere'] = "WHERE ";
                } else {
                    $default['sWhere'] .= " AND ";
                }
                $default['sWhere'] .= "`" . $default['aColumns'][$i] . "` LIKE '%" . $_POST['sSearch_' . $i] . "%' ";
            }
        }
        /* Output */
        //echo $default['datamodel'];
        //$model = $default['datamodel'];
        $default['sWhere'] = (empty($default['sWhere'])) ? '' : $default['sWhere'];
        $this->load->model($default['datamodel'], 'datamodel');
        $data = $this->datamodel->getData($default['sWhere'], $default['sOrder'], $default['sLimit']);
        $dataRow = array();
        $dataRow = $data['data'];
        //$number_row =  array();
        if (!empty($default['actiontable'])) {

            if (empty($default['number_row']) || !$default['number_row']) {
                array_splice($default['aColumns'], 0, 0, array('no'));
            }
            $default['aColumns'][] = 'actiontable';
            $actiontable = $default['actiontable'];
            $no = $_POST['iDisplayStart'] + 1;
            foreach ($dataRow as $keyd => $vald) {
                $aksi = '';
                foreach ($actiontable as $elemens) {
                    if (isset($elemens['tag'])) {
                        $aksi .= "<" . $elemens['tag'] . ' ';
                    } else {
                        $aksi .= "<a ";
                    }
                    foreach ($elemens as $key => $val) {
                        if ($key != 'label') {
                            if ($key == 'href') {
                                if(strpos($val, '{keyId}') !== false){
                                    $val = str_replace('{keyId}', $dataRow[$keyd][$default['id']], $val);
                                }else{
                                    $val .= '/' .  $dataRow[$keyd][$default['id']];
                                }
                            }

                            $aksi .= $key . '="' . $val . '" ';
                        }
                    }
                    $aksi .= '>';
                    if (isset($elemens['label'])) {
                        $aksi .= $elemens['label'];
                    }

                    if (isset($elemens['tag'])) {
                        $aksi .= "</" . $elemens['tag'] . '>';
                    } else {
                        $aksi .= "</a>&nbsp";
                    }
                }

                if (empty($default['number_row']) || !$default['number_row']) {
                    $dataRow[$keyd]['no'] = $no;
                }

                $dataRow[$keyd]['actiontable'] = $aksi;
                $no++;
            }
        }
        $iTotal = count($dataRow);
        $iFilteredTotal = $data['total'];
        $output = array(
            "sEcho" => intval($_POST['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array(),
        );
        //var_dump($default['aColumns']);
        foreach ($dataRow as $aRow) {
            $row = array();
            for ($i = 0; $i < count($default['aColumns']); $i++) {
                $row[] = $aRow[$default['aColumns'][$i]];
            }
            $output['aaData'][] = $row;
        }
        return json_encode($output);
    }

}
