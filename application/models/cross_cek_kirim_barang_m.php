<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cross_cek_kirim_barang_m extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getData($sWhere, $sOrder, $sLimit) {
        $result = array();
        
        $fil_bln = $this->input->post('fil_bln');
        $fil_thn = $this->input->post('fil_thn');
        if(empty($sWhere)){
            $sWhere = '';
            $sWhere .= ' WHERE (MONTH(m_cek_tgl)="'.$fil_bln.'" AND YEAR(m_cek_tgl)="'.$fil_thn.'") ';
        }else{
            $sWhere .= ' AND (MONTH(m_cek_tgl)="'.$fil_bln.'" AND YEAR(m_cek_tgl)="'.$fil_thn.'") ';
        }
        $query = "SELECT *,IF(stat2='1','Belum Selesai','Selesai') AS status_cek FROM v_m_cek $sWhere ";
        $sqlX = $this->db->query($query);
        $result['total'] = $sqlX->num_rows();

        $sqlY = $this->db->query($query . "$sOrder $sLimit");
        $result['data'] = $sqlY->result_array();
        return $result;
    }

    public function getDataKirim($nopol,$edit = false){
        if($edit){
            $where = "WHERE stat >='1' AND NOKIRIM = '$nopol'";
        }else{
            $where = "WHERE stat >='1' AND NOKIRIM = '$nopol'";
        }
        $query = $this->db->query("SELECT d.*,if(ISNULL(d.ket_cek),'',d.ket_cek) as ket_cek FROM tt_kirim d $where");
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }

    public function getNopol($query) {
        $querys = $this->db->query("SELECT nokirim,nopol,tglkirim FROM m_kirim 
                                    WHERE (nopol LIKE '%$query%' OR nokirim LIKE '%$query%') 
                                    AND nokirim NOT IN (SELECT m_cek_fk FROM m_cek) AND stat<>'data_lama'");
        return ($querys->num_rows() > 0) ? $querys->result_array() : FALSE;
    }
    
    public function getListById($id){
        $this->db->where('m_cek_no', $id);
        $result = $this->db->get('v_m_cek');
        return $result->row();
    }

    public function getLastNo($year, $month)
    {
        $query = $this->db->query("SELECT MAX(SUBSTRING(m_cek_no,4,4)) as lastno FROM m_cek WHERE YEAR(m_cek_tgl)=$year 
                                    AND MONTH(m_cek_tgl)=$month");
        if ($query) {
            $row = $query->row();
            return $row->lastno + 1;
        } else {
            return 1;
        }
    }
}
