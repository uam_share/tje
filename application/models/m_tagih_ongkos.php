<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class M_tagih_ongkos extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getData($sWhere, $sOrder, $sLimit)
    {
        $result = array();

        $sDate = '';
        $datemin = $this->input->post('date_min');
        $datemax = $this->input->post('date_max');
        //$sWhere =
        if (empty($sWhere)) {
            $sDate .= ' WHERE (TGLKIRIM>="' . $datemin . '" AND TGLKIRIM<="' . $datemax . '") ';
        } else {
            $sDate .= ' AND (TGLKIRIM>="' . $datemin . '" AND TGLKIRIM<="' . $datemax . '") ';
        }
        
        $query = "SELECT * FROM m_kirim $sWhere $sDate";

        $sqlX = $this->db->query($query);
        $result['total'] = $sqlX->num_rows();

        $sqlY = $this->db->query($query . "$sOrder $sLimit");
        $result['data'] = $sqlY->result_array();
        // echo $this->db->last_query();
        return $result;
    }

    public function getDataMuat($nopol)
    {
        $query = $this->db->query("select b.noterima as no_stt, b.NOMOR as kode,
                    a.banyak, a.SATUAN as satuan, b.nmpengirim as pengirim, b.NMPENERIMA as penerima,
                    b.`STATUS` as `status`,jml_ongkos from tt_muat a left join mtt_muat b on a.NOTERIMA=b.noterima
                    where b.nopol = '$nopol'");
        return ($query->num_rows() > 0) ? $query->result_array() : false;
    }

    public function getDataKirim($no)
    {
        $query = $this->db->query("SELECT a.nomuat AS no_stt, a.noterima AS kode,
                    a.banyak, a.SATUAN AS satuan, a.nmpengirim AS pengirim, a.NMPENERIMA AS penerima,
                    a.`STATUS` AS `status`,a.jml_ongkos FROM tt_kirim a WHERE a.nokirim = '$no'");
        return ($query->num_rows() > 0) ? $query->result_array() : false;
    }

    public function getNopol($query)
    {
        $query = $this->db->query("SELECT DISTINCT nopol FROM mtt_muat
                                WHERE nopol LIKE '%$query%'");
        return ($query->num_rows() > 0) ? $query->result_array() : false;
    }

    public function getNoKirim($year, $month)
    {
        // $sql = "SELECT MAX(LEFT(NOKIRIM,4)) AS nokirim FROM
        //         m_kirim WHERE YEAR(TGLKIRIM)='$year' AND MONTH(TGLKIRIM)='$month'";
        $sql = "SELECT MAX(SUBSTRING(NOKIRIM,4,4)) AS nokirim FROM
                m_kirim WHERE YEAR(TGLKIRIM)='$year' AND MONTH(TGLKIRIM)='$month'";
        $query = $this->db->query($sql);
        if ($query) {
            $row = $query->row();
            return $row->nokirim + 1;
        } else {
            return '0001';
        }
    }

    public function getMuatToKirim($nopol)
    {
        $sql = "select a.NOTERIMA as NOMUAT, b.tglterima as TGLTERIMA, b.NOMOR as NOTERIMA,
                b.TGL as TGLMUAT, a.banyak as BANYAK, a.barang as BARANG, a.JML_ONGKOS as JUMLAH,
                a.SAT, a.SATUAN, b.nmpengirim as NMPENGIRIM, b.NMPENERIMA, b.`STATUS`,a.jml_ongkos
                from tt_muat a left join mtt_muat b on a.NOTERIMA=b.noterima
                where b.nopol = '$nopol'";
        $query = $this->db->query($sql);
        foreach ($query->result_array() as $row) {
            $data[] = array(
                'NOKIRIM' => $_POST['no_kirim'],
                'NOTERIMA' => $row['NOTERIMA'],
                'TGLTERIMA' => $row['TGLTERIMA'],
                'NOMUAT' => $row['NOMUAT'],
                'TGLMUAT' => $row['TGLMUAT'],
                'BANYAK' => $row['BANYAK'],
                'BARANG' => $row['BARANG'],
                'JUMLAH' => $row['JUMLAH'],
                'SAT' => $row['SAT'],
                'SATUAN' => $row['SATUAN'],
                'NMPENGIRIM' => $row['NMPENGIRIM'],
                'NMPENERIMA' => $row['NMPENERIMA'],
                'STATUS' => $row['STATUS'],
                'jml_ongkos' => $row['jml_ongkos'],
            );
        }
        return $this->db->insert_batch('tt_kirim', $data);
    }

    public function saveMuatToKirim($nopol)
    {
        $sql = "select a.NOTERIMA as NOMUAT, b.tglterima as TGLMUAT, b.NOMOR as NOTERIMA,
                b.TGL as TGLTERIMA, a.banyak as BANYAK, a.barang as BARANG, a.jumlah as JUMLAH,
                a.SAT, a.SATUAN, b.nmpengirim as NMPENGIRIM, b.NMPENERIMA, b.`STATUS`,a.jml_ongkos
                from tt_muat a left join mtt_muat b on a.NOTERIMA=b.noterima
                where b.nopol = '$nopol'";
        $query = $this->db->query($sql);
        foreach ($query->result_array() as $row) {
            $data[] = array(
                'NOKIRIM' => $_POST['no_kirim'],
                'NOTERIMA' => $row['NOTERIMA'],
                'TGLTERIMA' => $row['TGLTERIMA'],
                'NOMUAT' => $row['NOMUAT'],
                'TGLMUAT' => $row['TGLMUAT'],
                'BANYAK' => !empty($row['BANYAK']) ? $row['BANYAK'] : 0,
                'BARANG' => $row['BARANG'],
                'JUMLAH' => !empty($row['JUMLAH']) ? $row['JUMLAH'] : 0,
                'SAT' => $row['SAT'],
                'SATUAN' => $row['SATUAN'],
                'NMPENGIRIM' => $row['NMPENGIRIM'],
                'NMPENERIMA' => $row['NMPENERIMA'],
                'STATUS' => $row['STATUS'],
                'jml_ongkos' => !empty($row['jml_ongkos']) ? $row['jml_ongkos'] : 0,
            );
        }
        return $this->db->insert_batch('tt_kirim', $data);
    }

    public function delete($where, $id, $table)
    {
        $this->db->where($where, $id);
        return $this->db->delete($table);
    }

    public function getdatarollbacktkirim($nokirim)
    {
        $dataRollBack = array();
        $query = "SELECT h.NOPOL,h.KETERANGAN,d.*,SUM(d.banyak) AS TOTAL FROM m_kirim h
                    INNER JOIN tt_kirim d ON h.NOKIRIM=d.NOKIRIM
                    WHERE h.nokirim = '$nokirim' GROUP BY d.NOMUAT";
        $rolldataH = $this->db->query($query)->result();
        foreach ($rolldataH as $header) {
            $dataH[] = array(
                'noterima' => $header->NOMUAT,
                'tglterima' => $header->TGLMUAT,
                'nmpengirim' => $header->NMPENGIRIM,
                'NMPENERIMA' => $header->NMPENERIMA,
                'nopol' => $header->NOPOL,
                'TOTAL' => $header->TOTAL,
                'NOMOR' => $header->NOTERIMA,
                'tgl' => $header->TGLTERIMA,
                'status' => $header->STATUS,
            );
        }
        $dataRollBack['dataH'] = $dataH;

        $query = "SELECT h.NOPOL,h.KETERANGAN,d.* FROM m_kirim h
                    INNER JOIN tt_kirim d ON h.NOKIRIM=d.NOKIRIM
                    WHERE h.nokirim = '$nokirim'";
        $rolldataD = $this->db->query($query)->result();
        foreach ($rolldataD as $detail) {
            $dataD[] = array(
                'NOTERIMA' => $detail->NOMUAT,
                'banyak' => $detail->BANYAK,
                'barang' => $detail->BARANG,
                'jumlah' => $detail->JUMLAH,
                'SAT' => $detail->SAT,
                'SATUAN' => $detail->SATUAN,
                'STATUS' => 'MUAT',
                'ONGKOS' => '',
                'JML_ONGKOS' => $detail->jml_ongkos,
            );
        }
        $dataRollBack['dataD'] = $dataD;
        return $dataRollBack;
    }

    public function getListById($id)
    {
        $this->db->where('nokirim', $id);
        $result = $this->db->get('m_kirim');
        return $result->row();
    }
}
