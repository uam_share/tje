<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Penagihan_ongkos_m extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function getData($sWhere, $sOrder, $sLimit) {
        $result = array();
        $fil_bln = $this->input->post('fil_bln');
        $fil_thn = $this->input->post('fil_thn');
        if(empty($sWhere)){
            $sWhere = '';
            $sWhere .= ' WHERE (MONTH(m_tagih_tgl)="'.$fil_bln.'" AND YEAR(m_tagih_tgl)="'.$fil_thn.'") ';
        }else{
            $sWhere .= ' AND (MONTH(m_tagih_tgl)="'.$fil_bln.'" AND YEAR(m_tagih_tgl)="'.$fil_thn.'") ';
        }
        $query = "SELECT *,IF(stat<'3','Belum Selesai','Selesai') AS status_cek  FROM v_m_tagih $sWhere ";
        $sqlX = $this->db->query($query);
        $result['total'] = $sqlX->num_rows();

        $sqlY = $this->db->query($query . "$sOrder $sLimit");
        $result['data'] = $sqlY->result_array();

        return $result;
    }

    function getDataKirim($nopol, $status_tagih = -1, $edit = false){
        if($edit){
            $where = '';
            switch ($status_tagih) {
                case '1':
                    $where .= "WHERE stat <'3' AND nokirim = '$nopol'";
                    break;
                case '3':
                    $where .= "WHERE stat ='3' AND nokirim = '$nopol'";
                    break;
                default:
                    $where = "WHERE stat >='1' AND nokirim = '$nopol'";
                    break;
            }
            
        }else{
            $where = "WHERE stat >='1' AND nokirim = '$nopol'";
        }

        $query = $this->db->query("SELECT d.*,if(ISNULL(d.ket_tagih),'',d.ket_tagih) as ket_tagih,(jml_ongkos + bb_jkt) AS subtotal FROM tt_kirim d $where");
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }
    
    function getNopol($query) {
        $querys = $this->db->query("SELECT nokirim,nopol,tglkirim FROM m_kirim
            WHERE nokirim NOT IN (SELECT m_tagih_fk FROM v_m_tagih) AND (nokirim LIKE '%$query%' OR nopol LIKE '%$query%') AND stat<>'data_lama'");
        return ($querys->num_rows() > 0) ? $querys->result_array() : FALSE;
    }
    
    function getListById($id){
        $this->db->where('m_tagih_no', $id);
        $result = $this->db->get('v_m_tagih');
        return $result->row();
    }

    public function getLastNo($year, $month)
    {
        $query = $this->db->query("SELECT MAX(SUBSTRING(m_tagih_no,4,4)) as lastno FROM m_tagih WHERE YEAR(m_tagih_tgl)=$year 
                                    AND MONTH(m_tagih_tgl)=$month");
        if ($query) {
            $row = $query->row();
            return $row->lastno + 1;
        } else {
            return 1;
        }
    }
}
