<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class M_kendaraan extends CI_Model
{

    protected $dbActive;
    public function __construct()
    {
        parent::__construct();
        $this->dbActive = $this->load->database('tjedb', TRUE); // $this->db
    }

    public function getData($sWhere, $sOrder, $sLimit)
    {
        $result = array();
        $query = "SELECT nopol, merk, jenis, warna, supir FROM m_kendaraan $sWhere ";

        $sqlX = $this->dbActive->query($query);
        $result['total'] = $sqlX->num_rows();

        $sqlY = $this->dbActive->query($query . "$sOrder $sLimit");
        $result['data'] = $sqlY->result_array();

        return $result;
    }

    public function getAsList($query, $columns = "nopol")
    {
        $query = $this->dbActive->query("SELECT $columns FROM m_kendaraan
                                WHERE nopol LIKE '%$query%'");
        return ($query->num_rows() > 0) ? $query->result_array() : false;
    }

}
