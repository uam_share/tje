<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class M_terima_muat extends CI_Model
{
    protected $dbTje;

    public function __construct()
    {
        parent::__construct();
        $this->dbTje = $this->load->database('tjedb', true); // $this->db
    }

    public function getNoMuat($date = '')
    {
        $pdate = (!empty($date)) ? $date : date('Y-m-d');
        $month = date('m', strtotime($pdate));
        $year = date('Y', strtotime($pdate));

        $query = $this->db->query("SELECT MAX(LEFT(NOMUAT,4)) as lastno FROM  muat_r WHERE thn=$year AND bln=$month");
        $lastno = $query->row()->lastno;

        $lastno = (int) $lastno + 1;

        if ($lastno < 10) {
            $lastno = '000' . $lastno;
        } else if ($lastno < 100) {
            $lastno = '00' . $lastno;
        } else if ($lastno < 1000) {
            $lastno = '0' . $lastno;
        } else if ($lastno < 10000) {
            $lastno = $lastno;
        }
        //echo $lastno;
        $rest['no_muat'] = $this->config->item('site_code') . $this->config->item('muat_code') . '-' .
                $lastno . '/' . $this->fungsi->blnTOromawi($month) . '/' . date('y', strtotime($pdate));
        return $rest;
    }

    public function getRows($noterima)
    {
        $query = $this->db->query("SELECT * FROM mtt_muat a left join m_pengirim b on a.nmpengirim=b.nmpengirim
                where noterima = '$noterima'");
        return ($query->num_rows() > 0) ? $query->result_array() : false;
    }

    // public function getData($sWhere, $sDate, $sOrder, $sLimit)
    public function getData($sWhere, $sOrder, $sLimit)
    {
        $result = array();
        $datemin = $this->input->post('date_min');
        $datemax = $this->input->post('date_max');
        //$sWhere =
        if (empty($sWhere)) {
            $sWhere = '';
            $sWhere .= ' WHERE (tglterima>="' . $datemin . '" AND tglterima<="' . $datemax . '") ';
        } else {
            $sWhere .= ' AND (tglterima>="' . $datemin . '" AND tglterima<="' . $datemax . '") ';
        }
        $query = "SELECT * FROM mtt_muat $sWhere ";
        //echo $query;
        $sqlX = $this->db->query($query);
        $result['total'] = $sqlX->num_rows();

        $sqlY = $this->db->query($query . "$sOrder $sLimit");
        $result['data'] = $sqlY->result_array();

        return $result;
    }

    public function getGridMuat($noterima)
    {
        $query = $this->db->query("select a.banyak as BANYAK,a.SATUAN as Satuan,a.barang as Barang,
                    round(jumlah,2) as JUMLAH, a.SAT,
                    a.Ongkos,a.Jml_ongkos
                    from tt_muat a where a.NoTerima = '$noterima'");
        return ($query->num_rows() > 0) ? $query->result_array() : false;
    }

    public function getNopol($query)
    {
        $query = $this->db->query("SELECT nopol FROM m_kendaraan
                                WHERE nopol LIKE '%$query%'");
        return ($query->num_rows() > 0) ? $query->result_array() : false;
    }

    public function getReport($noterima)
    {
        $sql = "SELECT mtt_muat.NOTERIMA,mtt_muat.TGLTERIMA,mtt_muat.NOPOL,mtt_muat.NMPENGIRIM,(m_pengirim.ALAMAT)AS ALAMAT1,
            mtt_muat.NMPENERIMA,(m_penerima.ALAMAT)AS ALAMAT2,mtt_muat.STATUS,tt_muat.BANYAK,tt_muat.SATUAN,tt_muat.BARANG,
            tt_muat.JUMLAH,tt_muat.SAT,tt_muat.ONGKOS,tt_muat.JML_ONGKOS,CONCAT(mtt_muat.NOMOR,' ') AS NOMOR
            FROM (mtt_muat INNER JOIN tt_muat ON mtt_muat.NOTERIMA=tt_muat.NOTERIMA),m_pengirim,m_penerima
            WHERE mtt_muat.NMPENGIRIM=m_pengirim.NMPENGIRIM AND mtt_muat.NMPENERIMA=m_penerima.NMPENERIMA AND mtt_muat.NOTERIMA='$noterima'";
        return $sql;
    }

    public function saveTtMuat($no, $jml, $status)
    {
        //echo $no;
        for ($i = 0; $i < $jml; $i++) {
            $data[] = array(
                'NOTERIMA' => $no,
                'banyak' => !empty($_POST['banyak'][$i]) ? $_POST['banyak'][$i] : 0,
                'barang' => $_POST['jenis_barang'][$i],
                'jumlah' => !empty($_POST['jumlah'][$i]) ? $_POST['jumlah'][$i] : 0,
                'SAT' => $_POST['kg_m3'][$i],
                'SATUAN' => $_POST['satuan'][$i],
                'STATUS' => 'MUAT',
                //'ONGKOS' => $_POST['ongkos'][$i] . '/' . $_POST['kg_m3'][$i],
                'ONGKOS' => !empty($_POST['ongkos'][$i]) ? $_POST['ongkos'][$i] : 0,
                'JML_ONGKOS' => !empty($_POST['jml_ongkos'][$i]) ? $_POST['jml_ongkos'][$i] : 0,
            );
            $data[$i]['JML_ONGKOS'] = str_replace(".", "", $data[$i]['JML_ONGKOS']);
        }
        return $this->db->insert_batch('tt_muat', $data);
    }

    public function add($request)
    {
        $this->db->trans_start();
        $data = array(
            'noterima' => $request['no_muat'],
            'tglterima' => $request['tanggal'],
            'nmpengirim' => $request['pengirim'],
            'NMPENERIMA' => $request['penerima'],
            'nopol' => $request['no_polisi'],
            'TOTAL' => $request['total'],
            'NOMOR' => $request['no_terima'],
            'tgl' => $request['tgl_terima'],
            'status' => $request['status'],
        );

        $query = $this->db->insert('mtt_muat', $data);
        if ($query) {
            // delete mtt_antri
            $tableNameTerima = $this->dbTje->database . '.' . 'mtt_antri';
            // $this->delete('NoTerima', $request['no_terima'], $tableNameTerima);
            $this->crud_m->delete($tableNameTerima, array('NoTerima' => $request['no_terima']));

            // update mtt_muat
            if (!empty($request['banyak'])) {
                $jml = count($request['banyak']);
                $this->saveTtMuat($request['no_muat'], $jml, $request['status']);
            }

            // increase no muat
            $nomuat = explode("/", $request['no_muat']);
            $this->updateMuatR($nomuat[0]);
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $error = $this->db->error();
            $msg['save'] = false;
            $msg['resp'] = !empty($error['message']) ? $error['message'] : 'Data Gagal disimpan';
        } else {
            $msg['save'] = true;
            $msg['resp'] = 'Data Berhasil Disimpan';
        }
        return $msg;
    }

    public function edit($request)
    {
        $this->db->trans_start();
        $data = array(
            'tglterima' => $request['tanggal'],
            'nmpengirim' => $request['pengirim'],
            'NMPENERIMA' => $request['penerima'],
            'nopol' => $request['no_polisi'],
            'TOTAL' => $request['total'],
            'NOMOR' => $request['no_terima'],
            'tgl' => $request['tgl_terima'],
            'status' => $request['status'],
        );

        // Update mtt_antry if no terima changed
        $this->backtoAntri($request['no_muat'], $request['no_terima']);

        // update mtt_muat
        $this->db->where('noterima', $request['no_muat']);
        $query = $this->db->update('mtt_muat', $data);
        if ($query) {

            // delete mtt_antri
            $tableNameTerima = $this->dbTje->database . '.' . 'mtt_antri';
            $this->crud_m->delete($tableNameTerima, array('NoTerima' => $request['no_terima']));

            // update mtt_muat
            if (!empty($request['banyak'])) {
                $this->db->where('noterima', $request['no_muat']);
                $this->db->delete('tt_muat');
                $jml = count($request['banyak']);
                $this->saveTtMuat($request['no_muat'], $jml, $request['status']);
            }
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $error = $this->db->error();
            $msg['save'] = false;
            $msg['resp'] = !empty($error['message']) ? $error['message'] : 'Data Gagal Diubah';
        } else {
            $msg['save'] = true;
            $msg['resp'] = 'Data Berhasil Diubah';
        }
        return $msg;
    }

    public function delete($nomor, $isSaveLog = true)
    {
        $this->db->trans_start();
        // back to mtt_antri
        $this->backtoAntri($nomor);
        // Delete Data mtt_muat
        $this->crud_m->delete('mtt_muat', array('noterima' => $nomor), $isSaveLog);
        $this->db->trans_complete();

        return $this->db->trans_status();
    }

    public function updateMuatR($no)
    {
        $nomuat = explode('-', $no);
        if(count($nomuat) > 1){
            $nomuat = $nomuat[1];
        }else{
            $nomuat = $nomuat[0];
        }
        $pdate = (!empty($date)) ? $date : date('Y-m-d');
        $month = date('m', strtotime($pdate));
        $year = date('Y', strtotime($pdate));
        $strsql = "UPDATE muat_r set nomuat='{$nomuat}', BLN='{$month}',THN='{$year}'";
        $this->db->query($strsql);
    }

    public function backtoAntri($noMuat, $noTerimaChange = null)
    {
        $shouldBack = true;
        $queryH = $this->db->query("SELECT * FROM mtt_muat WHERE noterima='$noMuat'");

        if (!empty($noTerimaChange)) {
            $shouldBack = ($queryH->row()->NOMOR != $noTerimaChange);
        }

        $dataH = array(
            'NoTerima' => $queryH->row()->NOMOR,
            'tglterima' => $queryH->row()->TGL,
            'nmpenerima' => $queryH->row()->NMPENERIMA,
            'nmpengirim' => $queryH->row()->nmpengirim,
            'STATUS' => $queryH->row()->STATUS,
        );
        if ($shouldBack && !empty($dataH['NoTerima'])) {
            $queryD = $this->db->query("SELECT * FROM tt_muat WHERE noterima='$noMuat'");

            $tableNameTerima = $this->dbTje->database . '.' . 'mtt_antri';
            $this->db->insert($tableNameTerima, $dataH);
            $dataD = array();
            $i = 0;
            foreach ($queryD->result() as $detail) {
                $dataD[$i] = array(
                    'Noterima' => $dataH['NoTerima'],
                    'BANYAK' => $detail->banyak,
                    'Satuan' => $detail->SATUAN,
                    'Barang' => $detail->barang,
                    'JUMLAH' => $detail->jumlah,
                    'SAT' => $detail->SAT,
                    'STATUS' => 'ANTRI',
                    'Ongkos' => $detail->ONGKOS,
                    'jml_ongkos' => $detail->JML_ONGKOS,
                );
                $i++;
            }
            if (!empty($dataD)) {
                $this->db->insert_batch($this->dbTje->database . '.' . 'tt_antri', $dataD);
            }
        }
    }

}
