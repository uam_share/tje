<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class M_pengirim extends CI_Model
{

    protected $dbActive;
    public function __construct()
    {
        parent::__construct();
        $this->dbActive = $this->load->database('tjedb', TRUE); // $this->db
    }

    public function getData($sWhere, $sOrder, $sLimit)
    {
        $result = array();
        $query = "SELECT nmpengirim, alamat, telp, email FROM m_pengirim $sWhere ";

        $sqlX = $this->db->query($query);
        $result['total'] = $sqlX->num_rows();

        $sqlY = $this->db->query($query . "$sOrder $sLimit");
        $result['data'] = $sqlY->result_array();

        return $result;
    }

    public function getAsList($query, $columns = "nmpengirim")
    {
        $query = $this->dbActive->query("SELECT $columns FROM m_pengirim
                                WHERE nmpengirim LIKE '%$query%'");
        return ($query->num_rows() > 0) ? $query->result_array() : false;
    }

}
