<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pengirim extends CI_Controller {

	function  __construct()
    {
        parent::__construct();
        $this->privileges->guestAction();
		$this->load->model('M_pengirim');
    }
	
	public function index()
	{
		$data['content'] = 'pengirim/index';
		$this->load->view('template',$data);
	}
	
	public function get_data()
    {
        $configs = array(
            'id' => 'nmpengirim',
            'aColumns' => array('nmpengirim', 'alamat', 'telp', 'email'),
            'datamodel' => 'M_pengirim',
            'actiontable' => array(
                'edit' => array(
                    'href' => 'pengirim/form',
                    'label' => '<i class="fa fa-edit"></i>', //"<img src='" . base_url() . "/assets/ico/ubah.png' />",
                    'title' => 'Ubah Data',
                ),
                'delete' => array(
                    'href' => 'pengirim/delete',
                    'label' => '<i class="fa fa-trash"></i>', //"<img src='" . base_url() . "/assets/ico/hapus.png' />",
                    'title' => 'Hapus Data',
                    'onclick' => 'return deleteData()',
                ),
            ),
        );
        echo $this->crud_m->get_data($configs);
    }
	
	function form(){
		$nama = rawurldecode($this->uri->segment(3));
		$this->form_validation->set_rules('nmpengirim', 'Nmpengirim', 'required');
		$this->form_validation->set_message('required', '<span style="color:red;font-weight:bold;">*wajib diisi!!</span>');
		if ($this->form_validation->run()) {
			if ($_POST) {
				if ($nama) {
					foreach($_POST as $row => $key){
						$data[$row] = $key;
					}
					$this->db->where('nmpengirim',$nama);
					$this->db->update('m_pengirim',$data);
					$this->session->set_flashdata('msg', '<div class="alert alert-info fade in">
							<button class="close" data-dismiss="alert" type="button">x</button>
							<strong>Data berhasil diupdate</strong>
							</div>');
					redirect('/pengirim');
				}else{
					foreach($_POST as $row => $key){
						$data[$row] = $key;
					}
					$this->db->insert('m_pengirim',$data);
					$this->session->set_flashdata('msg', '<div class="alert alert-info fade in">
							<button class="close" data-dismiss="alert" type="button">x</button>
							<strong>Data berhasil disimpan</strong>
							</div>');
					redirect('/pengirim');
				}
			}
		}
		$query = $this->db->query("SELECT * FROM m_pengirim WHERE nmpengirim='$nama'");
		$data['result'] = $query->result_array();
		$data['content'] = 'pengirim/form';
		$this->load->view('template',$data);
	}
	
	function delete(){
		$nama = rawurldecode($this->uri->segment(3));
		$this->db->where('nmpengirim', $nama);
		$this->db->delete('m_pengirim');
		
		$this->session->set_flashdata('msg', '<div class="alert alert-info fade in">
						<button class="close" data-dismiss="alert" type="button">x</button>
						<strong>Data berhasil dihapus</strong>
						</div>');
		redirect('/pengirim');
	}
}
