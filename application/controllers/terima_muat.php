<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Terima_muat extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->privileges->guestAction();
        $this->load->model('M_terima_muat');
        $this->load->model('M_terima_barang');
        $this->load->model('M_kendaraan');
    }

    public function index()
    {
        $data['content'] = 'terima_muat/index';
        $this->load->view('template', $data);
    }

    public function add()
    {
        $data['content'] = 'terima_muat/add';
        $this->load->view('template', $data);
    }

    public function edit()
    {
        $noterima = $this->uri->segment(3) . '/' . $this->uri->segment(4) . '/' . $this->uri->segment(5);
        $data['rows'] = $this->M_terima_muat->getRows($noterima);
        $data['content'] = 'terima_muat/edit';
        $this->load->view('template', $data);
    }

    public function get_no_terima()
    {
        $result = $this->M_terima_barang->getAsList($_POST['query']);
        echo json_encode($result);
    }

    public function get_no_muat($date = '')
    {
        echo json_encode($this->M_terima_muat->getNoMuat($date));
    }

    public function get_table()
    {
        $result['grid'] = $this->M_terima_muat->getGridMuat($_POST['no_terima']);
        echo json_encode($result);
    }

    public function get_nopol()
    {
        $result = $this->M_kendaraan->getAsList($_POST['query']);
        echo json_encode($result);
    }

    public function get_detail_terima()
    {
        $noterima = $_POST['no_terima'];
        $result = array();
        $result['detail'] = $this->M_terima_barang->getDetailAntri($noterima);
        $result['grid'] = $this->M_terima_barang->getGridAntri($noterima);
        echo json_encode($result);
    }

    public function get_data()
    {
        $configs = array(
            'id' => 'noterima',
            'aColumns' => array('noterima', 'tglterima', 'nopol', 'nmpengirim', 'NMPENERIMA', 'NOMOR', 'STATUS'),
            'datamodel' => 'M_terima_muat',
            'actiontable' => array(
                'print' => array(
                    // '<a href="' . base_url() . 'report/r_terima_muat/' . $aRow['noterima'] .
                    // '/muat_barang" target="_blank" class="print-muat-barang" >
                    'href' => base_url() . 'report/r_terima_muat',
                    'label' => '<i class="fa fa-print"></i>',
                    'title' => 'Cetak Data',
                    'class' => 'print-muat-barang',
                    'target' => '_blank',
                ),
                'edit' => array(
                    'href' => 'terima_muat/edit',
                    'label' => '<i class="fa fa-edit"></i>',
                    'title' => 'Ubah Data',
                ),
                'delete' => array(
                    'href' => 'terima_muat/delete',
                    'label' => '<i class="fa fa-trash"></i>',
                    'title' => 'Hapus Data',
                    'onclick' => 'return deleteData()',
                ),
            ),
        );
        echo $this->crud_m->get_data($configs);
    }

    public function simpan()
    {
        echo json_encode($this->M_terima_muat->add($_POST));
    }

    public function update()
    {
        echo json_encode($this->M_terima_muat->edit($_POST));
    }

    public function delete($id = '')
    {
        $nomor = $this->uri->segment(3) . '/' . $this->uri->segment(4) . '/' . $this->uri->segment(5);
        if ($this->M_terima_muat->delete($nomor) === false) {
            $this->session->set_flashdata('msg', '<div class="alert alert-warning fade in">
                        <button class="close" data-dismiss="alert" type="button">x</button>
                        <strong>Data gagal dihapus</strong>
                        </div>');
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-info fade in">
                        <button class="close" data-dismiss="alert" type="button">x</button>
                        <strong>Data berhasil dihapus</strong>
                        </div>');
        }
        redirect('/terima_muat');
    }
}
