<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Report extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->privileges->guestAction();
        $this->load->library('phpjasperxml');
        $this->load->model('M_terima_barang');
        $this->load->model('M_terima_muat');
    }

    public function index()
    {
        $data['content'] = 'cross_cek_kirim_barang/list';
        $this->load->view('template', $data);
    }

    public function r_terima_barang($noterima)
    {
        $xml = simplexml_load_file("assets/reportfile/terima_barang.jrxml");
        $this->phpjasperxml = new PHPJasperXML();
        $this->phpjasperxml->xml_dismantle($xml);
        $sql = $this->M_terima_barang->getReport($noterima);
        $this->phpjasperxml->queryString_handler($sql);
        $this->phpjasperxml->transferDBtoArray(
            $this->M_terima_barang->getDb()->hostname, 
            $this->M_terima_barang->getDb()->username, 
            $this->M_terima_barang->getDb()->password, 
            $this->M_terima_barang->getDb()->database
        );
        $this->phpjasperxml->outpage("I"); //page output method I:standard output  D:Download file
    }

    public function r_terima_muat($noterima)
    {
        $noterima = $this->uri->segment(3) . '/' . $this->uri->segment(4) . '/' . $this->uri->segment(5);
        $strsql = $this->M_terima_muat->getReport($noterima);;
        $data['ttmuat'] = $this->db->query($strsql)->result();

        $this->load->view('report/muat_barang', $data);
    }

    public function r_crosscek_kirim_barang($no = '')
    {
        $no = $this->uri->segment(3) . '/' . $this->uri->segment(4) . '/' . $this->uri->segment(5);
        $xml = simplexml_load_file("assets/reportfile/crosscek_kirim_barang.jrxml");
        $this->phpjasperxml = new PHPJasperXML();
        $this->phpjasperxml->xml_dismantle($xml);
        $this->phpjasperxml->queryString_handler("SELECT h.*,CONCAT(d.NOTERIMA,' ')AS NOTERIMA,d.NOMUAT,d.BANYAK,d.BARANG,d.JUMLAH,
                                                    d.SATUAN,d.NMPENGIRIM,d.NMPENERIMA,d.STATUS,d.stat,d.statname,
                                                    d.jml_ongkos FROM v_m_cek h INNER JOIN v_tt_cek d
                                                    ON h.NOKIRIM=d.NOKIRIM WHERE h.m_cek_no='$no'");

        $this->phpjasperxml->transferDBtoArray($this->db->hostname, $this->db->username, $this->db->password, $this->db->database);
        $this->phpjasperxml->outpage("I");
    }

    public function r_kirim_barang($no = '')
    {
        $no = $this->uri->segment(3) . '/' . $this->uri->segment(4) . '/' . $this->uri->segment(5);
        $xml = simplexml_load_file("assets/reportfile/kirim_barang.jrxml");
        $this->phpjasperxml = new PHPJasperXML();
        //$this->phpjasperxml->debugsql=true;
        //$this->phpjasperxml->arrayParameter=array("parameter1"=>1);
        $this->phpjasperxml->xml_dismantle($xml);
        //var_dump($xml);
        $this->phpjasperxml->queryString_handler("SELECT m_kirim.*,DATE_FORMAT(tglkirim,'%d %M %Y') AS tgl_kirim,CONCAT(tt_kirim.NOTERIMA,' ')AS NOTERIMA,tt_kirim.NOMUAT,tt_kirim.BANYAK,tt_kirim.BARANG,tt_kirim.JUMLAH,
                                                    tt_kirim.SATUAN,tt_kirim.NMPENGIRIM,tt_kirim.NMPENERIMA,tt_kirim.STATUS FROM m_kirim INNER JOIN tt_kirim
                                                    ON m_kirim.NOKIRIM=tt_kirim.NOKIRIM WHERE m_kirim.NOKIRIM='$no'");

        $this->phpjasperxml->transferDBtoArray($this->db->hostname, $this->db->username, $this->db->password, $this->db->database);
        $this->phpjasperxml->outpage("I");
    }

    public function r_penagihan_ongkos($no = '')
    {
        $no = $this->uri->segment(3) . '/' . $this->uri->segment(4) . '/' . $this->uri->segment(5);
        $xml = simplexml_load_file("assets/reportfile/penagihan_ongkos.jrxml");
        $this->phpjasperxml = new PHPJasperXML();
        //$this->phpjasperxml->debugsql=true;
        //$this->phpjasperxml->arrayParameter=array("parameter1"=>1);
        $this->phpjasperxml->xml_dismantle($xml);
        $this->phpjasperxml->queryString_handler("SELECT h.*,CONCAT(d.NOTERIMA,' ') as NOTERIMA,d.NOMUAT,d.BANYAK,d.BARANG,d.JUMLAH,
                                                    d.SATUAN,d.NMPENGIRIM,d.NMPENERIMA,d.STATUS,d.stat,d.statname,
                                                    d.jml_ongkos FROM v_m_tagih h INNER JOIN v_tt_tagih d
                                                    ON h.NOKIRIM=d.NOKIRIM WHERE h.m_tagih_no='$no'");

        $this->phpjasperxml->transferDBtoArray($this->db->hostname, $this->db->username, $this->db->password, $this->db->database);
        $this->phpjasperxml->outpage("I");
    }

}
