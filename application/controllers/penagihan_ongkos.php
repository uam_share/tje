<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Penagihan_ongkos extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->privileges->guestAction();
        $this->load->model('penagihan_ongkos_m');
    }

    public function index()
    {
        $data['content'] = 'penagihan_ongkos/list';
        $data['filter_bln'] = date('m');
        $data['filter_thn'] = date('Y');
        $this->load->view('template', $data);
    }

    public function add()
    {
        $data['content'] = 'penagihan_ongkos/add';
        $this->load->view('template', $data);
    }

    public function view($id = '')
    {
        $data['content'] = 'penagihan_ongkos/edit';
        $no = $this->uri->segment(3) . '/' . $this->uri->segment(4) . '/' . $this->uri->segment(5);
        $data['rowedit'] = $this->penagihan_ongkos_m->getListByid($no);

        $this->load->view('template', $data);
    }

    public function edit($id = '')
    {
        $data['content'] = 'penagihan_ongkos/edit';
        $no = $this->uri->segment(3) . '/' . $this->uri->segment(4) . '/' . $this->uri->segment(5);
        $data['rowedit'] = $this->penagihan_ongkos_m->getListByid($no);

        $this->load->view('template', $data);
    }

    public function printed($id = '')
    {
        $data['content'] = 'penagihan_ongkos/print';
        $no = $this->uri->segment(3) . '/' . $this->uri->segment(4) . '/' . $this->uri->segment(5);
        $status_tagih = $this->uri->segment(6);
        $format = $this->uri->segment(7);
        $data['rowedit'] = $this->penagihan_ongkos_m->getListByid($no);
        $nokirim = isset($data['rowedit']->nokirim) ? $data['rowedit']->nokirim : '';
        $data['grid'] = $this->penagihan_ongkos_m->getDataKirim($nokirim, $status_tagih, true);
        $data['print_title'] = 'PENAGIHAN ONGKOS';

        switch ($format) {
            case 'pdf':
                error_reporting(1);
                $html = $this->load->view('template-print', $data, true);

                require APPPATH . '../MPDF57/mpdf.php';
                $mypdf = new mPDF('c', 'A4', '', '', 5, 5, 5, 5, 0, 0);
                // $mpdf->debug = true;
                $mypdf->WriteHTML($html);
                $mypdf->Output();
            default:
                $this->load->view('template-print', $data);
        }

    }

    public function get_data()
    {
        $configs = array(
            'id' => 'm_tagih_no',
            'aColumns' => array('m_tagih_tgl', 'm_tagih_fk', 'tglkirim', 'nopol', 'keterangan', 'status_cek'),
            'datamodel' => 'penagihan_ongkos_m',
            'actiontable' => array(
                'print' => array(
                    'href' => 'penagihan_ongkos/printed/{keyId}/-1/pdf',
                    'label' => '<i class="fa fa-print"></i>',
                    'title' => 'Ubah Data',
                    'target' => '_blank',
                ),
                'edit' => array(
                    'href' => 'penagihan_ongkos/edit',
                    'label' => '<i class="fa fa-edit"></i>',
                    'title' => 'Ubah Data',
                ),
                'delete' => array(
                    'href' => 'penagihan_ongkos/delete',
                    'label' => '<i class="fa fa-trash"></i>',
                    'title' => 'Hapus Data',
                    'onclick' => 'return deleteData()',
                ),
            ),
        );
        echo $this->crud_m->get_data($configs);
    }

    public function get_no_kirim($date = '')
    {
        $pdate = (!empty($date)) ? $date : date('Y-m-d');
        $month = date('m', strtotime($pdate));
        $year = date('Y', strtotime($pdate));
        $lastno = $this->penagihan_ongkos_m->getLastNo($year, $month);
        $rest['no_kirim'] = $this->config->item('site_code') . $this->config->item('tagih_code') . '-' .
                    str_pad($lastno, 4, 0, STR_PAD_LEFT) . '/' . $this->fungsi->blnTOromawi($month) . '/' . date('y', strtotime($pdate));
        echo json_encode($rest);
    }

    public function get_detail_kirim($edit = false)
    {
        //var_dump($edit);
        $nopol = $_POST['no_polisi'];
        $status_tagih = !empty($_POST['status_tagih']) ? $_POST['status_tagih'] : -1    ;
        $result = array();
        $result['grid'] = $this->penagihan_ongkos_m->getDataKirim($nopol, $status_tagih, $edit);
        //echo $this->db->last_query();
        echo json_encode($result);
    }

    public function get_nopol()
    {
        $query = $_POST['query'];
        $result = $this->penagihan_ongkos_m->getNopol($query);
        echo json_encode($result);
    }

    public function simpan()
    {
        $this->db->trans_start();
        $data = array(
            'm_tagih_no' => $this->input->post('m_tagih_no'),
            'm_tagih_tgl' => $this->input->post('m_tagih_tgl'),
            'm_tagih_fk' => $this->input->post('m_tagih_fk'),
        );
        $nokirim = $this->input->post('nokirim');

        $stats = $this->input->post('stat');

        $insertData = $this->crud_m->insert('m_tagih', $data, true);
        if ($insertData === true) {
            $stats = $this->input->post('stat');
            $id_kirim = $this->input->post('id_kirim');
            $ket_tagihs = $this->input->post('ket_tagih');
            $jml_ongkoss = $this->input->post('jml_ongkos');
            $bb_jkt = $this->input->post('bb_jkt');
            if (!empty($id_kirim)) {
                foreach ($id_kirim as $key => $val) {
                    $datainsert = array();
                    $datainsert['ket_tagih'] = (isset($ket_tagihs[$key])) ? $ket_tagihs[$key] : '';
                    $datainsert['jml_ongkos'] = (isset($jml_ongkoss[$key])) ? $jml_ongkoss[$key] : 0;
                    $datainsert['jml_ongkos'] = str_replace(".", '', $datainsert['jml_ongkos']);
                    $datainsert['bb_jkt'] = (isset($bb_jkt[$key])) ? $bb_jkt[$key] : 0;
                    $datainsert['bb_jkt'] = str_replace(".", '', $datainsert['bb_jkt']);

                    if (is_array($stats)) {
                        if (in_array($val, $stats)) {
                            $datainsert['stat'] = '3';
                        }
                    }
                    $insertDataDetail = $this->crud_m->update('tt_kirim', $datainsert, array('id' => $val));
                    if (!$insertDataDetail) {
                        $this->crud_m->delete('m_tagih', array('m_tagih_no' => $data['m_tagih_no']));
                        $this->crud_m->update('tt_kirim', array('stat' => '2'), array('nokirim' => $nokirim));
                        $error = $this->crud_m->result($insertDataDetail);
                        $msg['resp'] = 'KESALAHAN SAAT INSERT DETAIL : ' . $error['message'];
                        break;
                    }
                }
            }
            if (isset($msg['resp'])) {
                $msg['save'] = false;
                $msg['resp'] = $msg['resp'];
            } else {
                $qcek = $this->db->query("SELECT nokirim FROM tt_kirim WHERE stat in ('1','2') AND nokirim='" . $nokirim . "'")->num_rows();
                if ($qcek == 0) {
                    $this->crud_m->update('m_kirim', array('stat' => '3'), array('nokirim' => $nokirim));
                } else {
                    $this->crud_m->update('m_kirim', array('stat' => '2'), array('nokirim' => $nokirim));
                }
                $msg['save'] = true;
                $msg['resp'] = 'SUKSES : Data berhasil disimpan.';
            }
        } else {
            $error = $this->crud_m->result($insertData);
            $msg['save'] = false;
            $msg['resp'] = 'KESALAHAN SAAT INSERT HEADER : ' . $error['error'];
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $error = $this->db->error();
            $msg['save'] = false;
            // $msg['resp'] = !empty($error['message']) ? $error['message'] : 'Data Gagal disimpan';
        } else {
            $msg['save'] = true;
            $msg['resp'] = 'Data Berhasil Disimpan';
        }
        echo json_encode($msg);
    }

    public function update()
    {
        $this->db->trans_start();
        $data = array(
            'm_tagih_no' => $this->input->post('m_tagih_no'),
            'm_tagih_tgl' => $this->input->post('m_tagih_tgl'),
            'm_tagih_fk' => $this->input->post('m_tagih_fk'),
        );
        $nokirim = $this->input->post('nokirim');
        $stats = $this->input->post('stat');
        $stat_old = $this->input->post('stat_old');

        $insertData = $this->crud_m->update('m_tagih', $data, array('m_tagih_no' => $data['m_tagih_no']), true);
        $this->crud_m->update('tt_kirim', array('stat' => '2'), array('nokirim' => $nokirim));
        if ($insertData === true) {
            $stats = $this->input->post('stat');
            $id_kirim = $this->input->post('id_kirim');
            $ket_tagihs = $this->input->post('ket_tagih');
            $jml_ongkoss = $this->input->post('jml_ongkos');
            $bb_jkt = $this->input->post('bb_jkt');
            if (!empty($id_kirim)) {
                foreach ($id_kirim as $key => $val) {
                    $datainsert = array();
                    $datainsert['ket_tagih'] = (isset($ket_tagihs[$key])) ? $ket_tagihs[$key] : '';
                    $datainsert['jml_ongkos'] = (isset($jml_ongkoss[$key])) ? $jml_ongkoss[$key] : 0;
                    $datainsert['jml_ongkos'] = str_replace(".", '', $datainsert['jml_ongkos']);
                    $datainsert['bb_jkt'] = (isset($bb_jkt[$key])) ? $bb_jkt[$key] : 0;
                    $datainsert['bb_jkt'] = str_replace(".", '', $datainsert['bb_jkt']);
                    if (is_array($stats)) {
                        if (in_array($val, $stats)) {
                            $datainsert['stat'] = '3';
                        } else {
                            $datainsert['stat'] = '2';
                        }

                    } else {
                        $datainsert['stat'] = '2';
                    }
                    $insertDataDetail = $this->crud_m->update('tt_kirim', $datainsert, array('id' => $val));
                    if (!$insertDataDetail) {
                        $this->crud_m->delete('m_tagih', array('m_tagih_no' => $data['m_tagih_no']));
                        $error = $this->crud_m->result($insertDataDetail);
                        $msg['resp'] = 'KESALAHAN SAAT INSERT DETAIL : ' . $error['message'];
                        break;
                    }
                }
            }
            if (isset($msg['resp'])) {
                $msg['save'] = false;
                $msg['resp'] = $msg['resp'];
            } else {
                $qcek = $this->db->query("SELECT nokirim FROM tt_kirim WHERE (stat IN ('1','2')) 
                                            AND nokirim='" . $nokirim . "'")->num_rows();
                if ($qcek == 0) {
                    $this->crud_m->update('m_kirim', array('stat' => '3'), array('nokirim' => $nokirim));
                } else {
                    $this->crud_m->update('m_kirim', array('stat' => '2'), array('nokirim' => $nokirim));
                }
                $msg['save'] = true;
                $msg['resp'] = 'SUKSES : Data berhasil disimpan.';
            }
        } else {
            $error = $this->crud_m->result($insertData);
            $msg['save'] = false;
            $msg['resp'] = 'KESALAHAN SAAT INSERT HEADER : ' . $error['error'];
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $error = $this->db->error();
            $msg['save'] = false;
            $msg['resp'] = !empty($error['message']) ? $error['message'] : $msg['resp'];
        } else {
            $msg['save'] = true;
            $msg['resp'] = 'Data Berhasil Disimpan';
        }
        echo json_encode($msg);
    }

    public function delete($id = '')
    {
        $this->db->trans_start();

        $no = $this->uri->segment(3) . '/' . $this->uri->segment(4) . '/' . $this->uri->segment(5);
        $query = $this->db->query("SELECT nokirim FROM v_m_tagih WHERE m_tagih_no = '$no'");
        $nofk = $query->row()->nokirim;

        $deleteData = $this->crud_m->delete('m_tagih', array('m_tagih_no' => $no), true);
        if ($deleteData) {
            $this->crud_m->update('tt_kirim', array('stat' => '2'), array('nokirim' => $nofk));
            $this->crud_m->update('m_kirim', array('stat' => '2'), array('nokirim' => $nofk));
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $error = $this->db->error();
            $this->session->set_flashdata('msg', '<div class="alert alert-warning fade in">
                        <button class="close" data-dismiss="alert" type="button">x</button>
                        <strong>Data gagal dihapus</strong>
                        </div>');
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-info fade in">
                        <button class="close" data-dismiss="alert" type="button">x</button>
                        <strong>Data berhasil dihapus</strong>
                        </div>');
        }
        redirect('/penagihan_ongkos');
    }

}
