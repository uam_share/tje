<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Tagih_ongkos extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->privileges->guestAction();
        $this->load->model('M_tagih_ongkos');
    }

    public function index()
    {
        $data['content'] = 'tagih_ongkos/index';
        $data['page_title'] = 'Tanda Kirim barang';
        $this->load->view('template', $data);
    }

    public function add()
    {
        $data['page_title'] = 'Tanda Kirim barang';
        $data['content'] = 'tagih_ongkos/add';
        $this->load->view('template', $data);
    }

    public function edit()
    {
        $data['page_title'] = 'Tanda Kirim barang';
        $data['content'] = 'tagih_ongkos/edit';
        $no = $this->uri->segment(3) . '/' . $this->uri->segment(4) . '/' . $this->uri->segment(5);
        $data['rowedit'] = $this->M_tagih_ongkos->getListByid($no);
        $this->load->view('template', $data);
    }

    public function get_no_kirim()
    {
        $lastno = $this->M_tagih_ongkos->getNoKirim(date('Y'), date('m'));
        $rest['no_kirim'] = $this->config->item('site_code') . $this->config->item('kirim_code') . '-' .
        str_pad($lastno, 4, 0, STR_PAD_LEFT) . '/' . $this->fungsi->blnTOromawi(date('m')) . '/' . date('y');

        echo json_encode($rest);
    }

    public function get_nopol()
    {
        $query = $_POST['query'];
        $result = $this->M_tagih_ongkos->getNopol($query);
        echo json_encode($result);
    }

    public function get_detail_muat()
    {
        $nopol = $_POST['no_polisi'];
        $result = array();
        $result['grid'] = $this->M_tagih_ongkos->getDataMuat($nopol);
        echo json_encode($result);
    }

    public function get_detail_kirim()
    {
        $nomor = $_POST['nokirim']; //$this->input->post('nokirim');
        //echo $nomor;
        $result = array();
        $result['grid'] = $this->M_tagih_ongkos->getDataKirim($nomor);
        echo json_encode($result);
    }

    public function get_data()
    {
        $configs = array(
            'id' => 'NOKIRIM',
            'aColumns' => array('NOKIRIM', 'TGLKIRIM', 'NOPOL', 'KETERANGAN'),
            'datamodel' => 'M_tagih_ongkos',
            'actiontable' => array(
                'print' => array(
                    // '<a href="' . base_url() . 'report/r_kirim_barang/' . $aRow['NOKIRIM'] . '/Perincian_Ongkos" target="_blank">
                    'href' => base_url() . 'report/r_kirim_barang',
                    'label' => '<i class="fa fa-print"></i>',
                    'title' => 'Cetak Data',
                    'class' => 'print-muat-barang',
                    'target' => '_blank',
                ),
                'edit' => array(
                    'href' => 'tagih_ongkos/edit',
                    'label' => '<i class="fa fa-edit"></i>',
                    'title' => 'Ubah Data',
                ),
                'delete' => array(
                    'href' => 'tagih_ongkos/delete',
                    'label' => '<i class="fa fa-trash"></i>',
                    'title' => 'Hapus Data',
                    'onclick' => 'return deleteData()',
                ),
            ),
        );
        echo $this->crud_m->get_data($configs);
    }

    public function simpan()
    {
        $this->db->trans_start();
        $data = array(
            'NOKIRIM' => $_POST['no_kirim'],
            'TGLKIRIM' => $_POST['tanggal'],
            'NOPOL' => $_POST['no_polisi'],
            'KETERANGAN' => $_POST['keterangan'],
        );
        $query = $this->db->insert('m_kirim', $data);
        if ($query) {
            $save = $this->M_tagih_ongkos->saveMuatToKirim($_POST['no_polisi']);
            if ($save) {
                $this->M_tagih_ongkos->delete('nopol', $_POST['no_polisi'], 'mtt_muat');
            }
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $error = $this->db->error();
            $msg['save'] = false;
            $msg['resp'] = !empty($error['message']) ? $error['message'] : 'Data Gagal disimpan';
        } else {
            $msg['save'] = true;
            $msg['resp'] = 'Data Berhasil Disimpan';
        }
        echo json_encode($msg);
    }

    public function update()
    {
        $this->db->trans_start();
        $data = array(
            'NOKIRIM' => $_POST['no_kirim'],
            'TGLKIRIM' => $_POST['tanggal'],
            'NOPOL' => $_POST['no_polisi'],
            'KETERANGAN' => $_POST['keterangan'],
        );

        $query = $this->crud_m->update('m_kirim', $data, array('NOKIRIM' => $data['NOKIRIM']));

        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $error = $this->db->error();
            $msg['save'] = false;
            $msg['resp'] = !empty($error['message']) ? $error['message'] : 'Data Gagal disimpan';
        } else {
            $msg['save'] = true;
            $msg['resp'] = 'Data Berhasil Disimpan';
        }
        echo json_encode($msg);
    }

    public function delete()
    {
        
        $nomor = $this->uri->segment(3) . '/' . $this->uri->segment(4) . '/' . $this->uri->segment(5);
        // Ceck MCek or MTagih
        $sqtrSql = "SELECT m_cek_no AS fkno FROM m_cek WHERE m_cek_fk = '$nomor' UNION 
                    SELECT m_tagih_no AS fkno FROM m_tagih WHERE m_tagih_fk ='$nomor'";
        $query = $this->db->query($sqtrSql);

        // delete mtt_maut
        if($query->num_rows() == 0){
            $this->db->trans_start();
            // back to mtt_muat
            $this->backtoMuat($nomor);
            $this->crud_m->delete('m_kirim', array('nokirim' => $nomor));

            $this->db->trans_complete();
            if ($this->db->trans_status() === false) {
                $error = $this->db->error();
                $this->session->set_flashdata('msg', '<div class="alert alert-warning fade in">
                            <button class="close" data-dismiss="alert" type="button">x</button>
                            <strong>Data gagal dihapus</strong>
                            </div>');
            } else {
                $this->session->set_flashdata('msg', '<div class="alert alert-info fade in">
                            <button class="close" data-dismiss="alert" type="button">x</button>
                            <strong>Data berhasil dihapus</strong>
                            </div>');
            }
        }else{
            $this->session->set_flashdata('msg', '<div class="alert alert-warning fade in">
                            <button class="close" data-dismiss="alert" type="button">x</button>
                            <strong>Data yang sudah di crosscek atau sudah ditagih tidak bisa dihapus </strong>
                            </div>');
        }
        redirect('/tagih_ongkos');
    }

    private function backtoMuat($noKirim, $oldNo = null)
    {
        $shouldBack = true;
        $datarollbacktkirim = $this->M_tagih_ongkos->getdatarollbacktkirim($noKirim);
        if ($shouldBack && !empty($datarollbacktkirim) && !empty($datarollbacktkirim['dataH'])) {
            $this->db->insert_batch('mtt_muat', $datarollbacktkirim['dataH']);
            if (!empty($datarollbacktkirim['dataD'])) {
                $this->db->insert_batch('tt_muat', $datarollbacktkirim['dataD']);
            }
        }
    }

}
