<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Terima_barang extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->privileges->guestAction();
        $this->load->model('M_terima_barang');
        $this->load->model('M_penerima');
        $this->load->model('M_pengirim');
    }

    public function index()
    {
        $data['content'] = 'terima_barang/index';
        $this->load->view('template', $data);
    }

    public function add()
    {
        $data['content'] = 'terima_barang/add';
        $this->load->view('template', $data);
    }

    public function edit()
    {
        $noterima = $this->uri->segment(3);
        $data['rows'] = $this->M_terima_barang->getRows($noterima);
        $data['content'] = 'terima_barang/edit';
        $this->load->view('template', $data);
    }

    public function get_table()
    {
        $noterima = $_POST['no_terima'];
        $result['grid'] = $this->M_terima_barang->getGridAntri($noterima);
        echo json_encode($result);
    }

    public function get_penerima()
    {
        $query = (empty($_POST['query'])) ? 'XXXXXXXXX' : $_POST['query'];
        // $result = $this->M_terima_barang->getTypehead('m_penerima', 'nmpenerima', $query);
        $result = $this->M_penerima->getAsList($query);
        echo json_encode($result);
    }

    public function get_pengirim()
    {
        $query = (empty($_POST['query'])) ? 'XXXXXXXXX' : $_POST['query'];
        // $result = $this->M_terima_barang->getTypehead('m_pengirim', 'nmpengirim', $query);
        $result = $this->M_pengirim->getAsList($query);
        echo json_encode($result);
    }

    public function get_data()
    {
        $configs = array(
            'id' => 'NoTerima',
            'aColumns' => array('NoTerima', 'tglterima', 'nmpenerima', 'nmpengirim', 'STATUS'),
            'datamodel' => 'M_terima_barang',
            'actiontable' => array(
                'print' => array(
                    // $row[] = '<a href="' . base_url() . 'report/r_terima_barang/' . $aRow['NoTerima'] . '/terima_barang" target="_blank">
                    'href' => base_url() . 'report/r_terima_barang',
                    'label' => '<i class="fa fa-print"></i>',
                    'title' => 'Cetak Data',
                    'target' => '_blank',
                ),
                'edit' => array(
                    'href' => 'terima_barang/edit',
                    'label' => '<i class="fa fa-edit"></i>',
                    'title' => 'Ubah Data',
                ),
                'delete' => array(
                    'href' => 'terima_barang/delete',
                    'label' => '<i class="fa fa-trash"></i>',
                    'title' => 'Hapus Data',
                    'onclick' => 'return deleteData()',
                ),
            ),
        );
        echo $this->crud_m->get_data($configs);
    }

    public function simpan()
    {
        $msg = array();
        $check = $this->M_terima_barang->checkNoTerima($_POST['no_terima']);
        if ($check > 0) {
            $msg['save'] = false;
            $msg['resp'] = 'NO TERIMA SUDAH ADA';
        } else {
            $msg = $this->M_terima_barang->add($_POST);
        }

        echo json_encode($msg);
    }

    public function update()
    {
        echo json_encode($this->M_terima_barang->edit($_POST));
    }

    public function delete()
    {
        $noterima = rawurldecode($this->uri->segment(3));
        if ($this->M_terima_barang->delete($noterima) === false) {
            // $error = $this->db->error();
            // echo $this->fungsi->warning(!empty($error['message']) ? $error['message'] : 'Data Gagal dihapus', base_url('terima_muat'));
            $this->session->set_flashdata('msg', '<div class="alert alert-warning fade in">
                        <button class="close" data-dismiss="alert" type="button">x</button>
                        <strong>Data gagal dihapus</strong>
                        </div>');
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-info fade in">
                        <button class="close" data-dismiss="alert" type="button">x</button>
                        <strong>Data berhasil dihapus</strong>
                        </div>');
        }
        redirect('/terima_barang');
    }

}
