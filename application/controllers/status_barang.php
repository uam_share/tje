<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Status_barang extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_terima_barang');
        $this->privileges->guestAction();
    }

    public function index()
    {
        if ($this->privileges->is_logged_in()) {
            $data['filter_bln'] = $this->input->post('filter_bln') ? $this->input->post('filter_bln') : date('m');
            $data['filter_thn'] = $this->input->post('filter_thn') ? $this->input->post('filter_thn') : date('Y');
            $data['content'] = 'status_barang/list';
            $data['nosearch'] = '';
            $this->load->view('template', $data);
        } else {
            $this->load->view('login');
        }
    }

    public function caristatus()
    {
        $jkriteria = $this->input->post('filter_no');

        $data['hasil'] = false;
        $data['filter_no'] = $jkriteria;

        $data['filter_bln'] = $this->input->post('filter_bln') ? $this->input->post('filter_bln') : date('m');
        $data['filter_thn'] = $this->input->post('filter_thn') ? $this->input->post('filter_thn') : date('Y');

        $where['kirim'] = "AND (MONTH(tglkirim)={$data['filter_bln']} AND YEAR(tglkirim)={$data['filter_thn']})";
        $where['muat'] = "AND (MONTH(tglmuat)={$data['filter_bln']} AND YEAR(tglmuat)={$data['filter_thn']})";
        $where['terima'] = "AND (MONTH(tglterima)={$data['filter_bln']} AND YEAR(tglterima)={$data['filter_thn']})";

        switch ($jkriteria) {
            default:
                $data['nosearch'] = '';
                break;
            case 'noterima':
                $no = $this->input->post('nosearch');
                $data['nosearch'] = $no;
                $data['msg'] = "Data dengan no barang : \" $no \" tidak ditemukan !!!";
                $query = $this->getTerimaBarang($no, $where, true);
                if ($query) {
                    $data['hasil'] = true;
                    $data['msg'] = "Status barang no : \" $no \" baru diterima";
                    $data['querydata'] = $query;
                } else {
                    $query = $this->getMuatBarang($no, $where, true);
                    if ($query) {
                        $data['hasil'] = true;
                        $data['msg'] = "Status barang no : \" $no \" sudah dimuat";
                        $data['querydata'] = $query;
                    } else {
                        $query = $this->getKirimBarang($no, $where, true);
                        if ($query) {
                            $data['hasil'] = true;
                            $data['msg'] = "Status barang no : \" $no \" sudah terkirim";
                            $data['querydata'] = $query;
                        }
                    }
                }
                break;
            case 'nmpengirim':
                $data['nmpengirim'] = $this->input->post('pengirim');
                $data['msg'] = 'Data dengan pengirim : "' . $data['nmpengirim'] . '" tidak ditemukan !!!';
                $query = $this->searchByPengirim($data['nmpengirim'], $where);
                if ($query == false) {
                    $query = $this->searchByPengirim($data['nmpengirim'], $where, $this->M_terima_barang->getDb()->database);
                }
                if ($query) {
                    $data['hasil'] = true;
                    $data['msg'] = '';
                    $data['querydata'] = $query;
                }
                break;
            case 'nmpenerima':
                $data['nmpenerima'] = $this->input->post('penerima');
                $data['msg'] = 'Data dengan penerima : "' . $data['nmpenerima'] . '" tidak ditemukan !!!';
                $query = $this->searchByPenerima($data['nmpenerima'], $where);
                if ($query == false) {
                    $query = $this->searchByPenerima($data['nmpenerima'], $where, $this->M_terima_barang->getDb()->database);
                }
                if ($query) {
                    $data['hasil'] = true;
                    $data['msg'] = '';
                    $data['querydata'] = $query;
                }
                break;
        }
        $data['content'] = 'status_barang/list';
        $this->load->view('template', $data);
    }

    public function getTerimaBarang($no, $where = '', $unionDb = false)
    {
        $sql = "SELECT h.noterima,tglterima,h.status,nmpenerima,nmpengirim,'JAKARTA' as status_barang,
            '' as nopol, '' as nomuat,'' as tglmuat,'' as nokirim, '' as tglkirim, d.`barang`,d.`banyak`,d.`satuan`,d.`jumlah`
            FROM mtt_antri h INNER JOIN tt_antri d ON h.`NoTerima`=d.`Noterima` WHERE h.noterima = '$no' {$where['terima']}";
        if ($unionDb != false) {
            $dbName = $this->M_terima_barang->getDb()->database;
            $sql = "SELECT h.noterima,tglterima,h.status,nmpenerima,nmpengirim,'JAKARTA' as status_barang,
            '' as nopol, '' as nomuat,'' as tglmuat,'' as nokirim, '' as tglkirim, d.`barang`,d.`banyak`,d.`satuan`,d.`jumlah`
            FROM $dbName.mtt_antri h INNER JOIN $dbName.tt_antri d ON h.`NoTerima`=d.`Noterima` WHERE h.noterima = '$no' {$where['terima']}";
        }
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getMuatBarang($no, $where, $unionDb = false)
    {
        $sqlStr = "SELECT
                nomor AS noterima,
                tgl AS tglterima,
                h.noterima AS nomuat,
                h.nopol,
                tglterima AS tglmuat,
                '' AS nokirim,
                '' AS tglkirim,
                nmpenerima,
                nmpengirim,
                    d.`barang`,
                    d.`banyak`,
                    d.`satuan`,
                    d.`jumlah`,
                'BERANGKAT' AS status_barang
            FROM mtt_muat h
            INNER JOIN tt_muat d ON h.`noterima` = d.`noterima` WHERE (h.noterima = '$no' OR h.nomor='$no') {$where['terima']} ";
        if ($unionDb) {
            $dbName = $this->M_terima_barang->getDb()->database;
            $sqlStr .= 'UNION ';
            $sqlStr .= "SELECT
                nomor AS noterima,
                tgl AS tglterima,
                h.noterima AS nomuat,
                h.nopol,
                tglterima AS tglmuat,
                '' AS nokirim,
                '' AS tglkirim,
                nmpenerima,
                nmpengirim,
                    d.`barang`,
                    d.`banyak`,
                    d.`satuan`,
                    d.`jumlah`,
                'BERANGKAT' AS status_barang
            FROM $dbName.mtt_muat h
            INNER JOIN $dbName.tt_muat d ON h.`noterima` = d.`noterima` WHERE (h.noterima = '$no' OR h.nomor='$no') {$where['terima']}";
        }
        $query = $this->db->query($sqlStr);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function getKirimBarang($no, $where = '', $unionDb = false)
    {
        $sqlStr = "SELECT
                h.nokirim,h.tglkirim,h.nopol,h.keterangan,
                d.noterima,nopol,nomuat,tglterima,tglmuat,
                nmpenerima,nmpengirim,
                d.`barang`,
                d.`banyak`,
                d.`satuan`,
                d.`jumlah`,
                (CASE
                    WHEN `d`.`stat` = '0' OR `d`.`stat2` = '0' THEN 'CLOSED'
                    WHEN `d`.`stat` = '1' AND `d`.`stat2` = '1' THEN 'BERANGKAT'
                    WHEN `d`.`stat2` = '2' OR `d`.`stat` = '2' THEN 'CUSTOMER'
                    WHEN `d`.`stat` = '3' THEN 'LUNAS' END) AS `status_barang`
            FROM m_kirim h
            INNER JOIN tt_kirim d ON h.`nokirim` = d.`nokirim`
            WHERE (h.nokirim = '$no' OR noterima='$no' OR nomuat='$no') {$where['terima']} ";
        if ($unionDb) {
            $dbName = $this->M_terima_barang->getDb()->database;
            $sqlStr .= 'UNION ';
            $sqlStr .= "SELECT
                h.nokirim,h.tglkirim,h.nopol,h.keterangan,
                d.noterima,nopol,nomuat,tglterima,tglmuat,
                nmpenerima,nmpengirim,
                d.`barang`,
                d.`banyak`,
                d.`satuan`,
                d.`jumlah`,
                (CASE
                    WHEN `d`.`stat` = '0' OR `d`.`stat2` = '0' THEN 'CLOSED'
                    WHEN `d`.`stat` = '1' AND `d`.`stat2` = '1' THEN 'BERANGKAT'
                    WHEN `d`.`stat2` = '2' OR `d`.`stat` = '2' THEN 'CUSTOMER'
                    WHEN `d`.`stat` = '3' THEN 'LUNAS' END) AS `status_barang`
            FROM $dbName.m_kirim h
            INNER JOIN $dbName.tt_kirim d ON h.`nokirim` = d.`nokirim`
            WHERE (h.nokirim = '$no' OR noterima='$no' OR nomuat='$no') {$where['terima']}";
        }
        $query = $this->db->query($sqlStr);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function searchByPengirim($nmpengirim, $where = '', $db = null)
    {
        $strSql = $this->searchByPengirimStr($nmpengirim, $where);
        $strSql .= 'UNION ';
        $strSql .= $this->searchByPengirimStr($nmpengirim, $where, $this->M_terima_barang->getDb()->database);
        $query = $this->db->query($strSql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function searchByPenerima($nmpengirim, $where = '')
    {
        $strSql = $this->searchByPenerimaStr($nmpengirim, $where);
        $strSql .= 'UNION ';
        $strSql .= $this->searchByPenerimaStr($nmpengirim, $where, $this->M_terima_barang->getDb()->database);
        $query = $this->db->query($strSql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    private function searchByPengirimStr($nmpengirim, $where = '', $db = null)
    {
        $tableMttAntri = (!empty($db)) ? "$db.mtt_antri" : 'mtt_antri';
        $tableTTAntri = (!empty($db)) ? "$db.tt_antri" : 'tt_antri';
        $tableMttMuat = (!empty($db)) ? "$db.mtt_muat" : 'mtt_muat';
        $tableTTMuat = (!empty($db)) ? "$db.tt_muat" : 'tt_muat';
        $tableMkirim = (!empty($db)) ? "$db.m_kirim" : 'm_kirim';
        $tableTTkirim = (!empty($db)) ? "$db.tt_kirim" : 'tt_kirim';
        $strSql = "(SELECT
                h.noterima,
                tglterima,
                nmpenerima,
                nmpengirim,
                '' AS nomuat,
                    '' AS nopol,
                    '' AS tglmuat,
                    '' AS nokirim,
                    '' AS tglkirim,
                    d.`barang`,
                    d.`banyak`,
                    d.`satuan`,
                    d.`jumlah`,
                    'JAKARTA' AS status_barang
            FROM $tableMttAntri h INNER JOIN $tableTTAntri d ON h.`NoTerima`=d.`Noterima` WHERE nmpengirim='$nmpengirim' {$where['terima']})
            UNION
            (SELECT
                nomor AS noterima,
                tgl AS tglterima,
                nmpenerima,
                nmpengirim,
                h.noterima AS nomuat,
                    h.nopol,
                tglterima AS tglmuat,
                '' AS nokirim,
                '' AS tglkirim,
                d.`barang`,
                    d.`banyak`,
                    d.`satuan`,
                    d.`jumlah`,
                'BERANGKAT' AS status_barang
            FROM $tableMttMuat h
            INNER JOIN $tableTTMuat d ON h.`noterima` = d.`noterima` WHERE nmpengirim='$nmpengirim' {$where['terima']})
            UNION
            (SELECT
                 d.noterima,
                 d.tglterima,
                 nmpenerima,
                 nmpengirim,
                 d.nomuat,
                 h.nopol,
                 tglmuat,
                 h.nokirim,
                 h.tglkirim,
                 d.`barang`,
                    d.`banyak`,
                    d.`satuan`,
                    d.`jumlah`,
                 (CASE
                WHEN `d`.`stat` = '0' OR `d`.`stat2` = '0' THEN 'CLOSED'
                WHEN `d`.`stat` = '1' AND `d`.`stat2` = '1' THEN 'BERANGKAT'
                WHEN `d`.`stat2` = '2' OR `d`.`stat` = '2' THEN 'CUSTOMER'
                WHEN `d`.`stat` = '3' THEN 'LUNAS' END) AS `status_barang`
            FROM $tableMkirim h
            INNER JOIN $tableTTkirim d ON h.`nokirim` = d.`nokirim`
            WHERE nmpengirim='$nmpengirim' {$where['kirim']}) ";
        return $strSql;
    }

    private function searchByPenerimaStr($nmpengirim, $where = '', $db = null)
    {
        $tableMttAntri = (!empty($db)) ? "$db.mtt_antri" : 'mtt_antri';
        $tableTTAntri = (!empty($db)) ? "$db.tt_antri" : 'tt_antri';
        $tableMttMuat = (!empty($db)) ? "$db.mtt_muat" : 'mtt_muat';
        $tableTTMuat = (!empty($db)) ? "$db.tt_muat" : 'tt_muat';
        $tableMkirim = (!empty($db)) ? "$db.m_kirim" : 'm_kirim';
        $tableTTkirim = (!empty($db)) ? "$db.tt_kirim" : 'tt_kirim';
        $strSql = "(SELECT
            h.noterima,
            tglterima,
            nmpenerima,
            nmpengirim,
            '' AS nomuat,
                '' AS nopol,
                '' AS tglmuat,
                '' AS nokirim,
                '' AS tglkirim,
                d.`barang`,
                d.`banyak`,
                d.`satuan`,
                d.`jumlah`,
                'JAKARTA' AS status_barang
        FROM $tableMttAntri h INNER JOIN $tableTTAntri d ON h.noterima=d.noterima WHERE nmpenerima='$nmpengirim' {$where['terima']})
        UNION
        (SELECT
            nomor AS noterima,
            tgl AS tglterima,
            nmpenerima,
            nmpengirim,
            h.noterima AS nomuat,
                h.nopol,
            tglterima AS tglmuat,
            '' AS nokirim,
            '' AS tglkirim,
            d.`barang`,
                d.`banyak`,
                d.`satuan`,
                d.`jumlah`,
            'BERANGKAT' AS status_barang
        FROM $tableMttMuat h
        INNER JOIN $tableTTMuat d ON h.`noterima` = d.`noterima` WHERE nmpenerima='$nmpengirim' {$where['terima']})
        UNION
        (SELECT
             d.noterima,
             d.tglterima,
             nmpenerima,
             nmpengirim,
             d.nomuat,
             h.nopol,
             tglmuat,
             h.nokirim,
             h.tglkirim,
             d.`barang`,
                d.`banyak`,
                d.`satuan`,
                d.`jumlah`,
             (CASE
            WHEN `d`.`stat` = '0' OR `d`.`stat2` = '0' THEN 'CLOSED'
            WHEN `d`.`stat` = '1' AND `d`.`stat2` = '1' THEN 'BERANGKAT'
            WHEN `d`.`stat2` = '2' OR `d`.`stat` = '2' THEN 'CUSTOMER'
            WHEN `d`.`stat` = '3' THEN 'LUNAS' END) AS `status_barang`
        FROM $tableMkirim h
        INNER JOIN $tableTTkirim d ON h.`nokirim` = d.`nokirim` WHERE nmpenerima='$nmpengirim' {$where['kirim']}) ";
        return $strSql;
    }
}
