<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Penerima extends CI_Controller {

	function  __construct()
    {
        parent::__construct();
        $this->privileges->guestAction();
		$this->load->model('M_penerima');
    }
	
	public function index()
	{
		$data['content'] = 'penerima/index';
		$this->load->view('template',$data);
	}
	
	public function get_data()
    {
        $configs = array(
            'id' => 'nmpenerima',
            'aColumns' => array('nmpenerima', 'alamat', 'telp', 'email'),
            'datamodel' => 'M_penerima',
            'actiontable' => array(
                'edit' => array(
                    'href' => 'penerima/form',
                    'label' => '<i class="fa fa-edit"></i>', //"<img src='" . base_url() . "/assets/ico/ubah.png' />",
                    'title' => 'Ubah Data',
                ),
                'delete' => array(
                    'href' => 'penerima/delete',
                    'label' => '<i class="fa fa-trash"></i>', //"<img src='" . base_url() . "/assets/ico/hapus.png' />",
                    'title' => 'Hapus Data',
                    'onclick' => 'return deleteData()',
                ),
            ),
        );
        echo $this->crud_m->get_data($configs);
    }

	function form(){
		$nama = rawurldecode($this->uri->segment(3));
		$this->form_validation->set_rules('nmpenerima', 'Nmpenerima', 'required');
		$this->form_validation->set_message('required', '<span style="color:red;font-weight:bold;">*wajib diisi!!</span>');
		if ($this->form_validation->run()) {
			if ($_POST) {
				if ($nama) {
					foreach($_POST as $row => $key){
						$data[$row] = $key;
					}
					$this->db->where('nmpenerima',$nama);
					$this->db->update('m_penerima',$data);
					$this->session->set_flashdata('msg', '<div class="alert alert-info fade in">
							<button class="close" data-dismiss="alert" type="button">x</button>
							<strong>Data berhasil diupdate</strong>
							</div>');
					redirect('/penerima');
				}else{
					foreach($_POST as $row => $key){
						$data[$row] = $key;
					}
					$this->db->insert('m_penerima',$data);
					$this->session->set_flashdata('msg', '<div class="alert alert-info fade in">
							<button class="close" data-dismiss="alert" type="button">x</button>
							<strong>Data berhasil disimpan</strong>
							</div>');
					redirect('/penerima');
				}
			}
		}
		$query = $this->db->query("SELECT * FROM m_penerima WHERE nmpenerima='$nama'");
		$data['result'] = $query->result_array();
		$data['content'] = 'penerima/form';
		$this->load->view('template',$data);
	}
	
	function delete(){
		$nama = rawurldecode($this->uri->segment(3));
		$this->db->where('nmpenerima', $nama);
		$this->db->delete('m_penerima');
		
		$this->session->set_flashdata('msg', '<div class="alert alert-info fade in">
						<button class="close" data-dismiss="alert" type="button">x</button>
						<strong>Data berhasil dihapus</strong>
						</div>');
		redirect('/penerima');
	}
}
