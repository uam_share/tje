<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Crosscek_kirim_barang extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->privileges->guestAction();
        $this->load->model('cross_cek_kirim_barang_m');
    }

    public function index()
    {
        $data['page_title'] = 'Crosscek Pengiriman Barang';
        $data['content'] = 'cross_cek_kirim_barang/list';
        $data['filter_bln'] = date('m');
        $data['filter_thn'] = date('Y');
        $this->load->view('template', $data);
    }

    public function add()
    {
        $data['page_title'] = 'Crosscek Pengiriman Barang';
        $data['content'] = 'cross_cek_kirim_barang/add';
        $this->load->view('template', $data);
    }

    public function view($id = '')
    {
        $data['page_title'] = 'Crosscek Pengiriman Barang';
        $data['content'] = 'cross_cek_kirim_barang/edit';
        $no = $this->uri->segment(3) . '/' . $this->uri->segment(4) . '/' . $this->uri->segment(5);
        $data['rowedit'] = $this->cross_cek_kirim_barang_m->getListByid($no);
        $this->load->view('template', $data);
    }

    public function edit($id = '')
    {
        $data['page_title'] = 'Crosscek Pengiriman Barang';
        $data['content'] = 'cross_cek_kirim_barang/edit';
        $no = $this->uri->segment(3) . '/' . $this->uri->segment(4) . '/' . $this->uri->segment(5);
        $data['rowedit'] = $this->cross_cek_kirim_barang_m->getListByid($no);
        $this->load->view('template', $data);
    }

    public function get_data()
    {
        $configs = array(
            'id' => 'm_cek_no',
            'aColumns' => array('m_cek_tgl', 'NOKIRIM', 'TGLKIRIM', 'NOPOL', 'KETERANGAN', 'status_cek'),
            'datamodel' => 'cross_cek_kirim_barang_m',
            'actiontable' => array(
                'print' => array(
                    'href' => 'report/r_crosscek_kirim_barang',
                    'label' => '<i class="fa fa-print"></i>', //"<img src='" . base_url() . "/assets/ico/ubah.png' />",
                    'title' => 'Ubah Data',
                    'target' => '_blank',
                ),
                'edit' => array(
                    'href' => 'crosscek_kirim_barang/edit',
                    'label' => '<i class="fa fa-edit"></i>', //"<img src='" . base_url() . "/assets/ico/ubah.png' />",
                    'title' => 'Ubah Data',
                ),
                'delete' => array(
                    'href' => 'crosscek_kirim_barang/delete',
                    'label' => '<i class="fa fa-trash"></i>', //"<img src='" . base_url() . "/assets/ico/hapus.png' />",
                    'title' => 'Hapus Data',
                    'onclick' => 'return deleteData()',
                ),
            ),
        );
        //echo "<pre>";
        echo $this->crud_m->get_data($configs);
        //echo "</pre>";
    }

    public function get_no_kirim($date = '')
    {
        $pdate = (!empty($date)) ? $date : date('Y-m-d');
        $month = date('m', strtotime($pdate));
        $year = date('Y', strtotime($pdate));
        $lastno = $this->cross_cek_kirim_barang_m->getLastNo($year, $month);
        $rest['no_kirim'] = $this->config->item('site_code') . $this->config->item('crosscek_code') . '-' .
                    str_pad($lastno, 4, 0, STR_PAD_LEFT) . '/' . $this->fungsi->blnTOromawi($month) . '/' . date('y', strtotime($pdate));
        echo json_encode($rest);
    }

    public function get_detail_kirim($edit = false)
    {
        $nopol = $_POST['no_polisi'];
        $result = array();
        $result['grid'] = $this->cross_cek_kirim_barang_m->getDataKirim($nopol, $edit);
        echo json_encode($result);
    }

    public function get_nopol()
    {
        $query = $_POST['query'];
        $result = $this->cross_cek_kirim_barang_m->getNopol($query);
        echo json_encode($result);
    }

    public function simpan()
    {
        $this->db->trans_start();
        $data = array(
            'm_cek_no' => $this->input->post('m_cek_no'),
            'm_cek_tgl' => $this->input->post('m_cek_tgl'),
            'm_cek_fk' => $this->input->post('m_cek_fk'),
        );

        $stats = $this->input->post('stat2');
        $id_kirim = $this->input->post('id_kirim');
        $ket_ceks = $this->input->post('ket_cek');

        $insertData = $this->crud_m->insert('m_cek', $data);
        if ($insertData === true) {
            if (!empty($id_kirim)) {
                foreach ($id_kirim as $key => $val) {
                    $datainsert = array();
                    $datainsert['ket_cek'] = (isset($ket_ceks[$key])) ? $ket_ceks[$key] : '';
                    if (is_array($stats)) {
                        if (in_array($val, $stats)) {
                            $datainsert['stat2'] = '2';
                            $datainsert['stat'] = '2';
                        }

                    }
                    $insertDataDetail = $this->crud_m->update('tt_kirim', $datainsert, array('id' => $val));
                    if (!$insertDataDetail) {
                        $this->crud_m->delete('m_cek', array('m_cek_no' => $data['m_cek_no']));
                        $this->crud_m->update('tt_kirim', array('stat2' => '1', 'ket_cek' => ''), array('nokirim' => $data['m_cek_fk']));
                        $error = $this->crud_m->result($insertDataDetail);
                        $msg['resp'] = 'KESALAHAN SAAT INSERT DETAIL : ' . $error['message'];
                        break;
                    }
                }
            }
            if (isset($msg['resp'])) {
                $msg['save'] = false;
                $msg['resp'] = $msg['resp'];
            } else {
                $qcek = $this->db->query("SELECT nokirim FROM tt_kirim WHERE stat2='1' AND nokirim='" . $data['m_cek_fk'] . "'")->num_rows();
                if ($qcek <= 0) {
                    $this->crud_m->update('m_kirim', array('stat2' => '2'), array('nokirim' => $data['m_cek_fk']));
                }
                $msg['save'] = true;
                $msg['resp'] = 'SUKSES : Data berhasil disimpan.';
            }
        } else {
            $error = $this->crud_m->result($insertData);
            $msg['save'] = false;
            $msg['resp'] = 'KESALAHAN SAAT INSERT HEADER : ' . $error['error'];
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $error = $this->db->error();
            $msg['save'] = false;
            // $msg['resp'] = !empty($error['message']) ? $error['message'] : 'Data Gagal disimpan';
        } else {
            $msg['save'] = true;
            $msg['resp'] = 'Data Berhasil Disimpan';
        }
        echo json_encode($msg);
    }

    public function update()
    {
        $this->db->trans_start();
        $data = array(
            'm_cek_no' => $this->input->post('m_cek_no'),
            'm_cek_tgl' => $this->input->post('m_cek_tgl'),
            'm_cek_fk' => $this->input->post('m_cek_fk'),
        );
        $parentstat = $this->input->post('parentstat');
        $stats = $this->input->post('stat2');
        $stat_old = $this->input->post('stat_old');
        $id_kirim = $this->input->post('id_kirim');
        $ket_ceks = $this->input->post('ket_cek');
        if ($parentstat == '3') {
            $msg['success'] = false;
            $msg['resp'] = 'PERINGATAN : Data sudah LUNAS.';
        } else {
            $insertData = $this->crud_m->update('m_cek', $data, array('m_cek_no' => $data['m_cek_no']));

            if ($insertData === true) {
                if (!empty($id_kirim)) {
                    foreach ($id_kirim as $key => $val) {
                        $datainsert = array();
                        $datainsert['ket_cek'] = (isset($ket_ceks[$key])) ? $ket_ceks[$key] : '';
                        if (is_array($stats)) {
                            if (in_array($val, $stats)) {
                                $datainsert['stat2'] = '2';
                                $datainsert['stat'] = '2';
                            } else {
                                $datainsert['stat2'] = '1';
                                $datainsert['stat'] = '1';
                            }
                        } else {
                            $datainsert['stat2'] = '1'; //$stat_old[$key];
                            $datainsert['stat'] = '1';
                        }
                        $insertDataDetail = $this->crud_m->update('tt_kirim', $datainsert, array('id' => $val));
                        if (!$insertDataDetail) {
                            $error = $this->crud_m->result($insertDataDetail);
                            $msg['resp'] = 'KESALAHAN SAAT INSERT DETAIL : ' . $error['message'];
                            break;
                        }
                    }
                }
                if (isset($msg['resp'])) {
                    $msg['save'] = false;
                    $msg['resp'] = $msg['resp'];
                } else {
                    $qcek = $this->db->query("SELECT nokirim FROM tt_kirim WHERE stat2='1' AND nokirim='" . $data['m_cek_fk'] . "'")->num_rows();
                    if ($qcek <= 0) {
                        $this->crud_m->update('m_kirim', array('stat2' => '2'), array('nokirim' => $data['m_cek_fk']));
                    } else {
                        $this->crud_m->update('m_kirim', array('stat2' => '1'), array('nokirim' => $data['m_cek_fk']));
                    }
                    $msg['save'] = true;
                    $msg['resp'] = 'SUKSES : Data berhasil disimpan.';
                }
            } else {
                $error = $this->crud_m->result($insertData);
                $msg['save'] = false;
                $msg['resp'] = 'KESALAHAN SAAT INSERT HEADER : ' . $error['error'];
            }
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $error = $this->db->error();
            $msg['save'] = false;
            $msg['resp'] = !empty($error['message']) ? $error['message'] : $msg['resp'];
        } else {
            $msg['save'] = true;
            $msg['resp'] = 'Data Berhasil Disimpan';
        }
        echo json_encode($msg);
    }

    public function delete($id = '')
    {
        $no = $this->uri->segment(3) . '/' . $this->uri->segment(4) . '/' . $this->uri->segment(5);
        $query = $this->db->query("SELECT m_cek_fk FROM m_cek WHERE m_cek_no = '$no' AND m_cek_fk NOT IN
                                    (SELECT m_tagih_fk FROM m_tagih)");

        if ($query->num_rows() > 0) {

            $this->db->trans_start();
            $nofk = $query->row()->m_cek_fk;
            $deleteData = $this->crud_m->delete('m_cek', array('m_cek_no' => $no));
            if ($deleteData) {
                $this->crud_m->update('tt_kirim', array('stat2' => '1', 'ket_cek' => ''), array('nokirim' => $nofk));
                $this->crud_m->update('m_kirim', array('stat2' => '1'), array('nokirim' => $nofk));
            } 
            $this->db->trans_complete();
            if ($this->db->trans_status() === false) {
                $error = $this->db->error();
                $this->session->set_flashdata('msg', '<div class="alert alert-warning fade in">
                            <button class="close" data-dismiss="alert" type="button">x</button>
                            <strong>Data gagal dihapus</strong>
                            </div>');
            } else {
                $this->session->set_flashdata('msg', '<div class="alert alert-info fade in">
                            <button class="close" data-dismiss="alert" type="button">x</button>
                            <strong>Data berhasil dihapus</strong>
                            </div>');
            }
        } else {
            // echo $this->fungsi->warning('PERINGATAN : Data sudah LUNAS.', base_url('crosscek_kirim_barang'));
            $this->session->set_flashdata('msg', '<div class="alert alert-info fade in">
                            <button class="close" data-dismiss="alert" type="button">x</button>
                            <strong>Data sudah LUNAS</strong>
                            </div>');
        }
        
        redirect('/crosscek_kirim_barang');
    }

}
