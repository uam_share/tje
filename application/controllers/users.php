<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Users extends CI_Controller
{

    //put your code here
    public function __construct()
    {
        parent::__construct();
        $this->privileges->guestAction();
        $this->load->model('user_m', 'user');
        $this->load->model('menu_m', 'menu');
        $this->load->library('encrypt');
    }

    public function index()
    {
        $this->getList();
    }

    public function getList()
    {
        $data['content'] = 'users/list';
        $this->load->view('template', $data);
    }

    public function get_data()
    {
        $configs = array(
            'id' => 'user_id',
            'aColumns' => array('user_name', 'user_username', 'e_mail', 'user_aktif'),
            'datamodel' => 'user_m',
            'actiontable' => array(
                'edit' => array(
                    'href' => 'users/edit',
                    'label' => '<i class="fa fa-edit"></i>',
                    'title' => 'Ubah Data',
                ),
                'delete' => array(
                    'href' => 'users/delete',
                    'label' => '<i class="fa fa-trash"></i>',
                    'title' => 'Hapus Data',
                    'onclick' => 'return deleteData()',
                ),
            ),
        );
        echo $this->crud_m->get_data($configs);
    }

    public function add()
    {
        $data['content'] = 'users/add';
        $this->load->view('template', $data);
    }

    public function edit($id = '')
    {
        $data['content'] = 'users/edit';
        $data['rowedit'] = $this->user->getListByid($id);
        $this->load->view('template', $data);
    }

    public function password()
    {
        $data['content'] = 'users/password';
        $this->load->view('template', $data);
    }

    public function menu($id = '', $nama = '')
    {
        $data['content'] = 'users/menu';
        $data['listtabel'] = $this->user->getList();
        $data['aksesmenu'] = $this->getAksesMenu($id);
        $data['user_id'] = $id;
        $user = $this->db->query("SELECT user_id,user_name FROM users WHERE user_id = '$id'")->row();
        $data['nama'] = (!empty($user)) ? $user->user_name : '';
        $this->load->view('template', $data);
    }

    public function getAksesMenu($user_id = '', $parent = 0)
    {
        $user = $user_id;
        $output = '';
        $query = $this->menu->getListMenu($user, $parent);
        if (count($query) > 0) {
            $output .= ($parent == 0) ? '<ul id="chektree">' : '<ul>';
            foreach ($query as $menu) {
                $output .= '<li>';
                if ($menu->checked == 1) {
                    $output .= "<input type='checkbox' name='menu[$menu->menu_id]' checked /> $menu->menu";
                } else {
                    $output .= "<input type='checkbox' name='menu[$menu->menu_id]' /> $menu->menu";
                }
                $output .= $this->getAksesMenu($user_id, $menu->menu_id);

                $output .= "</li>\n";
            }
            $output .= "</ul>\n";
        } else {
            // echo 'Data is Empty';
        }

        return $output;
    }

    public function create()
    {
        $post = $this->securepost->postMethod();
        $post['user_password'] = sha1($post['user_password']);
        $insertData = $this->crud_m->insert('users', $post);
        if ($insertData) {
            $this->session->set_flashdata('msg', '<div class="alert alert-info fade in">
                            <button class="close" data-dismiss="alert" type="button">x</button>
                            <strong>Data berhasil disimpan</strong>
                            </div>');
            redirect('/users');
        } else {
            $error = $this->crud_m->result($insertData);
            $this->session->set_flashdata('msg', '<div class="alert alert-warning fade in">
                            <button class="close" data-dismiss="alert" type="button">x</button>
                            <strong>Data gagal disimpan</strong>
                            </div>');
        }
    }

    public function update()
    {
        $post = $this->securepost->postMethod();
        if(!empty($post['user_password'])){
            $post['user_password'] = sha1($post['user_password']);
        }else{
            unset($post['user_password']);
        }
        $updateData = $this->crud_m->update('users', $post, array('user_id' => $post['user_id']));
        if ($updateData) {
            $this->session->set_flashdata('msg', '<div class="alert alert-info fade in">
                            <button class="close" data-dismiss="alert" type="button">x</button>
                            <strong>Data berhasil diupdate</strong>
                            </div>');
            redirect('/users');
        } else {
            $error = $this->crud_m->result($updateData);
            $this->session->set_flashdata('msg', '<div class="alert alert-warning fade in">
                            <button class="close" data-dismiss="alert" type="button">x</button>
                            <strong>Data gagal diupdate</strong>
                            </div>');
        }
    }

    public function delete($id = '')
    {
        // $updateData = $this->crud_m->update('users', array('user_aktif' => 0), array('user_id' => $id));
        $updateData = $this->crud_m->delete('users', array('user_id' => $id), true);
        if ($updateData) {
            $this->session->set_flashdata('msg', '<div class="alert alert-info fade in">
                        <button class="close" data-dismiss="alert" type="button">x</button>
                        <strong>Data berhasil dihapus</strong>
                        </div>');
        } else {
            $error = $this->crud_m->result($updateData);
            // echo $this->fungsi->warning($error['error'], base_url('users'));
            $this->session->set_flashdata('msg', '<div class="alert alert-warning fade in">
                        <button class="close" data-dismiss="alert" type="button">x</button>
                        <strong>Data gagal dihapus</strong>
                        </div>');
        }
        redirect('/users');
    }

    public function updatepass()
    {
        $this->load->library('encrypt');
        $post = $this->securepost->postMethod();
        $user_id = $this->session->userdata(SESS_PREFIK . 'user_id');

        $old = $this->db->query("SELECT user_id,user_password FROM users WHERE user_id = '$user_id'")->row();
        if (sha1($post['old_password']) != $old->user_password) {
            $this->session->set_flashdata('msg', '<div class="alert alert-warning fade in">
                            <button class="close" data-dismiss="alert" type="button">x</button>
                            <strong>Password lama tidak sesuai</strong>
                            </div>');
            redirect('/users/password');
        } else {
            if ($post['newpassword'] != $post['confirmpassword']) {
                $this->session->set_flashdata('msg', '<div class="alert alert-warning fade in">
                            <button class="close" data-dismiss="alert" type="button">x</button>
                            <strong>Password baru yang anda input harus sama</strong>
                            </div>');
                redirect('/users/password');
            } else {
                $updateData = $this->crud_m->update(
                    'users', 
                    array(
                        'user_password' => sha1($post['newpassword']),
                    ), 
                    array(
                        'user_id' => $user_id,
                        'user_password' => sha1($post['old_password']),
                    )
                );
                if ($updateData) {
                    $this->session->set_flashdata('msg', '<div class="alert alert-info fade in">
                        <button class="close" data-dismiss="alert" type="button">x</button>
                        <strong>Update password berhasil</strong>
                        </div>');
                    redirect('/users');
                } else {
                    $error = $this->crud_m->result($updateData);
                    $this->session->set_flashdata('msg', '<div class="alert alert-warning fade in">
                            <button class="close" data-dismiss="alert" type="button">x</button>
                            <strong>' . $error['message'] . '</strong></div>');
                    redirect('/users/password');
                }
            }
        }
    }

    public function updatemenu()
    {
        $user = $this->input->post('user_id');
        $postMenu = $this->input->post('menu');
        $this->db->delete('menu_akses', array('user_id' => $user));
        $arrTmp = array();
        foreach ($postMenu as $key => $val) {
            array_push($arrTmp, array('menu_id' => $key, 'user_id' => $user));
        }
        $insertMenuAkses = $this->db->insert_batch('menu_akses', $arrTmp);
        if ($insertMenuAkses) {
            echo $this->fungsi->warning('Udpdate Menu Akses Sukses', base_url('users/menu'));
        } else {
            echo $this->fungsi->warning('Udpdate Menu Akses gagal', base_url('users/menu'));
        }
    }
}
