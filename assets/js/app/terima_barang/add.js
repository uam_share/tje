$(document).ready(function() {
    
    var oTable = $('#terima-barang-detail').dataTable({
        "bProcessing": true,
        "bFilter": false,
        "bPaginate": false,
        "bSort": false,
        "sPaginationType": "bootstrap",
        "bScrollCollapse": true
    });

    $("#batal").click(function(){
        window.location = BASEURL + 'terima_barang';
    });

    var addDiv = $('#terima-barang-detail');
    $('#jml_ongkos1').keydown(function(e){
        $( "#addNew" ).trigger( "click" );
        return false;
    });
    // $('#addNew').live('click', function() {
    $('#addNew').on('click', function() {
        var i = $('#terima-barang-detail tr.addrow').length + 1;
        var banyak = 'banyak'+i;
        var satuan = 'satuan'+i;
        var jenis = 'jenis'+i;
        var jumlah = 'jumlah'+i;
        var kg_m3 = 'kg_m3'+i;
        var ongkos = 'ongkos'+i;
        var jml_ongkos = 'jml_ongkos'+i;
        var remNew = 'remNew'+i;
	
        $('<tr class="addrow">'
            +'<td><input type="text" class="span12" name="banyak[]" id="'+banyak+'"/></td>'
            +'<td><input type="text" class="span12" name="satuan[]" id="'+satuan+'"/></td>'
            +'<td><input type="text" class="span12" name="jenis_barang[]" id="'+jenis+'"/></td>'
            +'<td><input type="text" class="span12" name="jumlah[]" id="'+jumlah+'" autocomplete="off"/></td>'
            +'<td><select name="kg_m3[]" id="'+kg_m3+'" class="span12"><option value="KG">KG</option><option value="M3">M3</option></select></td>'
            +'<td><input type="text" class="span12" name="ongkos[]" id="'+ongkos+'" autocomplete="off"/></td>'
            +'<td><input type="text" class="span12" name="jml_ongkos[]" id="'+jml_ongkos+'" /></td>'
            +'<td style="text-align: center;"><a id="'+remNew+'" href="javascript:void(0);"><i class="icon-minus-sign fa fa-trash"></i></a></td>'
            +'</tr>').appendTo(addDiv);
	
        $('#'+remNew).on('click', function() {
            $(this).parents('tr').remove();
        });
        $('.addrow input, .addrow select').keydown(function(e){
            if(e.keyCode==13){
                if($(this).attr('id')==jml_ongkos){
                    $( "#addNew" ).trigger( "click" );
                    return false;
                }   
                if($(':input:eq(' + ($(':input').index(this) + 1) + ')').attr('type')=='submit'){
                    return true;
                }
                $(':input:eq(' + ($(':input').index(this) + 1) + ')').focus();
                return false;
            }
        });
        $('#'+banyak).focus();
        return false;
    });

    $('#tanda_terima').submit(function() {
        var no_terima = $("#no_terima").val();
        if (no_terima.length == 0){
            bootWindow('NO TERIMA HARUS DIISI');
            return false;
        }
        var pengirim = $("#pengirim").val();
        if (pengirim == null || pengirim == ''){
            bootWindow('PENGIRIM HARUS DIISI');
            return false;
        }
        var penerima = $("#penerima").val();
        if (penerima == null || penerima == ''){
            bootWindow('PENERIMA HARUS DIISI');
            return false;
        }

        var tanggal = $("#tanggal").val();
        if (tanggal.length == 0){
            bootWindow('TANGGAL HARUS DIISI');
            return false;
        }
        var banyak1 = $("#banyak1").val();
        if (banyak1 == undefined || banyak1.length == 0){
            bootWindow('TABEL HARUS DIISI');
            return false;
        }
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: $(this).serialize(),
            dataType: 'json',
            success: function(data) {
                // console.log(data);
                if(data.save){
                    bootWindow(data.resp, BASEURL + 'terima_barang', true);
                }else{
                    bootWindow(data.resp);
                }
            }
        })
        return false;
    });

    function bootWindow(msg,turl, success){
        var modal = '.modal-warning';
        if(success != undefined && success == true){
            modal = '.modal-success'
        }

        $(modal + ' .modal-body p').text(msg);
        $(modal).modal('show');
        if(typeof(turl) !== 'undefined'){
            setTimeout(function(){window.location = turl},2000);
        }
    }

    $('#pengirim').select2({
        ajax: {
            type: 'POST',
            url: BASEURL + 'terima_barang/get_pengirim',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return "query="+params.term;
            },
            processResults: function (data) {
                objects = [];
                if (data == false){
                    $('#penerima').val('');
                    return false;
                }
                $.each(data, function(i, object) {
                    objects.push({
                        id : object.nmpengirim,
                        text : object.nmpengirim
                    });
                });
                return {
                    results : objects
                };
            }
        }
    });
    $('#penerima').select2({
        ajax: {
            type: 'POST',
            url: BASEURL + 'terima_barang/get_penerima',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return "query="+params.term;
            },
            processResults: function (data) {
                objects = [];
                if (data == false){
                    $('#penerima').val('');
                    return false;
                }
                $.each(data, function(i, object) {
                    objects.push({
                        id : object.nmpenerima,
                        text : object.nmpenerima
                    });
                });
                return {
                    results : objects
                };
            }
        }
    });
    
});