$(document).ready(function() {
	var oTable = $('#dtTable').DataTable(
    	$.extend({}, defaultDTOptions, {
			"bProcessing": true,
	        "bServerSide": true,
	        "aaSorting": [[ 0, "desc" ]],
			"sAjaxSource": BASEURL + 'terima_barang/get_data',
			'fnServerData': function(sSource, aoData, fnCallback) {
	            $.ajax({
	                'dataType': 'json',
	                'type': 'POST',
	                'url': sSource,
	                'data': aoData,
	                'success': fnCallback
	            });
	        }
		})
    );
});