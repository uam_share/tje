$(document).ready(function() {
    var oTable = $('#terima-barang-detail').dataTable({
        "bProcessing": true,
        "bFilter": false,
        "bPaginate": false,
        "bSort": false,
        "sPaginationType": "bootstrap",
        "bScrollCollapse": true
    });

    $("#ongkos0").keyup(function(){
        var total = $("#jumlah0").val() * $("#ongkos0").val();
        $("#jml_ongkos0").val(total);
    });

    $("#jumlah0").keyup(function(){
        var total = $("#jumlah0").val() * $("#ongkos0").val();
        $("#jml_ongkos0").val(total);
    });

    $("#batal").click(function(){
        window.location = "<?php echo base_url(); ?>terima_barang";
    });

    var addDiv = $('#terima-barang-detail');
    // var i = $('#terima-barang-detail tr').length;
    $('#addNew').on('click', function() {
        var i = $('#terima-barang-detail tr.addrow').length + 1;
        var banyak = 'banyak'+i;
        var satuan = 'satuan'+i;
        var jenis = 'jenis'+i;
        var jumlah = 'jumlah'+i;
        var kg_m3 = 'kg_m3'+i;
        var ongkos = 'ongkos'+i;
        var jml_ongkos = 'jml_ongkos'+i;
        var remNew = 'remNew'+i;
    
        $('<tr class="addrow">'
            +'<td><input type="text" class="span12" name="banyak[]" id="'+banyak+'"/></td>'
            +'<td><input type="text" class="span12" name="satuan[]" id="'+satuan+'"/></td>'
            +'<td><input type="text" class="span12" name="jenis_barang[]" id="'+jenis+'"/></td>'
            +'<td><input type="text" class="span12" name="jumlah[]" id="'+jumlah+'" autocomplete="off"/></td>'
            +'<td><select name="kg_m3[]" id="'+kg_m3+'" class="span12"><option value="KG">KG</option><option value="M3">M3</option></select></td>'
            +'<td><input type="text" class="span12" name="ongkos[]" id="'+ongkos+'" autocomplete="off"/></td>'
            +'<td><input type="text" class="span12" name="jml_ongkos[]" id="'+jml_ongkos+'" /></td>'
            +'<td style="text-align: center;"><a id="'+remNew+'" href="javascript:void(0);"><i class="icon-minus-sign fa fa-trash"></i></a></td>'
            +'</tr>').appendTo(addDiv);
    
        $('#'+remNew).on('click', function() {
            $(this).parents('tr').remove();
        });
        $('.addrow input, .addrow select').keydown(function(e){
            if(e.keyCode==13){
                if($(this).attr('id')==jml_ongkos){
                    $( "#addNew" ).trigger( "click" );
                    return false;
                }   
                if($(':input:eq(' + ($(':input').index(this) + 1) + ')').attr('type')=='submit'){
                    return true;
                }
                $(':input:eq(' + ($(':input').index(this) + 1) + ')').focus();
                return false;
            }
        });
        $('#'+banyak).focus();
        return false;
    });

    function addTable(data){
        for(var i=0;i<data.length;i++){
            var banyak = 'banyak'+i;
            var satuan = 'satuan'+i;
            var jenis = 'jenis'+i;
            var jumlah = 'jumlah'+i;
            var kg_m3 = 'kg_m3'+i;
            var ongkos = 'ongkos'+i;
            var jml_ongkos = 'jml_ongkos'+i;
            var remNew = 'remNew'+i;
            if (i == 0){
                $('#'+banyak).val(data[i].BANYAK);
                $('#'+satuan).val(data[i].Satuan);
                $('#'+jenis).val(data[i].Barang);
                $('#'+jumlah).val(data[i].JUMLAH);
                $('#'+kg_m3).val(data[i].SAT);
                $('#'+ongkos).val(data[i].Ongkos);
                $('#'+jml_ongkos).val(toRP(data[i].jml_ongkos));
            }else{
                $('<tr class="dimrow addNew">'
                    +'<td><input type="text" class="span12" name="banyak[]" id="'+banyak+'" /></td>'
                    +'<td><input type="text" class="span12" name="satuan[]" id="'+satuan+'" /></td>'
                    +'<td><input type="text" class="span12" name="jenis_barang[]" id="'+jenis+'" /></td>'
                    +'<td><input type="text" class="span12" name="jumlah[]" id="'+jumlah+'" /></td>'
                    +'<td><select name="kg_m3[]" id="'+kg_m3+'" class="span12"><option value="KG">KG</option><option value="M3">M3</option></select></td>'
                    +'<td><input type="text" class="span12" name="ongkos[]" id="'+ongkos+'" /></td>'
                    +'<td><input type="text" class="span12" name="jml_ongkos[]" id="'+jml_ongkos+'" /></td>'
                    +'<td style="text-align: center;"><a id="'+remNew+'" href="javascript:void(0);"><i class="icon-minus-sign fa fa-trash"></i></a></td>'
                    +'</tr>').appendTo(addDiv);
                $('#'+banyak).val(data[i].BANYAK);
                $('#'+satuan).val(data[i].Satuan);
                $('#'+jenis).val(data[i].Barang);
                $('#'+jumlah).val(data[i].JUMLAH);
                $('#'+kg_m3).val(data[i].SAT);
                $('#'+ongkos).val(data[i].Ongkos);
                $('#'+jml_ongkos).val(toRP(data[i].jml_ongkos));
                $('#'+remNew).on('click', function(){
                    $(this).parents('tr').remove();
                });
            }
        }
        // return false;
    }

    $('#tanda_terima').submit(function() {
        var no_terima = $("#no_terima").val();
        if (no_terima.length == 0){
            bootWindow('NO TERIMA HARUS DIISI');
            return false;
        }
        var pengirim = $("#pengirim").val();
        if (pengirim == null || pengirim == ''){
            bootWindow('PENGIRIM HARUS DIISI');
            return false;
        }
        var penerima = $("#penerima").val();
        if (penerima == null || penerima == ''){
            bootWindow('PENERIMA HARUS DIISI');
            return false;
        }

        var tanggal = $("#tanggal").val();
        if (tanggal.length == 0){
            bootWindow('TANGGAL HARUS DIISI');
            return false;
        }
        var banyak0 = $("#banyak0").val();
        if (banyak0 == undefined || banyak0.length == 0){
            bootWindow('TABEL HARUS DIISI');
            return false;
        }
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: $(this).serialize(),
            dataType: 'json',
            success: function(data) {
                // bootWindow(data.resp,BASEURL + 'terima_barang');
                if(data.save){
                    bootWindow(data.resp, BASEURL + 'terima_barang', true);
                }else{
                    bootWindow(data.resp);
                }
            }
        })
        return false;
    });

    function bootWindow(msg,turl,success){
        var modal = '.modal-warning';
        if(success != undefined && success == true){
            modal = '.modal-success'
        }

        $(modal + ' .modal-body p').text(msg);
        $(modal).modal('show');
        if(typeof(turl) !== 'undefined'){
            setTimeout(function(){window.location = turl},2000);
        }
    }

    function lodaData(){
        $('#pengirim').val($('#pengirim').val());
        $('#penerima').val($('#penerima').val());
        $.ajax({
            type: 'POST',
            url: BASEURL + "terima_barang/get_table/",
            data: "no_terima=" + $('#no_terima').val(), 
            dataType:"json",
            success:function(data){
                addTable(data.grid);
            }
        });
    }
    lodaData();
    
        
    $('#pengirim').select2({
        placeholder: '--- SELECT ---',
        // 
        ajax: {
            type: 'POST',
            url: BASEURL + 'terima_barang/get_pengirim',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return "query="+params.term;
            },
            processResults: function (data) {
                objects = [];
                if (data == false){
                    $('#penerima').val('');
                    return false;
                }
                $.each(data, function(i, object) {
                    objects.push({
                        id : object.nmpengirim,
                        text : object.nmpengirim
                    });
                });
                return {
                    results : objects
                };
            }
        }
    });
    $('#penerima').select2({
        placeholder: '--- SELECT ---',
        ajax: {
            type: 'POST',
            url: BASEURL + 'terima_barang/get_penerima',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return "query="+params.term;
            },
            processResults: function (data) {
                objects = [];
                if (data == false){
                    $('#penerima').val('');
                    return false;
                }
                $.each(data, function(i, object) {
                    objects.push({
                        id : object.nmpenerima,
                        text : object.nmpenerima
                    });
                });
                return {
                    results : objects
                };
            }
        }
    });
});