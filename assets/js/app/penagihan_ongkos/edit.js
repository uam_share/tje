$(document).ready(function() {
    getdetailkirim($('#nokirim').val(), BASEURL + "penagihan_ongkos/get_detail_kirim/true");
    $('#tanda_terima').submit(function() {
        var no_polisi = $("#no_polisi").val();
        if ($("#m_tagih_no").val().length == 0) {
            bootWindow('NO HARUS DIISI');
            return false;
        }
        if ($("#m_tagih_tgl").val().length == 0) {
            bootWindow('TGL HARUS DIISI');
            $("#m_tagih_tgl").focus();
            return false;
        }
        if ($("#m_tagih_fk").val().length == 0) {
            bootWindow('NO KIRIM HARUS DIISI');
            return false;
        }
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: $(this).serialize(),
            dataType: 'json',
            success: function(data) {
                if (data.save == true) {
                    bootWindow(data.resp, BASEURL + 'penagihan_ongkos', true);
                } else {
                    bootWindow(data.resp);
                }
            }
        })
        return false;
    });

    function addTable(data) {
        var addDiv = $('#tagih-ongkos-detail');
        var jml_bbjkt;
        var jml_bbjbi;
        var jml_subtotal;
        jml_bbjkt = jml_bbjbi = jml_subtotal = 0;
        for (var i = 0; i < data.length; i++) {
            var childcheck = (data[i].stat == '3') ? 'checked' : '';
            var clstr = '';
            if (data[i].stat == '3') {
                clstr = 'list-ceck';
            } else {
                clstr = '';
            }
            var number = i + 1;
            $('<tr class="dimrow addrow' + clstr + '">'
                    + '<td width="1%">' + number + '</td>'
                    + '<td>' + data[i].NOMUAT + '<input type="hidden" name="id_kirim[]" value="' + data[i].id + '"/></td>'
                    + '<td>' + data[i].NOTERIMA + '</td>'
                    + '<td width="15%">' + data[i].BARANG + '</td>'
                    + '<td style="text-align: center;">' + data[i].BANYAK + '</td>'
                    + '<td>' + data[i].SATUAN + '</td>'
                    + '<td>' + data[i].NMPENGIRIM + '</td>'
                    + '<td>' + data[i].NMPENERIMA + '</td>'
                    + '<td><input class="in_bbjkt text-right" onchange="updateJmlBBjkt(this)" type="text" name="bb_jkt[]" value="' + toRP(data[i].bb_jkt) + '" /></td>'
                    + '<td><input class="in_bbjbi text-right" onchange="updateJmlBBjbi(this)" type="text" name="jml_ongkos[]" value="' + toRP(data[i].jml_ongkos) + '" /></td>'
                    + '<td class="in_subtotal text-right">' + toRP(data[i].subtotal) + '</td>'
                    + '<td><textarea name="ket_tagih[]" cols="30" rows="1">' + data[i].ket_tagih + '</textarea></td>'
                    + '<td><input type="checkbox" class="span12 child-check" ' + childcheck + ' name="stat[]" value="' + data[i].id + '" onclick="clickcek(this)" /></td>'
                    + '</tr>').appendTo(addDiv);
            jml_bbjkt = jml_bbjkt + parseInt(data[i].bb_jkt);
            jml_bbjbi = jml_bbjbi + parseInt(data[i].jml_ongkos);
            jml_subtotal = jml_subtotal + parseInt(data[i].subtotal);
        }
        //alert(jml_bbjkt);
        $('#jml_bbjkt').html(toRP(jml_bbjkt));
        $('#jml_bbjbi').html(toRP(jml_bbjbi));
        $('#jml_subtotal').html(toRP(jml_subtotal));
        $('.addrow input, .addrow select').keydown(function(e){
            if(e.keyCode==13){
                if($(':input:eq(' + ($(':input').index(this) + 1) + ')').attr('type')=='submit'){
                    return true;
                }
                $(':input:eq(' + ($(':input').index(this) + 1) + ')').focus();
                return false;
            }
        });
    }

    function getdetailkirim(item, turl) {
        $.ajax({
            type: 'POST',
            url: turl,
            data: {
                no_polisi: item,
                status_tagih: $('#filter-status_tagih').val()
            }, //"no_polisi=" + item,
            dataType: "json",
            success: function(data) {
                $('.odd').remove();
                $('.dimrow').remove();
                addTable(data.grid);
            }
        });
    }
    $('#filter-status_tagih').change(function(e) {
        getdetailkirim($('#nokirim').val(), BASEURL + "penagihan_ongkos/get_detail_kirim/true");
    });
    $('.export-data').click(function(e) {
        $(this).attr('href', $(this).attr('data-url') + '/' + $('#filter-status_tagih').val() + '/' + $(this).attr('data-format'));
        return true;
    });

    $('#parent-check').click(function(e) {
        if ($(this).is(':checked')) {
            $('#tagih-ongkos-detail > tbody > tr > td > .child-check').prop('checked', true);
        } else {
            $('#tagih-ongkos-detail > tbody > tr > td > .child-check').prop('checked', false);
        }
    });
    $('#tagih-ongkos-detail').on('click', 'tr > td > input[type="checkbox"]', function (e) {
        if ($(this).is(':checked')) {
           $(this).parent().parent().addClass('list-ceck');
        } else {
            $('#parent-check').prop('checked', false);
            $(this).parent().parent().removeClass('list-ceck');
        }
    });
});

function updateJmlBBjkt(obj) {
    setJmlbbjkt();
}

function setJmlbbjkt() {
    var in_bbjbi = $('.in_bbjbi');
    var in_bbjkt = $('.in_bbjkt');
    var in_subtotal = $('.in_subtotal');
    var jml_bbjkt, jml_subtotal;
    jml_bbjkt = jml_subtotal = 0;
    var inbbjbi;
    var inbbjkt;
    var insubtotal;
    for (var i = 0; i < in_bbjkt.length; i++) {
        //alert($(in_bbjkt[i]).val());
        inbbjkt = $(in_bbjkt[i]).val();
        inbbjkt = inbbjkt.replace(".", "");
        inbbjkt = inbbjkt.replace(".", "");
        jml_bbjkt = jml_bbjkt + parseInt(inbbjkt);
        inbbjbi = $(in_bbjbi[i]).val();
        inbbjbi = inbbjbi.replace(".", "");
        inbbjbi = inbbjbi.replace(".", "");
        insubtotal = parseInt(inbbjbi) + parseInt(inbbjkt);
        $(in_subtotal[i]).html(toRP(insubtotal));
        jml_subtotal += insubtotal;
    }
    $('#jml_bbjkt').html(toRP(jml_bbjkt));
    $('#jml_subtotal').html(toRP(jml_subtotal));
}

function updateJmlBBjbi(obj) {
    //alert($(obj).val());
    setJmlbbjbi();
}

function setJmlbbjbi() {
    var in_bbjbi = $('.in_bbjbi');
    var in_bbjkt = $('.in_bbjkt');
    var in_subtotal = $('.in_subtotal');
    var jml_bbjbi, jml_subtotal;
    jml_bbjbi = jml_subtotal = 0;
    var inbbjbi;
    var inbbjkt;
    var insubtotal;
    for (var i = 0; i < in_bbjbi.length; i++) {
        //alert($(in_bbjkt[i]).val());
        inbbjbi = $(in_bbjbi[i]).val();
        inbbjbi = inbbjbi.replace(".", "");
        inbbjbi = inbbjbi.replace(".", "");
        jml_bbjbi = jml_bbjbi + parseInt(inbbjbi);
        inbbjkt = $(in_bbjkt[i]).val();
        inbbjkt = inbbjkt.replace(".", "");
        inbbjkt = inbbjkt.replace(".", "");
        insubtotal = parseInt(inbbjbi) + parseInt(inbbjkt);
        $(in_subtotal[i]).html(toRP(insubtotal));
        jml_subtotal += insubtotal;
    }
    $('#jml_bbjbi').html(toRP(jml_bbjbi));
    $('#jml_subtotal').html(toRP(jml_subtotal));
    //alert(in_bbjkt.length);
}

function clickcek(obj) {
    //alert($(this).attr('checked'));
    // if ($(obj).attr('checked') != 'checked') {
    //     //alert('tes');
    //     $('#parent-check').attr('checked', false);
    //     $(obj).parent().parent().removeClass('list-ceck');
    // } else {
    //     $(obj).parent().parent().addClass('list-ceck');
    // }
}