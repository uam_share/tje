$(document).ready(function() {
    function getNoKirim() {
        var url_no = BASEURL + 'penagihan_ongkos/get_no_kirim';
        $.getJSON(url_no, function(data) {
            $('#m_tagih_no').val(data.no_kirim);
        });
    }
    getNoKirim();
    $("#batal").click(function() {
        $('#tanda_terima input[type="text"]').val('');
        $('.dimrow').remove();
        getNoKirim();
    });
    $('#tanda_terima').submit(function() {
        var no_polisi = $("#no_polisi").val();
        if ($("#m_tagih_no").val().length == 0) {
            bootWindow('NO HARUS DIISI');
            return false;
        }
        if ($("#m_tagih_tgl").val().length == 0) {
            bootWindow('TGL HARUS DIISI');
            $("#m_tagih_tgl").focus();
            return false;
        }
        if ($("#m_tagih_fk").val().length == 0) {
            bootWindow('NO KIRIM HARUS DIISI');
            return false;
        }
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: $(this).serialize(),
            dataType: 'json',
            success: function(data) {
                if (data.save == true) {
                    bootWindow(data.resp, BASEURL + 'penagihan_ongkos', true);
                } else {
                    bootWindow(data.resp);
                }
            }
        })
        return false;
    });

    function addTable(data) {
        var addDiv = $('#tagih-ongkos-detail');
        var jml_bbjkt;
        var jml_bbjbi;
        var jml_subtotal;
        jml_bbjkt = jml_bbjbi = jml_subtotal = 0;
        for (var i = 0; i < data.length; i++) {
            var number = i + 1;
            $('<tr class="dimrow addrow">'
                 + '<td width="1%" class="text-center">' + number + '</td>'
                +'<td>'+ data[i].NOMUAT + '<input type="hidden" name="id_kirim[]" value="' + data[i].id + '"/></td>'
                +'<td>' + data[i].NOTERIMA + '</td>'
                +'<td width="15%">' + data[i].BARANG + '</td>'
                +'<td style="text-align: center;">' + data[i].BANYAK + '</td>'
                +'<td width="5%">' + data[i].SATUAN + '</td>'
                +'<td width="30%">' + data[i].NMPENGIRIM + '</td>'
                +'<td>' + data[i].NMPENERIMA + '</td>'
                +'<td><input class="in_bbjkt text-right" onchange="updateJmlBBjkt(this)" type="number" name="bb_jkt[]" value="' + toRP(data[i].bb_jkt) + '" /></td>'
                +'<td><input class="in_bbjbi text-right" onchange="updateJmlBBjbi(this)" type="number" name="jml_ongkos[]" value="' + toRP(data[i].jml_ongkos) + '" /></td>'
                +'<td class="in_subtotal text-right">' + toRP(data[i].subtotal) + '</td>'
                +'<td><textarea name="ket_tagih[]" cols="30" rows="1">'+data[i].ket_tagih+'</textarea></td>'
                +'<td><input type="checkbox" class="span12 child-check" name="stat[]" value="' + data[i].id + '" onclick="clickcek(this)" /></td>'
                +'</tr>').appendTo(addDiv);
            jml_bbjkt = jml_bbjkt + parseInt(data[i].bb_jkt);
            jml_bbjbi = jml_bbjbi + parseInt(data[i].jml_ongkos);
            jml_subtotal = jml_subtotal + parseInt(data[i].subtotal);
        }
        $('#jml_bbjkt').html(toRP(jml_bbjkt));
        $('#jml_bbjbi').html(toRP(jml_bbjbi));
        $('#jml_subtotal').html(toRP(jml_subtotal));

        $('.addrow input, .addrow select').keydown(function(e){
            if(e.keyCode==13){
                if($(':input:eq(' + ($(':input').index(this) + 1) + ')').attr('type')=='submit'){
                    return true;
                }
                $(':input:eq(' + ($(':input').index(this) + 1) + ')').focus();
                return false;
            }
        });
    }
    var dtobj = [];
    $('#m_tagih_fk').typeahead({
        source: function(query, process) {
            var objects = [];
            var map = {};
            $.ajax({
                type: 'POST',
                url: BASEURL + "penagihan_ongkos/get_nopol",
                data: "query=" + query,
                dataType: "json",
                success: function(data) {
                    if (data == false) {
                        $('#m_tagih_fk').val('');
                        return false;
                    }
                    dtobj = data;
                    $.each(data, function(i, object) {
                        map[object.nokirim] = object;
                        objects.push(object.nokirim);
                    });
                    process(objects);
                }
            });
        },
        updater: function(item) {
            var nokirim = '';
            $.each(dtobj, function(i, object) {
                if (item == object.nokirim) {
                    //                        alert(object.nopol);
                    $('#tgl_kirim').val(object.tglkirim);
                    $('#nokirim').val(object.nokirim);
                    $('#no_polisi').val(object.nopol);
                    nokirim = object.nokirim;
                }
            });
            $.ajax({
                type: 'POST',
                url: BASEURL + "penagihan_ongkos/get_detail_kirim",
                data: "no_polisi=" + nokirim,
                dataType: "json",
                success: function(data) {
                    $('.odd').remove();
                    $('.dimrow').remove();
                    addTable(data.grid);
                }
            });
            return item;
        }
    });

    $('#parent-check').click(function(e) {
        if ($(this).is(':checked')) {
            $('#tagih-ongkos-detail > tbody > tr > td > .child-check').prop('checked', true);
        } else {
            $('#tagih-ongkos-detail > tbody > tr > td > .child-check').prop('checked', false);
        }
    });

    $('#tagih-ongkos-detail').on('click', 'tr > td > input[type="checkbox"]', function (e) {
        if ($(this).is(':checked')) {
           $(this).parent().parent().addClass('list-ceck');
        } else {
            $('#parent-check').prop('checked', false);
            $(this).parent().parent().removeClass('list-ceck');
        }
    });
});

function updateJmlBBjkt(obj) {
    setJmlbbjkt();
}

function setJmlbbjkt() {
    var in_bbjbi = $('.in_bbjbi');
    var in_bbjkt = $('.in_bbjkt');
    var in_subtotal = $('.in_subtotal');
    var jml_bbjkt, jml_subtotal;
    jml_bbjkt = jml_subtotal = 0;
    var inbbjbi;
    var inbbjkt;
    var insubtotal;
    for (var i = 0; i < in_bbjkt.length; i++) {
        //alert($(in_bbjkt[i]).val());
        inbbjkt = $(in_bbjkt[i]).val();
        inbbjkt = inbbjkt.replace(".", "");
        inbbjkt = inbbjkt.replace(".", "");
        jml_bbjkt = jml_bbjkt + parseInt(inbbjkt);
        inbbjbi = $(in_bbjbi[i]).val();
        inbbjbi = inbbjbi.replace(".", "");
        inbbjbi = inbbjbi.replace(".", "");
        insubtotal = parseInt(inbbjbi) + parseInt(inbbjkt);
        $(in_subtotal[i]).html(toRP(insubtotal));
        jml_subtotal += insubtotal;
    }
    $('#jml_bbjkt').html(toRP(jml_bbjkt));
    $('#jml_subtotal').html(toRP(jml_subtotal));
}

function updateJmlBBjbi(obj) {
    setJmlbbjbi();
}

function setJmlbbjbi() {
    var in_bbjbi = $('.in_bbjbi');
    var in_bbjkt = $('.in_bbjkt');
    var in_subtotal = $('.in_subtotal');
    var jml_bbjbi, jml_subtotal;
    jml_bbjbi = jml_subtotal = 0;
    var inbbjbi;
    var inbbjkt;
    var insubtotal;
    for (var i = 0; i < in_bbjbi.length; i++) {
        //alert($(in_bbjkt[i]).val());
        inbbjbi = $(in_bbjbi[i]).val();
        inbbjbi = inbbjbi.replace(".", "");
        inbbjbi = inbbjbi.replace(".", "");
        jml_bbjbi = jml_bbjbi + parseInt(inbbjbi);
        inbbjkt = $(in_bbjkt[i]).val();
        inbbjkt = inbbjkt.replace(".", "");
        inbbjkt = inbbjkt.replace(".", "");
        insubtotal = parseInt(inbbjbi) + parseInt(inbbjkt);
        $(in_subtotal[i]).html(toRP(insubtotal));
        jml_subtotal += insubtotal;
    }
    $('#jml_bbjbi').html(toRP(jml_bbjbi));
    $('#jml_subtotal').html(toRP(jml_subtotal));
    //alert(in_bbjkt.length);
}

function clickcek(obj) {
    // if ($(obj).attr('checked') != 'checked') {
    //     $('#parent-check').attr('checked', false);
    //     $(obj).parent().parent().removeClass('list-ceck');
    // } else {
    //     $(obj).parent().parent().addClass('list-ceck');
    // }
}