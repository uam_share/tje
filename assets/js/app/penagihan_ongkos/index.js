$(document).ready(function() {
    var oTable = $('#dtTable').DataTable(
        $.extend({}, defaultDTOptions, {
            "bProcessing": true,
            "bServerSide": true,
            "aaSorting": [[ 0, "desc" ]],
            "sAjaxSource": BASEURL + "penagihan_ongkos/get_data",
            'fnServerData': function(sSource, aoData, fnCallback) {
                aoData.push({
                    'name': 'fil_bln',
                    'value': $("select[name=filter_bln]").val()
                });
                aoData.push({
                    'name': 'fil_thn',
                    'value': $("select[name=filter_thn]").val()
                });
                $.ajax({
                    'dataType': 'json',
                    'type': 'POST',
                    'url': sSource,
                    'data': aoData,
                    'success': fnCallback
                });
            }
        })
    );
    $('select.filter-el').on('change', function() {
        oTable.draw();
    });
});