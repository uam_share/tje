$(document).ready(function() {
    var oTable = $('#dtTable').DataTable(
        $.extend({}, defaultDTOptions, {
            "bProcessing": true,
            "bServerSide": true,
            "aaSorting": [[ 0, "desc" ]],
            "sAjaxSource": BASEURL + 'terima_muat/get_data',
            'fnServerData': function(sSource, aoData, fnCallback) {
                aoData.push( { 'name' : 'date_min', 'value' : $("#datefrom0").val() } );
                aoData.push( { 'name' : 'date_max', 'value' : $("#dateto0").val() } );
                $.ajax({
                    'dataType': 'json',
                    'type': 'POST',
                    'url': sSource,
                    'data': aoData,
                    'success': fnCallback
                });
            }
        })
    );

    function filterDate(){
        $("#dateto0").datepicker('setStartDate', $("#datefrom0").val());
        $("#datefrom0").datepicker('setEndDate',$("#dateto0").val());
    }
    $('#datefrom0, #dateto0').datepicker( {
        format : 'yyyy-mm-dd',
        firstDay: 1,
        autoclose : true
    })
    .on('changeDate', function(e, date) {
        filterDate();
        // oTable.fnDraw();
        oTable.draw();
    });
    filterDate();

    $('.addon1 > i').click(function(e){
        $('#datefrom0').datepicker().focus();
    });
    $('.addon2 > i').click(function(e){
        $('#dateto0').datepicker().focus();
    });

    $('#dtTable').on('click', 'tr > td > a.print-muat-barang', function (e) {
        e.preventDefault();
        var href = $(this).attr('href');
        showPrintDialog(
            href, 
            "Print Muat Barang",
            '_blank'
        );
    } );
});
