$(document).ready(function() {
    $('.datepicker').datepicker({
        format : 'yyyy-mm-dd'
    });
    var oTable = $('#example').dataTable({
        "bProcessing": true,
        "bFilter": false,
        "bPaginate": false,
        "bSort": false,
        "sPaginationType": "bootstrap",
        "bScrollCollapse": true
    });


    $("#batal").click(function(){
        window.location = BASEURL + "terima_muat";
    });

    function sum_total(){
        var sum = 0;
        $("input[name *= 'jumlah']").each(function(){
            sum += +$(this).val();
        });
        $("#total").val(sum);
    }

    var addDiv = $('#terima-muat-detail');
    $('#tanda_terima').submit(function() {
        var no_terima = $("#no_terima").val();
        if (no_terima.length == 0){
            bootWindow('NO TERIMA HARUS DIISI');
            return false;
        }
        var no_muat = $("#no_muat").val();
        if (no_muat.length == 0){
            bootWindow('NO MUAT HARUS DIISI');
            return false;
        }
        var no_polisi = $("#no_polisi").val();
        if (no_polisi.length == 0){
            bootWindow('NO POLISI HARUS DIISI');
            return false;
        }
        var banyak1 = $("#banyak0").val();
        if (banyak1 == undefined || banyak1.length == 0){
            bootWindow('TABEL HARUS DIISI');
            return false;
        }
        
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: $(this).serialize(),
            dataType: 'json',
            success: function(data) {
                if(data.save){
                    $('#tanda_terima input[type="text"]').val('');
                    $('.dimrow').remove();
                    showPrintDialog(
                        "report/r_terima_muat/" + no_muat + "/muat_barang", 
                        "Print Muat Barang",
                        '_blank'
                    );
                    bootWindow(data.resp, BASEURL + 'terima_muat', true);
                }else{
                    bootWindow(data.resp);
                }
            }
        })
        return false;
    });

    $.ajax({
        type: 'POST',
        url: BASEURL + "terima_muat/get_table",
        data: "no_terima=" +  $('#no_muat').val(), 
        dataType:"json",
        success:function(data){
            addTable(data.grid, true);
            sum_total();
        }
    });

    function addTable(data, isinit){
        for(var i=0;i<data.length;i++){
            var banyak = 'banyak'+i;
            var satuan = 'satuan'+i;
            var jenis = 'jenis'+i;
            var jumlah = 'jumlah'+i;
            var kg_m3 = 'kg_m3'+i;
            var ongkos = 'ongkos'+i;
            var jml_ongkos = 'jml_ongkos'+i;
		
            if (i == 0 && isinit == true){
                // console.log(i);
                $('#'+banyak).val(data[i].BANYAK);
                $('#'+satuan).val(data[i].Satuan);
                $('#'+jenis).val(data[i].Barang);
                $('#'+jumlah).val(data[i].JUMLAH);
                $('#'+kg_m3).val(data[i].SAT);
                $('#'+ongkos).val(data[i].Ongkos);
                $('#'+jml_ongkos).val(toRP(data[i].Jml_ongkos));
            }else{
                console.log(i);
                $('<tr class="dimrow addrow">'
                    +'<td><input type="text" class="span12" name="banyak[]" id="'+banyak+'" /></td>'
                    +'<td><input type="text" class="span12" name="satuan[]" id="'+satuan+'" /></td>'
                    +'<td><input type="text" class="span12" name="jenis_barang[]" id="'+jenis+'" /></td>'
                    +'<td><input type="text" class="span12 text-right" name="jumlah[]" id="'+jumlah+'" /></td>'
                    +'<td><input type="text" class="span12" readonly name="kg_m3[]" id="'+kg_m3+'" /></td>'
                    +'<td><input type="text" class="span12 text-right" readonly name="ongkos[]" id="'+ongkos+'" /></td>'
                    +'<td><input type="text" class="span12 text-right" readonly name="jml_ongkos[]" id="'+jml_ongkos+'" /></td>'
                    +'</tr>'
                ).appendTo(addDiv);
                $('#'+banyak).val(data[i].BANYAK);
                $('#'+satuan).val(data[i].Satuan);
                $('#'+jenis).val(data[i].Barang);
                $('#'+jumlah).val(data[i].JUMLAH);
                $('#'+kg_m3).val(data[i].SAT);
                $('#'+ongkos).val(data[i].Ongkos);
                $('#'+jml_ongkos).val(toRP(data[i].Jml_ongkos));
            }

            $('.addrow input, .addrow select').keydown(function(e){
                if(e.keyCode==13){
                    if($(this).attr('id')==jml_ongkos){
                        $( "#addNew" ).trigger( "click" );
                        return false;
                    }   
                    if($(':input:eq(' + ($(':input').index(this) + 1) + ')').attr('type')=='submit'){
                        return true;
                    }
                    $(':input:eq(' + ($(':input').index(this) + 1) + ')').focus();
                    return false;
                }
            });
        }
    }
    $('#no_terima').typeahead({
        source: function (query, process) {
            objects = [];
            map = {};
            $.ajax({
                type: 'POST',
                url: BASEURL + "terima_muat/get_no_terima",
                data: "query="+query, 
                dataType:"json",
                success:function(data){
                    if (data == false){
                        $('#no_terima').val('');
                        return false;
                    }
                    $.each(data, function(i, object) {
                        map[object.no_terima] = object;
                        objects.push(object.no_terima);
                    });
                    process(objects);
                }
            });
        }
        ,updater: function(item) {
            $.ajax({
                type: 'POST',
                url: BASEURL + "terima_muat/get_detail_terima",
                data: "no_terima="+item, 
                dataType:"json",
                success:function(data){
                    $('#pengirim').val(data.detail[0].nmpengirim);
                    $('#penerima').val(data.detail[0].nmpenerima);
                    $('#status').val(data.detail[0].STATUS);
                    $('#alamat').val(data.detail[0].alamat);
                    $('.addrow').remove();
                    addTable(data.grid);
                    sum_total();
                }
            });
            return item;
        }
    });

    $('#no_polisi').typeahead({
        source: function (query, process) {
            objects = [];
            map = {};
            $.ajax({
                type: 'POST',
                url: BASEURL + "terima_muat/get_nopol",
                data: "query="+query, 
                dataType:"json",
                success:function(data){
                    if (data == false){
                        $('#no_polisi').val('');
                        return false;
                    }
                    $.each(data, function(i, object) {
                        map[object.nopol] = object;
                        objects.push(object.nopol);
                    });
                    process(objects);
                }
            });
        }
    });
});