$(document).ready(function() {
    $('#table-search').DataTable(
        $.extend({}, defaultDTOptions, {
            info:false,
            order: true,
            responsive: true,
            paging: false,
            searching: false,
 
        })
    );
    
    $('#pengirim').typeahead({
        source: function (query, process) {
            objects = [];
            map = {};
            $.ajax({
                type: 'POST',
                url: BASEURL + "terima_barang/get_pengirim",
                data: "query="+query, 
                dataType:"json",
                success:function(data){
                    if (data == false){
                        $('#pengirim').val('');
                        return false;
                    }
                    $.each(data, function(i, object) {
                        map[object.nmpengirim] = object;
                        objects.push(object.nmpengirim);
                    });
                    process(objects);
                }
            });
        }
    });
    $('#penerima').typeahead({
        source: function (query, process) {
            objects = [];
            map = {};
            $.ajax({
                type: 'POST',
                url: BASEURL + "terima_barang/get_penerima",
                data: "query="+query, 
                dataType:"json",
                success:function(data){
                    if (data == false){
                        $('#penerima').val('');
                        return false;
                    }
                    $.each(data, function(i, object) {
                        map[object.nmpenerima] = object;
                        objects.push(object.nmpenerima);
                    });
                    process(objects);
                }
            });
            return false
        }
    });
    $('#filter_no').val('noterima');
    $('#filter-item > li > a').click(function(e){
        e.preventDefault();
        $('#filter_no').val($(this).attr('value'));
        var filterNo = $('#filter_no').val();
        
        switch(filterNo){
            case 'noterima':  
                $('#nosearch').show();
                $('#penerima').hide();
                $('#pengirim').hide();
                $('#filter-title').html('No Barang');
                break;
            case 'nmpenerima':  
                $('#filter-title').html('Penerima');
                $('#penerima').show();
                $('#nosearch').hide();
                $('#pengirim').hide();
                break;
            case 'nmpengirim':  
                $('#filter-title').html('Pengirim');
                $('#pengirim').show();
                $('#penerima').hide();
                $('#nosearch').hide();
                break;
        }
    })
});