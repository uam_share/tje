$(document).ready(function() {
    // var oTable = $('#tagih-ongkos-detail').dataTable({
    //     "bProcessing": true,
    //     "bFilter": false,
    //     "bPaginate": false,
    //     "bSort": false,
    //     "sPaginationType": "bootstrap",
    //     "bScrollCollapse": true
    // });

    $("#batal").click(function(){
        $('#tanda_terima input[type="text"]').val('');
        $('.dimrow').remove();
        getNoKirim();
    });
    var addDiv = $('#tagih-ongkos-detail');

    function sum_total(){
        var sum = 0;
        $("input[name *= 'jml_ongkos']").each(function(){
            sum += +$(this).val();
        });
        $("#total").val(sum);
    }

    $('#tanda_terima').submit(function() {
        var no_polisi = $("#no_polisi").val();
        if ($("#no_kirim").val().length == 0){
            bootWindow('NO KIRIM HARUS DIISI');
            return false;
        }
        if ($("#tanggal").val().length == 0){
            bootWindow('TANGGAL HARUS DIISI');
            return false;
        }
        if (no_polisi.length == 0){
            bootWindow('NO POLISI HARUS DIISI');
            return false;
        }
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: $(this).serialize(),
            dataType: 'json',
            success: function(data) {
                if(data.save){
                    $('#tanda_terima input[type="text"]').val('');
                    $('#keterangan').val('');
                    $('.dimrow').remove();
                    bootWindow(data.resp, BASEURL + 'tagih_ongkos', true);
                }else{
                    bootWindow(data.resp);
                }
            }
        })
        return false;
    });

    function addTable(data){
        for(var i=0;i<data.length;i++){
	
            var no_stt = 'no_stt'+i;
            var kode = 'kode'+i;
            var banyak = 'banyak'+i;
            var satuan = 'satuan'+i;
            var pengirim = 'pengirim'+i;
            var penerima = 'penerima'+i;
            var status = 'status'+i;
            var jml_ongkos = 'jml_ongkos'+i;
		
            $('<tr class="dimrow">'
                +'<td>'+data[i].no_stt+'<input type="hidden" class="span12" name="no_stt[]" id="'+no_stt+'" readonly="true"/></td>'
                +'<td>'+data[i].kode+'<input type="hidden" class="span12" name="kode[]" id="'+kode+'" readonly="true"/></td>'
                +'<td>'+data[i].banyak+'<input type="hidden" class="span12" name="banyak[]" id="'+banyak+'" readonly="true"/></td>'
                +'<td>'+data[i].satuan+'<input type="hidden" class="span12" name="satuan[]" id="'+satuan+'" readonly="true"/></td>'
                +'<td>'+data[i].pengirim+'<input type="hidden" class="span12" name="pengirim[]" id="'+pengirim+'" readonly="true"/></td>'
                +'<td>'+data[i].penerima+'<input type="hidden" class="span12" name="penerima[]" id="'+penerima+'" readonly="true"/></td>'
                +'<td>'+data[i].status+'<input type="hidden" class="span12" name="status[]" id="'+status+'" readonly="true"/>'
                +'<input type="hidden" class="span12" name="jml_ongkos[]" id="'+jml_ongkos+'" readonly="true"/></td>'
            //                        +'<td><input type="checkbox" class="span12 child-check" name="stat[' + data[i].no_stt + ']" value="' + data[i].no_stt + '" onclick="clickcek()" /></td>'
                +'</tr>').appendTo(addDiv);
            $('#'+no_stt).val(data[i].no_stt);
            $('#'+kode).val(data[i].kode);
            $('#'+banyak).val(data[i].banyak);
            $('#'+satuan).val(data[i].satuan);
            $('#'+pengirim).val(data[i].pengirim);
            $('#'+penerima).val(data[i].penerima);
            $('#'+status).val(data[i].status);
            $('#'+jml_ongkos).val(data[i].jml_ongkos);
                
            //                }
        }
    }
    //Get Data Detail Kirim
    $.ajax({
        type: 'POST',
        url: BASEURL + "tagih_ongkos/get_detail_kirim",
        data: "nokirim=" + $('#no_kirim').val(), 
        dataType:"json",
        success:function(data){
            $('.odd').remove();
            $('.dimrow').remove();
            addTable(data.grid);
        }
    });
});