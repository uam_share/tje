$(document).ready(function() {
	$('#dtTable').DataTable(
    	$.extend({}, defaultDTOptions, {
			"bProcessing": true,
	        "bServerSide": true,
			"sAjaxSource": BASEURL + 'kendaraan/get_data',
			'fnServerData': function(sSource, aoData, fnCallback) {
	            $.ajax({
	                'dataType': 'json',
	                'type': 'POST',
	                'url': sSource,
	                'data': aoData,
	                'success': fnCallback
	            });
	        }
		})
    );
});