$(function(){ 
    $("#chektree").treeview({
        persist: "location",
        collapsed: true,
        unique: true
    });
    $("#chektree").find('li').children('input[type=checkbox]').click(function(){
        var childrenCheck = $(this).parent('li').find('input[type=checkbox]');
        if($(this).is(':checked')){
            $(childrenCheck).attr('checked',true);
        }else{
            $(childrenCheck).attr('checked',false);
        }
    })
    $("tr").not(':first').hover(
        function () {
            $(this).css("background","#9ccaf8");
        }, 
        function () {
            $(this).css("background","");
        }
    );
});