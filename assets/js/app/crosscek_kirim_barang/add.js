$(document).ready(function() {
    function getNoKirim() {
        var url_no = BASEURL + 'crosscek_kirim_barang/get_no_kirim';
        $.getJSON(url_no, function(data) {
            $('#m_cek_no').val(data.no_kirim);
        });
    }
    getNoKirim();

    $("#batal").click(function() {
        $('#tanda_terima input[type="text"]').val('');
        $('.dimrow').remove();
        getNoKirim();
    });
    
    $('#tanda_terima').submit(function() {
        var no_polisi = $("#no_polisi").val();
        if ($("#m_cek_no").val().length == 0) {
            bootWindow('NO HARUS DIISI');
            return false;
        }
        if ($("#m_cek_tgl").val().length == 0) {
            bootWindow('TGL HARUS DIISI');
            $("#m_cek_tgl").focus();
            return false;
        }
        if ($("#m_cek_fk").val().length == 0) {
            bootWindow('NO KIRIM HARUS DIISI');
            return false;
        }
        if (no_polisi.length == 0) {
            bootWindow('NO POLISI HARUS DIISI');
            return false;
        }

        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: $(this).serialize(),
            dataType: 'json',
            success: function(data) {
                if (data.save == true) {
                    bootWindow(data.resp, BASEURL + 'crosscek_kirim_barang', true);
                } else {
                    bootWindow(data.resp);
                }
            }
        })
        return false;
    });

    function addTable(data) {
        var addDiv = $('#cek-kirim-detail');
        for (var i = 0; i < data.length; i++) {
            var childcheck = (data[i].stat2 == '2') ? 'checked' : '';
            var childread;
            if (data[i].stat2 == '2') {
                childread = 'disabled checked';
                $('#parent-check').hide();
                $('#parent-check').attr('checked', true)
                $('#parent-check').val(data[i].stat)
            } else {
                childread = '';
            }
            var number = i + 1;
            $('<tr class="dimrow">'
                + '<td width="1%">' + number + '</td>'
                +'<td>'+ data[i].NOMUAT + '<input type="hidden" name="id_kirim[]" value="' + data[i].id + '"/></td>'
                +'<td>' + data[i].NOTERIMA + '</td>'
                +'<td width="15%">' + data[i].BARANG + '</td>'
                +'<td>' + data[i].BANYAK + '</td>'
                +'<td width="1%">' + data[i].SATUAN + '</td>'
                +'<td>' + data[i].NMPENGIRIM + '</td>'
                +'<td>' + data[i].NMPENERIMA + '</td>'
                +'<td><textarea name="ket_cek[]" cols="50" rows="1">'+ data[i].ket_cek + '</textarea></td>'
                +'<td><input type="checkbox" '+ childread + ' class="span12 child-check" '
                    +childcheck+' name="stat2[]" value="' + data[i].id + '" />' 
                +'</td>'
                +'</tr>').appendTo(addDiv);
        }
    }
    var dtobj = [];
    $('#m_cek_fk').typeahead({
        source: function(query, process) {
            var objects = [];
            var map = {};
            $.ajax({
                type: 'POST',
                url: BASEURL + "crosscek_kirim_barang/get_nopol",
                data: "query=" + query,
                dataType: "json",
                success: function(data) {
                    if (data == false) {
                        $('#m_cek_fk').val('');
                        return false;
                    }
                    dtobj = data;
                    $.each(data, function(i, object) {
                        map[object.nokirim] = object;
                        objects.push(object.nokirim);
                    });
                    process(objects);
                }
            });
        },
        updater: function(item) {
            $.each(dtobj, function(i, object) {
                if (item == object.nokirim) {
                    $('#tgl_kirim').val(object.tglkirim);
                    $('#no_polisi').val(object.nopol);
                }
            });
            $.ajax({
                type: 'POST',
                url: BASEURL + "crosscek_kirim_barang/get_detail_kirim",
                data: "no_polisi=" + item,
                dataType: "json",
                success: function(data) {
                    $('.odd').remove();
                    $('.dimrow').remove();
                    addTable(data.grid);
                }
            });
            return item;
        }
    });
    $('#parent-check').click(function(e) {
        if ($(this).is(':checked')) {
            $('#cek-kirim-detail > tbody > tr > td > .child-check').prop('checked', true);
        } else {
            $('#cek-kirim-detail > tbody > tr > td > .child-check').prop('checked', false);
        }
    });

    $('#cek-kirim-detail').on('click', 'tr > td > input[type="checkbox"]', function (e) {
        if ($(this).is(':checked')) {
           $(this).parent().parent().addClass('list-ceck');
        } else {
            $('#parent-check').prop('checked', false);
            $(this).parent().parent().removeClass('list-ceck');
        }
    });
});