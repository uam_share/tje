$(document).ready(function() {
    getdetailkirim($('#m_cek_fk').val(), BASEURL + "crosscek_kirim_barang/get_detail_kirim/true");
    $("#batal").click(function() {
        $('#tanda_terima input[type="text"]').val('');
        $('.dimrow').remove();
        // getNoKirim();
    });
    $('#tanda_terima').submit(function() {
        var no_polisi = $("#no_polisi").val();
        if ($("#m_cek_no").val().length == 0) {
            bootWindow('NO HARUS DIISI');
            return false;
        }
        if ($("#m_cek_tgl").val().length == 0) {
            bootWindow('TGL HARUS DIISI');
            $("#m_cek_tgl").focus();
            return false;
        }
        if ($("#m_cek_fk").val().length == 0) {
            bootWindow('NO KIRIM HARUS DIISI');
            return false;
        }
        if (no_polisi.length == 0) {
            bootWindow('NO POLISI HARUS DIISI');
            return false;
        }
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: $(this).serialize(),
            dataType: 'json',
            success: function(data) {
                if (data.save == true) {
                    bootWindow(data.resp, BASEURL + 'crosscek_kirim_barang', true);
                } else {
                    bootWindow(data.resp);
                }
            }
        })
        return false;
    });

    function addTable(data) {
        var addDiv = $('#cek-kirim-detail');
        for (var i = 0; i < data.length; i++) {
            var childcheck = (data[i].stat2 == '2') ? 'checked' : '';
            var childread;
            var clstr = '';
            if (data[i].stat2 == '2') {
                childread = 'checked';
                $('#parent-check').attr('checked', true);
                $('#parent-check').val(data[i].stat);
                clstr = 'list-ceck';
            } else {
                childread = '';
            }
            var number = i + 1;
            $('<tr class="dimrow ' + clstr + '">'
                    + '<td width="1%">' + number + '</td>'
                    + '<td>' + data[i].NOMUAT + '<input type="hidden" name="id_kirim[]" value="' + data[i].id + '"/></td>'
                    + '<td>' + data[i].NOTERIMA + '<input type="hidden" name="stat_old[]" value="' + data[i].stat + '"/></td>'
                    + '<td width="15%">' + data[i].BARANG + '</td>'
                    + '<td>' + data[i].BANYAK + '</td>'
                    + '<td width="1%">' + data[i].SATUAN + '</td>'
                    + '<td>' + data[i].NMPENGIRIM + '</td>'
                    + '<td>' + data[i].NMPENERIMA + '</td>'
                    + '<td><textarea name="ket_cek[]" cols="50" rows="1">' + data[i].ket_cek + '</textarea></td>'
                    + '<td><input type="checkbox" ' + childread + ' class="span12 child-check" ' + childcheck + ' name="stat2[]" value="' + data[i].id + '" onclick="clickcek(this)" /></td>'
                    + '</tr>').appendTo(addDiv);
        }
    }

    function getdetailkirim(item, turl) {
        console.log('getdetailkirim');
        $.ajax({
            type: 'POST',
            url: turl,
            data: "no_polisi=" + item,
            dataType: "json",
            success: function(data) {
                $('.odd').remove();
                $('.dimrow').remove();
                addTable(data.grid);
            }
        });
    }

    $('#parent-check').click(function(e) {
        if ($(this).is(':checked')) {
            $('#cek-kirim-detail > tbody > tr > td > .child-check').prop('checked', true);
        } else {
            $('#cek-kirim-detail > tbody > tr > td > .child-check').prop('checked', false);
        }
    });

    $('#cek-kirim-detail').on('click', 'tr > td > input[type="checkbox"]', function (e) {
        if ($(this).is(':checked')) {
           $(this).parent().parent().addClass('list-ceck');
        } else {
            $('#parent-check').prop('checked', false);
            $(this).parent().parent().removeClass('list-ceck');
        }
    });

    // $('#parent-check').click(function() {
    //     if ($(this).attr('checked') == 'checked') {
    //         $('#example > tbody > tr > td > .child-check').attr('checked', true);
    //     } else {
    //         $('#example > tbody > tr > td > .child-check').attr('checked', false);
    //     }
    // });
});

// function clickcek(obj) {
//     //alert($(this).attr('checked'));
//     if ($(obj).attr('checked') != 'checked') {
//         //alert('tes');
//         $('#parent-check').attr('checked', false);
//         $(obj).parent().parent().removeClass('list-ceck');
//     } else {
//         $(obj).parent().parent().addClass('list-ceck');
//     }
// }