function showPrintDialog(url, title, features){
    var wPrint = window.open( url, title, features);
    wPrint.window.print();
    wPrint.document.close();
}

function bootWindow(msg,turl, success){
    var modal = '.modal-warning';
    if(success != undefined && success == true){
        modal = '.modal-success'
    }

    $(modal + ' .modal-body p').text(msg);
    $(modal).modal('show');
    if(typeof(turl) !== 'undefined'){
        setTimeout(function(){window.location = turl},2000);
    }
}

var defaultDTOptions = {
    "sPaginationType": "full_numbers",
    "language": {
        processing: "Loading...",
        search: "Pencarian &nbsp;:",
        info: "baris <b>_START_</b> s/d <b>_END_</b> dari <b>_TOTAL_</b> data",
        infoEmpty: "Data tidak ditemukan",
        lengthMenu: "per _MENU_ Baris",
        infoFiltered:   "(filter per _MAX_ data)",
        // infoPostFix:    "",
        // loadingRecords: "Chargement en cours...",
        // zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
        emptyTable:     "Tidak ada data",
        paginate: {
            first: '<i class="fa fa-angle-double-left"></i>',
            previous: '<i class="fa fa-angle-left"></i>',
            next: '<i class="fa fa-angle-right"></i>',
            last: '<i class="fa fa-angle-double-right"></i>',
        },
        // aria: {
        //     sortAscending:  ": activer pour trier la colonne par ordre croissant",
        //     sortDescending: ": activer pour trier la colonne par ordre décroissant"
        // }
    },
    columnDefs: [{
        targets: 'no-sort',
        orderable: false
    }],
};

$(document).ready(function(e) {
	$('.form-input input, .form-input select').keydown(function(e){
		if(e.keyCode==13){       
			if($(':input:eq(' + ($(':input').index(this) + 1) + ')').attr('type')=='submit'){// check for submit button and submit form on enter press
				return true;
			}
			$(':input:eq(' + ($(':input').index(this) + 1) + ')').focus();
			return false;
		}
	});

	$('.datepicker').datepicker({
        format : 'yyyy-mm-dd',
        firstDay: 1,
        autoclose : true
    });
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass   : 'iradio_minimal-blue'
    })
});